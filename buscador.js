$(function(){
	$("#busqueda").val("");
	$("#busqueda").focus();
	$("#dataTables1").dataTable();
	var dat = {
		"funcion" : "buscador"
	}
	var jsonC = JSON.stringify(dat);
	$.ajax({
		url : "php/consulta_ajax.php",
		type : "POST",
		data : 'json=' + jsonC,
		dataType: "json",
		success : function(res){
			var template = "";
			$("#tabla_buscador").html("");
			$.each(res, function(key,value){
            	template += '<tr>'+
							'<td>'+
								value['id_articulos'] + 
							'</td>'  +
							'<td>'+
								value['nombre_articulo'] + 
							'</td>'  +
							'<td>'+
								value['tipo_articulo'] +
							'</td>'  + 
							'<td>'+
								value['unidad'] +
							'</td>' +
							'<td>'+
								value['existencia'] + 
							'</td>'+
							'<td>' +
								'<div><a data-toggle="tooltip" data-placement="top" title="Ver Mas" style="margin-right:15px" class="btn btn-primary btn-sm" href="detallearticulo.php?ref='+value['id_articulos']+'"><i style="color:#fff" class="glyphicon glyphicon-info-sign"></i></a></div>'+
							'</td>';
            });
            $("#tabla_buscador").html(template);
		},
		error : function(res){
			console.log(res);
		}
	});
	$("#busqueda").keyup(function(){
		var texto = $("#busqueda").val();
		var dat = {
			"funcion" : "inputbuscador",
			"texto"   : texto
		}
		var jsonC = JSON.stringify(dat);
		$.ajax({
			url : "php/consulta_ajax.php",
			type : "POST",
			data : 'json=' + jsonC,
			dataType: "json",
			success : function(res){
				var template = "";
				$("#tabla_buscador").html("");
				$.each(res, function(key,value){
	            	template += '<tr>'+
								'<td>'+
									value['id_articulos'] + 
								'</td>'  +
								'<td>'+
									value['nombre_articulo'] + 
								'</td>'  +
								'<td>'+
									value['tipo_articulo'] +
								'</td>'  + 
								'<td>'+
									value['unidad'] +
								'</td>' +
								'<td>'+
									value['existencia'] + 
								'</td>'+
								'<td>' +
									'<div><a data-toggle="tooltip" data-placement="top" title="Ver Mas" style="margin-right:15px" class="btn btn-primary btn-sm" href="detallearticulo.php?ref='+value['id_articulos']+'"><i style="color:#fff" class="glyphicon glyphicon-info-sign"></i></a></div>'+
								'</td>';
	            });
	            $("#tabla_buscador").html(template);
			},
			error : function(res){
				console.log(res);
			}
		});
	});
});