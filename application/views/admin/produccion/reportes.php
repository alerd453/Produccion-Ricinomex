<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Produccion
        <small>Reportes</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Produccion</li>
        <li>Reportes</li>
      </ol>
    </section>
    <section class="content">
      <div class="box box-solid">
        <div class="box-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h3>Consuta por rango de fechas</h3>
              <form action="<?php echo base_url();?>reportes_produccion/consulta" method="POST">
                <div class="form-group">
                  <label>Fecha Inicial</label>
                  <input type="date" name="fechaini" id="fechaini" class="form-control"> 
                </div>
                <div class="form-group">
                  <label>Fecha Final</label>
                  <input type="date" name="fechafin" id="fechafin" class="form-control">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-flat btn-success">Consultar</button>
                </div>
              </form>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-8 col-md-offset-2" id="report_produccion">
              <table class="table table-bordered table-hover" id="personal">
                <thead>
                  <th>Tipo</th>
                  <th>#</th>
                  <th>Costales</th>
                  <th>Kg</th>
                  <th>$</th>
                </thead>
                <?php 
                  if(count($humedo)==0){
                    $numHumedo = 0;
                    $cosHumedo = 0;
                    $kgHumedo = 0;
                  } else{
                    $numHumedo = count($humedo);
                    $cosHumedo = 0;
                    $kgHumedo = 0;
                    foreach ($humedo as $h) {
                      $cosHumedo += $h->costales_i;
                      $kgHumedo += $h->kg_i;
                    }
                  }
                  if(count($sindescascarar)==0){
                    $numsindescascarar = 0;
                    $cossindescascarar = 0;
                    $kgsindescascarar = 0;
                  } else{
                    $numsindescascarar = count($sindescascarar);
                    $cossindescascarar = 0;
                    $kgsindescascarar = 0;
                    foreach ($sindescascarar as $h) {
                      $cossindescascarar += $h->costales_i;
                      $kgsindescascarar += $h->kg_i;
                    }
                  }
                  if(count($sinlimpiar)==0){
                    $numsinlimpiar = 0;
                    $cossinlimpiar = 0;
                    $kgsinlimpiar = 0;
                  } else{
                    $numsinlimpiar = count($sinlimpiar);
                    $cossinlimpiar = 0;
                    $kgsinlimpiar = 0;
                    foreach ($sinlimpiar as $h) {
                      $cossinlimpiar += $h->costales;
                      $kgsinlimpiar += $h->kg;
                    }
                  }
                  if(count($porpagar)==0){
                    $numporpagar = 0;
                    $cosporpagar = 0;
                    $kgporpagar = 0;
                  } else{
                    $numporpagar = count($porpagar);
                    $cosporpagar = 0;
                    $kgporpagar = 0;
                    foreach ($porpagar as $h) {
                      $cosporpagar += $h->costales;
                      $kgporpagar += $h->kg;
                    }
                  }
                  if(count($pagadoc)==0){
                    $numpagadoc = 0;
                    $ppagadoc = 0;
                    $kgpagadoc = 0;
                  } else{
                    $numpagadoc = count($pagadoc);
                    $ppagadoc = 0;
                    $kgpagadoc = 0;
                    foreach ($pagadoc as $h) {
                      $ppagadoc += $h->importetotal;
                      $kgpagadoc += $h->cantidadkg;
                    }
                  }
                  if(count($pagadoh)==0){
                    $numpagadoh = 0;
                    $ppagadoh = 0;
                    $kgpagadoh = 0;
                  } else{
                    $numpagadoh = count($pagadoh);
                    $ppagadoh = 0;
                    $kgpagadoh = 0;
                    foreach ($pagadoh as $h) {
                      $ppagadoh += $h->importetotal;
                      $kgpagadoh += $h->cantidadkg;
                    }
                  }
                  if(count($hueso)==0){
                    $numhueso = 0;
                    $kghueso = 0;
                  }else{
                    $numhueso = count($hueso);
                    $kghueso = 0;
                    foreach ($hueso as $hu) {
                      $kghueso += $hu->hueso;
                    }
                  }
                ?>
                <tbody>
                  <tr>
                    <td>Semilla Humeda</td>
                    <td><?php echo $numHumedo; ?></td>
                    <td><?php echo $cosHumedo; ?></td>
                    <td><?php echo number_format($kgHumedo,2); ?></td>
                    <td>-</td>
                  </tr>
                  <tr>
                    <td>Semilla sin Descascarar</td>
                    <td><?php echo $numsindescascarar; ?></td>
                    <td><?php echo $cossindescascarar; ?></td>
                    <td><?php echo number_format($kgsindescascarar,2); ?></td>
                    <td>-</td>
                  </tr>
                  <tr>
                    <td>Semilla sin Limpiar</td>
                    <td><?php echo $numsinlimpiar; ?></td>
                    <td><?php echo $cossinlimpiar; ?></td>
                    <td><?php echo number_format($kgsinlimpiar,2); ?></td>
                    <td>-</td>
                  </tr>
                  <tr>
                    <td>Semilla por Pagar</td>
                    <td><?php echo $numporpagar; ?></td>
                    <td><?php echo $cosporpagar; ?></td>
                    <td><?php echo number_format($kgporpagar,2); ?></td>
                    <td>-</td>
                  </tr>
                  <tr>
                    <td>Semilla Pagada Criolla</td>
                    <td><?php echo $numpagadoc; ?></td>
                    <td>-</td>
                    <td><?php echo number_format($kgpagadoc,2); ?></td>
                    <td><?php setlocale(LC_MONETARY,"es_MX"); echo money_format("%.2n", $ppagadoc); ?></td>
                  </tr>
                  <tr>
                    <td>Semilla Pagada Hibrida</td>
                    <td><?php echo $numpagadoh; ?></td>
                    <td>-</td>
                    <td><?php echo number_format($kgpagadoh,2); ?></td>
                    <td><?php setlocale(LC_MONETARY,"es_MX"); echo money_format("%.2n", $ppagadoh); ?></td>
                  </tr>
                  <tr>
                    <td>Hueso <code>(beta)</code></td>
                    <td><?php echo $numhueso; ?></td>
                    <td>-</td>
                    <td><?php echo number_format($kghueso,2); ?></td>
                    <td>-</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->