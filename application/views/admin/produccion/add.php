<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proceso
        <small>Agregar</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Proceso</li>
        <li>Agregar</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h3 class="text-center">Escoja el Productor</h3>
                        <hr>
                        <a href="<?php echo base_url(); ?>productores/add" class="btn btn-primary btn-flat pull-right"><span class="fa fa-plus"></span> Agregar Productor</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <hr>
                        <table class="table table-bordered table-hover" id="personal">
                            <thead>
                                <th>#</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Nombre</th>
                                <th>CURP</th>
                                <th>Localidad</th>
                                <th>Municipio</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                                <?php if (!empty($productores)):?>
                                    <?php foreach ($productores as $p): ?>
                                        <tr>
                                            <td><?php echo $p->idProducores ?></td>
                                            <td><?php echo $p->apellido_paterno ?></td>
                                            <td><?php echo $p->apellido_materno ?></td>
                                            <td><?php echo $p->nombre ?></td>
                                            <td><?php echo $p->curp ?></td>
                                            <td><?php echo $p->nombre_l ?></td>
                                            <td><?php echo $p->nombre_m ?></td>
                                            <td>
                                                <button type="button" class="btn btn-info btn-form-prod" data-toggle="modal" data-target="#modal-default" value="<?php echo $p->idProducores;?>">
                                                <span class="fa fa-check-square "></span>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Captura de Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->