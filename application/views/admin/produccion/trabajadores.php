<div class="form-group">
    <label>Asignacion de Trabajador</label>
    <br>
    <select class="form-control" name="trabajador" id="trabajador">
        <option></option>
        <?php foreach ($trabajadores as $trabajador): ?>
            <option value="<?php echo $trabajador->idTrabajadores ?>"><?php echo $trabajador->nombre ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group">
    <label>Asignacion de maquina descascaradora</label>
    <br>
    <select class="form-control" name="maquina" id="maquina">
        <option></option>
        <?php foreach ($maquinas as $maquina): ?>
            <option value="<?php echo $maquina->idMaquinas ?>"><?php echo $maquina->nombre ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group">
    <button class="btn btn-info">Aceptar</button>
</div>