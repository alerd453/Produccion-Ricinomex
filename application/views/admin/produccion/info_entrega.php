<h3>Información Del Proceso</h3>
<h4>Información Inicial</h4>
<strong>Fecha Inicial : </strong><p><?php echo $info->fecha_inicial?></p>
<strong>Costales Traidos : </strong><p><?php echo $info->costales_i?></p>
<strong>Kilogramos Traidos : </strong><p><?php echo $info->kg_i?></p>
<hr>
<h4>Información Tras Descascarado</h4>
<strong>Costales con Semilla : </strong><p><?php echo $info->costales_d?></p>
<strong>Kilogramos con Semilla : </strong><p><?php echo $info->kg_d?></p>
<hr>
<h4>Información Tras Limpieza</h4>
<strong>Costales con Semilla : </strong><p><?php echo $info->costales?></p>
<strong>Kilogramos con Semilla : </strong><p><?php echo $info->kg?></p>
<hr>
<h4>Información de Pago</h4>
<strong>Total a Pagar : </strong><p><?php echo $info->kg*$info->tipo?></p>
<button class="btn btn-info btn-flat">Pagar</button>