<?php
	date_default_timezone_set('America/Mexico_City'); 
 ?>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form action="<?php echo base_url();?>produccion/store" method="POST">
			<?php $fechaactual=	getdate();?>
			<input type="datetime" name="fecha_actual" value="<?php echo $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds']; ?>" hidden>
			<input type="number" name="id_productor" value="<?php echo $productor->idProducores ?>" hidden>
			<div class="form-group">
				<label for="nom">Nombre Completo</label>
				<input type="text" name="nombre" class="form-control" disabled="true" value="<?php echo $productor->nombre_completo ?>">
			</div>
			<div class="form-group">
				<label for="nom">CURP</label>
				<input type="text" name="curp" class="form-control" disabled="true" value="<?php echo $productor->curp ?>">
			</div>
			<div class="form-group">
				<label for="nom">Municipio</label>
				<input type="text" name="municipio" class="form-control" disabled="true" value="<?php echo $productor->nombre_m ?>">
			</div>
			<div class="form-group">
				<label for="nom">Localidad</label>
				<input type="text" name="localidad" class="form-control" disabled="true" value="<?php echo $productor->nombre_l ?>">
			</div>
			<div class="form-group">
				<label for="nom">Telefono</label>
				<input type="phone" name="telefono" class="form-control"  placeholder="Escriba el telefono del productor">
			</div>
			<div class="form-group">
				<label for="nom">Cantidad de Costales</label>
				<input type="number" name="costales" class="form-control" required="true" placeholder="Escriba la cantidad de costales traidos por el productor">
			</div>
			<div class="form-group">
				<label for="nom">Cantidad en Kg</label>
				<input type="number" name="kilos" class="form-control" step="0.5" required="true" placeholder="Escriba la cantidad de kilos traidos por el productor">
			</div>
			<div class="form-group">
				<label class="radio-inline">
                  <input type="radio" name="semilla" value="9" checked>Semilla Criolla
                </label>
                <label class="radio-inline">
                  <input type="radio" name="semilla" value="12">Semilla Hibrida
                </label>
			</div>
			<div class="form-group">
				<label>Semilla humeda</label>
				<input type="checkbox" name="humedo">
			</div>
			<button type="submit" class="btn btn-success btn-float">Guardar</button>
		</form>
	</div>
</div>