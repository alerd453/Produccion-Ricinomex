<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proceso
        <small>Listado</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Proceso</li>
        <li>Listado</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo base_url(); ?>produccion/add" class="btn btn-primary btn-flat pull-right"><span class="fa fa-plus"></span> Nuevo Proceso</a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-bordered table-hover" id="personal">
                            <thead>
                                <th>#</th>
                                <th>Fecha Registro</th>
                                <th>Nombre</th>
                                <th>Localidad</th>
                                <th>Municipio</th>
                                <th>Telefono</th>
                                <th>Cantidad Costales</th>
                                <th>Cantidad Kg</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                                <th>Pagar</th>
                            </thead>
                            <tbody>
                                <?php if (!empty($proceso)):?>
                                    <?php foreach ($proceso as $p): ?>
                                        <tr>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->idProceso ?></td>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->fecha_inicial ?></td>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->nombre_completo ?></td>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->nombre_l ?></td>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->nombre_m ?></td>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->telefono ?></td>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->costales_i ?></td>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->kg_i ?></td>
                                            <td class="<?php echo $p->color; ?>"><?php echo $p->idRP_Status ?> - <?php echo $p->nombre_s ?></td>
                                            <td class="<?php echo $p->color; ?>">
                                                <?php if($p->idRP_Status==0):?>
                                                    <button type="button" class="btn btn-info btn-view-s0" data-toggle="modal" data-target="#modal-0" value="<?php echo $p->idProceso."|".$p->idProducores;?>">
                                                    <small>DESCASCARAR</small>
                                                    </button>
                                                <?php elseif($p->idRP_Status==1): ?>
                                                    <!-- <button type="button" class="btn btn-info btn-view-productor" data-toggle="modal" data-target="#modal-1" value="<?php echo $p->idProceso."|".$p->idRP_Status;?>">
                                                    <small>DESCASCARANDO</small>
                                                    </button> -->
                                                    <?php if(!empty($descascarado)):?>
                                                        <?php foreach ($descascarado as $d): ?>
                                                            <?php if($d->RP_Proceso_idRP_Proceso == $p->idProceso):?>
                                                                <?php if($d->estado == "Running"): ?>
                                                                    <a href="#" class="btn btn-primary btn-sm btn-view-s1-pause" title="Pausar" data-toggle="tooltip" data-placement="top" data-id="<?php echo $p->idProceso;?>" style="margin-right:5px"><i class="fa fa-pause"></i></a>
                                                                    <a href="#" class="btn btn-danger btn-sm btn-view-s1-stop" title="Finalizar" data-toggle="modal" data-target="#modal-1" data-id="<?php echo $p->idProceso."|".$p->idProducores;?>" style="margin-right:5px"><i class="fa fa-stop"></i></a>
                                                                <?php elseif ($d->estado == "Pause"): ?>
                                                                    <a href="#" class="btn btn-primary btn-sm btn-view-s1-play" title="Continuar" data-toggle="tooltip" data-placement="top" data-id="<?php echo $p->idProceso;?>" style="margin-right:5px"><i class="fa fa-play"></i></a>
                                                                    <a href="#" class="btn btn-danger btn-sm btn-view-s1-stop" title="Finalizar" data-toggle="modal" data-target="#modal-1" data-id="<?php echo $p->idProceso."|".$p->idProducores;?>" style="margin-right:5px"><i class="fa fa-stop"></i></a>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                <?php elseif($p->idRP_Status==2): ?>
                                                    <button type="button" class="btn btn-info btn-view-s2" data-toggle="modal" data-target="#modal-2" value="<?php echo $p->idProceso."|".$p->idProducores;?>">
                                                    <small>LIMPIAR</small>
                                                    </button>
                                                <?php elseif($p->idRP_Status==3): ?>
                                                    <!-- <button type="button" class="btn btn-info btn-view-productor" data-toggle="modal" data-target="#modal-3" value="<?php echo $p->idProceso."|".$p->idRP_Status;?>">
                                                    <small>LIMPIANDO</small>
                                                    </button> -->
                                                    <?php if(!empty($limpieza)):?>
                                                        <?php foreach ($limpieza as $d): ?>
                                                            <?php if($d->RP_Proceso_idRP_Proceso == $p->idProceso):?>
                                                                <?php if($d->estado == "Running"): ?>
                                                                    <a href="#" class="btn btn-primary btn-sm btn-view-s3-pause" title="Pausar" data-toggle="tooltip" data-placement="top" data-id="<?php echo $p->idProceso;?>" style="margin-right:5px"><i class="fa fa-pause"></i></a>
                                                                    <a href="#" class="btn btn-danger btn-sm btn-view-s3-stop" title="Finalizar" data-toggle="modal" data-target="#modal-3" data-id="<?php echo $p->idProceso."|".$p->idProducores;?>" style="margin-right:5px"><i class="fa fa-stop"></i></a>
                                                                <?php elseif ($d->estado == "Pause"): ?>
                                                                    <a href="#" class="btn btn-primary btn-sm btn-view-s3-play" title="Continuar" data-toggle="tooltip" data-placement="top" data-id="<?php echo $p->idProceso;?>" style="margin-right:5px"><i class="fa fa-play"></i></a>
                                                                    <a href="#" class="btn btn-danger btn-sm btn-view-s3-stop" title="Finalizar" data-toggle="modal" data-target="#modal-3" data-id="<?php echo $p->idProceso."|".$p->idProducores;?>" style="margin-right:5px"><i class="fa fa-stop"></i></a>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                <?php elseif($p->idRP_Status==4): ?>
                                                    <button type="button" class="btn btn-info btn-view-s4" data-toggle="modal" data-target="#modal-4" value="<?php echo $p->idProceso."|".$p->idProducores;?>">
                                                    <small>PAGAR</small>
                                                    </button>
                                                <?php elseif($p->idRP_Status==5): ?>
                                                    <button type="button" class="btn btn-info btn-view-s5" data-toggle="modal" data-target="#modal-5" value="<?php echo $p->idProceso;?>">
                                                    <small>VER DETALLES</small>
                                                    </button>
                                                <?php elseif($p->idRP_Status==6): ?>
                                                    <button type="button" class="btn btn-info btn-view-s6" data-toggle="tooltip" title="Secar" data-placement="top" data-id="<?php echo $p->idProceso."|".$p->idProducores;?>">
                                                    <small>SECAR</small>
                                                    </button>
                                                <?php elseif($p->idRP_Status==7): ?>
                                                    <button type="button" class="btn btn-info btn-view-s7" data-toggle="modal" data-target="#modal-7" value="<?php echo $p->idProceso;?>">
                                                    <small>VER DETALLES</small>
                                                    </button>
                                                <?php elseif($p->idRP_Status==8): ?>
                                                    <!-- <button type="button" class="btn btn-info btn-view-productor" data-toggle="modal" data-target="#modal-7" value="<?php echo $p->idProceso."|".$p->idRP_Status;?>">
                                                    <small>SECANDO</small>
                                                    </button> -->
                                                    <a href="#" class="btn btn-danger btn-sm btn-view-s8-stop" title="Finalizar" data-toggle="tooltip" data-placement="top" data-id="<?php echo $p->idProceso;?>" style="margin-right:5px"><i class="fa fa-stop"></i></a>
                                                <?php endif; ?>
                                            </td>
                                            <td class="<?php echo $p->color; ?>">
                                                <?php if($p->idRP_Status < 4 && $p->RPid_compra_semilla==null): ?>
                                                    <button type="button" class="btn btn-info btn-view-s9" data-toggle="modal" data-target="#modal-8" data-id="<?php echo $p->idProceso."|".$p->kg_i."|".$p->telefono."|".$p->idProducores;?>">
                                                        <small>PAGAR</small>
                                                    </button>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<div class="modal fade" id="modal-0">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Inicio del Descascarado</h4>
            </div>
            <div class="modal-body">
                <?php 
                    date_default_timezone_set('America/Mexico_City');
                    $fechaactual=   getdate();  
                ?>
                <form action="produccion/save_form_ini_desc" method="POST">
                    <input type="number" name="id_proceso" id="id_proceso" hidden="true">
                    <input type="number" name="id_productor" id="id_productor" hidden="true">
                    <input type="text" name="fecha_inicial" value="<?php echo $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds']; ?>" hidden="true">
                    <div id="contenido0"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fin del Descascarado</h4>
            </div>
            <div class="modal-body">
                <?php 
                    date_default_timezone_set('America/Mexico_City');
                    $fechaactual=   getdate();  
                ?>
                <form action="produccion/save_form_fin_desc" method="POST">
                    <input type="number" name="id_proceso" id="id_proceso" hidden="true">
                    <input type="number" name="id_productor" id="id_productor" hidden="true">
                    <input type="text" name="fecha_final" value="<?php echo $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds']; ?>" hidden="true">
                    <div id="contenido1">
                        <div class="form-group">
                            <label>Costales vacios tras Descascarado</label>
                            <input type="number" name="cvacios" class="form-control" placeholder="Numero de Costales Vacios">
                        </div>
                        <div class="form-group">
                            <label>Costales con semilla tras Descascarado</label>
                            <input type="number" name="csemilla" class="form-control" placeholder="Numero de Costales con Semilla">
                        </div>
                        <div class="form-group">
                            <label>Kg de semilla tras Descascarado</label>
                            <input type="number" name="kgsemilla" class="form-control" placeholder="Numero de kilogramos de semilla" step="0.01">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success btn-flat">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Inicio Limpieza</h4>
            </div>
            <div class="modal-body">
                <?php 
                    date_default_timezone_set('America/Mexico_City');
                    $fechaactual=   getdate();  
                ?>
                <form action="produccion/save_form_ini_limp" method="POST">
                    <input type="number" name="id_proceso" id="id_proceso" hidden="true">
                    <input type="number" name="id_productor" id="id_productor" hidden="true">
                    <input type="text" name="fecha_inicial" value="<?php echo $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds']; ?>" hidden="true">
                    <div id="contenido0"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-3">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fin de la Limpieza</h4>
            </div>
            <div class="modal-body">
                <?php 
                    date_default_timezone_set('America/Mexico_City');
                    $fechaactual=   getdate();  
                ?>
                <form action="produccion/save_form_fin_limp" method="POST">
                    <input type="number" name="id_proceso" id="id_proceso" hidden="true">
                    <input type="number" name="id_productor" id="id_productor" hidden="true">
                    <input type="text" name="fecha_final" value="<?php echo $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds']; ?>" hidden="true">
                    <div id="contenido1">
                        <div class="form-group">
                            <label>Costales vacios tras Limpeza</label>
                            <input type="number" name="cvacios" class="form-control" placeholder="Numero de Costales Vacios">
                        </div>
                        <div class="form-group">
                            <label>Costales con semilla tras Limpeza</label>
                            <input type="number" name="csemilla" class="form-control" placeholder="Numero de Costales con Semilla">
                        </div>
                        <div class="form-group">
                            <label>Kg de semilla tras Limpeza</label>
                            <input type="number" name="kgsemilla" class="form-control" placeholder="Numero de kilogramos de semilla" step="0.01">
                        </div>
                        <div class="form-group">
                            <label>Kg de Hueso</label>
                            <input type="number" name="kghueso" class="form-control" placeholder="Numero de kilogramos de Hueso" step="0.01">
                        </div>
                        
                        <div class="form-group">
                            <button class="btn btn-success btn-flat">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-4">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pagar</h4>
            </div>
            <div class="modal-body">
                <?php 
                    date_default_timezone_set('America/Mexico_City');
                    $fechaactual=   getdate();  
                ?>
                <form action="produccion/generateFolio" method="POST">
                    <input type="number" name="id_proceso" id="id_proceso" hidden="true">
                    <input type="number" name="id_productor" id="id_productor" hidden="true">
                    <!-- <input type="text" name="fecha_final" value="<?php echo $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds']; ?>" hidden="true"> -->
                    <div id="contenido1">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-5">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Informacion Compra</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- modal -->
<div class="modal fade" id="modal-6">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Generar Folio de Compra</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- modal -->
<div class="modal fade" id="modal-8">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pago al 42%</h4>
            </div>
            <div class="modal-body">
                <div class="form">
                    <input type="text" name="id_productor" id="id_productor" hidden="true">
                    <input type="text" name="fecha" id="fecha" hidden="true">
                    <div class="form-group">
                        <label>Calculo:</label>
                        <input type="number" name="propuesta" class="form-control" id="propuesta" step="0.5"> 
                    </div>
                    <div class="form-group">
                        <label>Cantidad Kg a pagar:</label>
                        <input type="number" name="real" class="form-control" id="real" step="0.5">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="pagarcuarentaydos">Pagar</button>
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->