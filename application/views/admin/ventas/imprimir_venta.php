<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ventas
        <small>Reimprimir Venta</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Ventas</li>
        <li>Reimprimir Venta</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <form class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                          <label>Folio a Reimprimir</label>
                          <input type="text" name="folio" placeholder="" class="form-control" required id="folio_v" value="">
                        </div>
                        <input class="btn btn-info print_folio_v" value="Reimprimir Folio">
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->    