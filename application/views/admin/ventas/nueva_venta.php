<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ventas
        <small>Nueva Venta</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Ventas</li>
        <li>Nueva Venta</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <form class="col-md-8 col-md-offset-2" method="post" action="ventas/save_venta">
                        <?php date_default_timezone_set('America/Mexico_City');  ?>
                        <div class="form-group">
                            <label>Sr. (a)</label>
                            <input type="text" name="nombre" placeholder="" class="form-control" required id="nombre" value="">
                        </div>
                        <div class="form-group">
                            <label>Fecha Venta</label>
                            <input type="date" name="Fecha" placeholder="" class="form-control" required id="fecha" value="<?php echo date("Y")."-".date("m")."-".date("d") ?>">
                        </div>
                        <div class="form-group">
                            <label for="productoselec">Seleccione Producto: </label>
                            <select class="form-control" id="productoselec" name="producto">
                                <option>Seleccione algun producto</option>
                                <option value="Aceite">Aceite</option>
                                <option value="Pasta">Pasta</option>
                                <option value="Maiz">Maiz</option>
                                <option>Otro</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Precio</label>
                            <input type="number" name="precio" placeholder="" class="form-control" value="" id="precioventa">
                        </div>
                        <div class="form-group">
                            <label>Cantidad</label>
                            <input type="number" name="cantidad" placeholder="" class="form-control" required id="cantidadventa" value="">
                        </div>
                        <div class="form-group">
                            <label>Importe</label>
                            <input type="text" name="importe" placeholder="" class="form-control " required id="importeventa">
                        </div>
                        <button class="btn btn-info" type="submit">Generar Comprobante</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->    