<fieldset>


<input type="number" name="id" id="id_productor" required="true" hidden="true" value="<?php echo $productor->idProducores ?>">
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nombre">Nombre Completo</label>  
  <div class="col-md-6">
  <input id="nombre_prod_en" name="nombre" type="text" placeholder="" class="form-control input-md" required="" disabled="true" value="<?php echo $productor->nombre_completo ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="hectareas">Hectáreas a Surtir</label>  
  <div class="col-md-6">
  <input id="hectareas_en" name="hectareas" type="number" step="0.01" placeholder="¿Cuantas hectáreas para surtir?" class="form-control input-md" required="" value="0">
    
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hibrida">¿Semilla Híbrida?</label>
  <div class="col-md-6">
  <div class="radio">
    <label for="hibrida-0">
      <input type="radio" name="hibrida" id="hibrida-0" value="1">
      Si
    </label>
	</div>
  <div class="radio">
    <label for="hibrida-1">
      <input type="radio" name="hibrida" id="hibrida-1" value="0" checked="checked">
      No
    </label>
	</div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="urea">Urea (Kg)</label>  
  <div class="col-md-6">
    <div class="input-group">
      <input id="urea" name="urea" type="number" step="0.01" placeholder="Cantidad en Kg de Urea" class="form-control input-md" required="" disabled="true">
      <span class="input-group-addon">
        <input id="urea_check" type="checkbox" aria-label="..."> 
      </span>
    </div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="dap">DAP 18-46 (Kg)</label>  
  <div class="col-md-6">
    <div class="input-group">
      <input id="dap" name="dap" type="number" step="0.01" placeholder="Cantidad en Kg de 18-46" class="form-control input-md" required="" disabled="true">
      <span class="input-group-addon">
        <input id="dap_check" type="checkbox" aria-label="..."> 
      </span>
    </div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="insecticida">Insecticida (ml)</label>  
  <div class="col-md-6">
    <div class="input-group">
       <input id="insecticida" name="insecticida" type="number" step="0.01" placeholder="Cantidad en ml de Insecticida" class="form-control input-md" disabled="true">
      <span class="input-group-addon">
        <input id="insecticida_check" type="checkbox" aria-label="..."> 
      </span>
    </div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="mejorador">Mejorador (L)</label>  
  <div class="col-md-6">
    <div class="input-group">
       <input id="mejorador" name="mejorador" type="number" step="0.01" placeholder="Cantidad de Mejorador en L" class="form-control input-md" disabled="true">
      <span class="input-group-addon">
        <input id="mejorador_check" type="checkbox" aria-label="..."> 
      </span>
    </div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="fertilizante">Fertilizante (L)</label>  
  <div class="col-md-6">
    <div class="input-group">
       <input id="fertilizante" name="fertilizante" type="number" step="0.01" placeholder="Cantidad de Fertilizante en L" class="form-control input-md" disabled="true">
      <span class="input-group-addon">
        <input id="fertilizante_check" type="checkbox" aria-label="..."> 
      </span>
    </div>
  </div>
</div>
<br>
<br>
<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="enviar"></label>
  <div class="col-md-6">
    <button id="enviar_entrega_2018" name="enviar" class="btn btn-inverse">Entregar</button>
  </div>
</div>

</fieldset>
<script type="text/javascript">
  $(document).ready(function(){
    var hibrida = 0;
    $("input[name=hibrida]").change(function() {
      if($(this).val()){
        hibrida = 1;
        $("#urea").removeAttr("disabled");
        $("#dap").removeAttr("disabled");
        $("#insecticida").removeAttr("disabled");
        $("#mejorador").removeAttr("disabled");
        $("#fertilizante").removeAttr("disabled");
      }
      if($(this).val()==0){
        hibrida = 0;
        $("#urea").prop('disabled', true);
        $("#dap").prop('disabled', true);
        $("#insecticida").prop('disabled', true);
        $("#mejorador").prop('disabled', true);
        $("#fertilizante").prop('disabled', true);
      }
    });
    $("#hectareas_en").change(function(){
      var ha = $("#hectareas_en").val();
      var hibrida = $("input[name=hibrida]").val();
      if(hibrida){
        var urea = 50 * ha;
        var dap = 50 * ha;
        var insec = 250 * ha;
        var fa = ha;
        var mej = 5 * ha;
        $("#urea").val(urea);
        $("#dap").val(dap);
        $("#insecticida").val(insec);
        $("#mejorador").val(mej);
        $("#fertilizante").val(fa);
      }
    });
    $("#enviar_entrega_2018").on("click",function(e){
      var idProductor = $("#id_productor").val();
      var ha_en = $("#hectareas_en").val();
      //var hibrida = $("input[name=hibrida]").val();
      var urea = $("#urea").val();
      var urea_check = $("#urea_check:checked").val();
      var dap = $("#dap").val();
      var dap_check = $("#dap_check:checked").val();
      var insecticida = $("#insecticida").val();
      var insecticida_check = $("#insecticida_check:checked").val();
      var mejorador = $("#mejorador").val();
      var mejorador_check = $("#mejorador_check:checked").val();
      var fertilizante = $("#fertilizante").val();
      var fertilizante_check = $("#fertilizante_check:checked").val();
      var fecha = "<?php echo date("Y")."-".date("m")."-".date("d") ?>";
      //alert(hibrida);
      if(urea_check != "on"){
        urea = -1;
      }
      if(dap_check != "on"){
        dap = -1;
      }
      if(insecticida_check != "on"){
        insecticida = -1;
      }
      if(mejorador_check != "on"){
        mejorador = -1;
      }
      if(fertilizante_check != "on"){
        fertilizante = -1;
      }
      $.ajax({
        url: "http://www.ricinomex.com.mx/produccion/entrega_2018/save/" + idProductor + "/" + hibrida + "/" + urea + "/" + dap + "/" + insecticida + "/" + mejorador + "/" + fertilizante + "/" + fecha,
        type: "POST",
        success:function(resp){
          alert(resp);
        }
      });
    });
  });
</script>