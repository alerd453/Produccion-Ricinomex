<?php if($Entrega_2017->fecha_entrega_polvo == '0000-00-00' && $Entrega_2017->fecha_entrega_liquido == '0000-00-00'):?>
	<div class="col-md-6 text-center">
		<button class="btn btn-flat btn-success entrega_polvo">Entregar Polvo</button>
	</div>
	<div class="col-md-6 text-center">
		<button class="btn btn-flat btn-success entrega_liquido">Entregar Liquido</button>
	</div>
<?php elseif($Entrega_2017->fecha_entrega_polvo != '0000-00-00' && $Entrega_2017->fecha_entrega_liquido == '0000-00-00'): ?>
	<h2>Solo se ha entrego polvo</h2>
<?php elseif($Entrega_2017->fecha_entrega_polvo == '0000-00-00' && $Entrega_2017->fecha_entrega_liquido != '0000-00-00'): ?>
	<h2>Solo se ha entrego liquido</h2>
<?php else: ?>
	<h2>Se entrego Completo</h2>
<?php endif; ?>
