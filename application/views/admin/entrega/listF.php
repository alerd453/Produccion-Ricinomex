<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Productor
        <small>Listado</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Productor</li>
        <li>Listado</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover" id="personal">
                            <thead>
                                <th>#</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Nombre</th>
                                <th>CURP</th>
                                <th>Localidad</th>
                                <th>Municipio</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                                <?php
                                    $arrayid = array();
                                    foreach ($entregados as $e) {
                                        array_push($arrayid, $e->id_productor);
                                    }
                                ?>
                                <?php if (!empty($productores)):?>
                                    <?php foreach ($productores as $p): ?>
                                        <tr>
                                            <td><?php echo $p->idProducores ?></td>
                                            <td><?php echo $p->apellido_paterno ?></td>
                                            <td><?php echo $p->apellido_materno ?></td>
                                            <td><?php echo $p->nombre ?></td>
                                            <td><?php echo $p->curp ?></td>
                                            <td><?php echo $p->nombre_l ?></td>
                                            <td><?php echo $p->nombre_m ?></td>
                                            <td>
                                                <?php if(in_array($p->idProducores, $arrayid)): ?>
                                                    <div class="btn-group">
                                                        Entregado
                                                    </div>
                                                <?php else: ?>    
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info btn-view-entre" data-toggle="modal" data-target="#modal-entrega" value="<?php echo $p->idProducores;?>">
                                                        <span class="fa fa-pencil"></span>
                                                        </button>
                                                    </div>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<div class="modal fade" id="modal-entrega">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                   <!-- Form Name -->
                <legend>Entrega Fertilizante</legend>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" style="height: 500px;">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->