<?php 
	date_default_timezone_set('UTC');
	date_default_timezone_set("America/Mexico_City");
	header("Content-Type: text/html;charset=utf-8");
	function nombremes($mes){
		setlocale(LC_TIME, 'spanish');  
		$nombre=strftime("%B",mktime(0, 0, 0, $mes, 1, 2000)); 
		return $nombre;
	}
		$cantidad = (float) $compra->superficie * 50;
		$superficie = $compra->superficie;
		$fecha = $compra->fecha_compra;
		$array = explode("-", $fecha);
		$dia = $array[2];
		$mes = $array[1];
		$anio = $array[0];
	$this->pdf = new Pdf();
	$this->pdf->AddPage();
	$this->pdf->SetFont('Arial','B',16);
	$this->pdf->SetFillColor(0,0,0);
	$this->pdf->SetTextColor(255,255,255);
	$this->pdf->MultiCell(80,7,"Nota de remision de compra de Manzanita",0,'C',1);
	$this->pdf->Image('images/ricinomex_logo.png',95,10,30);
	$this->pdf->SetTextColor(0,0,0);
	$this->pdf->Cell(315,-20,"Folio",0,0,"C");
	$this->pdf->SetFont('Arial','',18);
	$this->pdf->SetTextColor(255,0,0);
	$this->pdf->Ln(0);
	$this->pdf->Cell(315,0,date("Y")."-".$id_compra,0,0,"C");
	$this->pdf->SetFont('Arial','',11);
	$this->pdf->SetTextColor(0,0,0);
	$this->pdf->Ln(8);
	$this->pdf->Cell(180,10,"Calle Higuerilla, Monte del toro, Heroica Cuidad de Ejutla de Crespo, Oaxaca, a ".$dia." de ".nombremes($mes)." de ".$anio,0,1,'C');
	$this->pdf->SetXY(12,45); // 77 = 70 posiciónY_anterior + 7 altura de las de cabecera
	$this->pdf->SetFont('Arial','',12); //Fuente, normal, tamaño
	$this->pdf->Cell(175,10,"Vendedor",1, 0 , 'L' );
	$this->pdf->SetXY(12,55);
	$this->pdf->Cell(44,10,"Sr. (a)",1, 0 , 'L' );
	$this->pdf->Cell(131,10,utf8_decode($compra->nombre_completo),1, 0 , 'C' );
	$this->pdf->SetXY(12,65);
	$this->pdf->Cell(44,10,"Hectareas Sembradas",1, 0 , 'L' );
	$this->pdf->Cell(43,10,utf8_decode($superficie),1, 0 , 'C' );
	$this->pdf->Cell(44,10,utf8_decode("Teléfono"),1, 0 , 'L' );
	$this->pdf->Cell(44,10,utf8_decode($compra->telefono),1, 0 , 'C' );
	$this->pdf->SetXY(12,75);
	$localidad = utf8_decode($compra->nombre_l);
	$this->pdf->Cell(44,10,utf8_decode("Población"),1, 0 , 'L' );
	$this->pdf->Cell(131,10,$localidad,1, 0 , 'C' );
	$this->pdf->SetXY(12,85);
	$this->pdf->Cell(44,10,utf8_decode("Cantidad Kg"),1, 0 , 'L' );
	$this->pdf->Cell(55,10,utf8_decode("Descripción"),1, 0 , 'L' );
	$this->pdf->Cell(32,10,utf8_decode("$ / kg"),1, 0 , 'L' );
	$this->pdf->Cell(44,10,utf8_decode("Importe Total"),1, 0 , 'L' );
	$this->pdf->SetXY(12,95);
	$this->pdf->SetFont('Arial','',18);
	$this->pdf->SetTextColor(255,0,0);
	$this->pdf->Cell(44,10,utf8_decode($compra->cantidadkg)." Kg",1, 0 , 'C' );
	$this->pdf->SetTextColor(0,0,0);
	$this->pdf->SetFont('Arial','',12);
	$this->pdf->Cell(55,10,utf8_decode("Manzanita"),1, 0 , 'L' );
	$this->pdf->SetFont('Arial','',12);
	$this->pdf->Cell(32,10,utf8_decode("$4.00"),1, 0 , 'C' );
	$this->pdf->SetFont('Arial','',18);
	$this->pdf->SetTextColor(255,0,0);
	$this->pdf->Cell(44,10,utf8_decode("$ ".$compra->importetotal),1, 0 , 'C' );
	$this->pdf->SetTextColor(0,0,0);
	$this->pdf->SetFont('Arial','',12);
	$this->pdf->SetXY(12,105);
	$this->pdf->Cell(65,10,"Recibo de conformidad mi pago",1, 0 , 'L' );
	$this->pdf->Cell(110,10,utf8_decode(""),1, 0 , 'C' );
	$this->pdf->SetXY(12,115);
	$this->pdf->Cell(65,10,"Compro",1, 0 , 'L' );
	$this->pdf->Cell(110,10,utf8_decode(""),1, 0 , 'C' );
	$this->pdf->Ln(40);
	$this->pdf->SetFont('Arial','B',16);
	$this->pdf->SetFillColor(0,0,0);
	$this->pdf->SetTextColor(255,255,255);
	$this->pdf->MultiCell(80,7,"Nota de remision de compra de Manzanita",0,'C',1);
	$this->pdf->Image('images/ricinomex_logo.png',95,10,30);
	$this->pdf->SetTextColor(0,0,0);
	$this->pdf->Cell(315,-20,"Folio",0,0,"C");
	$this->pdf->SetFont('Arial','',18);
	$this->pdf->SetTextColor(255,0,0);
	$this->pdf->Ln(0);
	$this->pdf->Cell(315,0,date("Y")."-".$id_compra,0,0,"C");
	$this->pdf->SetFont('Arial','',11);
	$this->pdf->SetTextColor(0,0,0);
	$this->pdf->Ln(8);
	$this->pdf->Cell(180,10,"Calle Higuerilla, Monte del toro, Heroica Cuidad de Ejutla de Crespo, Oaxaca, a ".$dia." de ".nombremes($mes)." de ".$anio,0,1,'C');
	$this->pdf->SetXY(12,190); // 77 = 70 posiciónY_anterior + 7 altura de las de cabecera
	$this->pdf->SetFont('Arial','',12); //Fuente, normal, tamaño
	$this->pdf->Cell(175,10,"Vendedor",1, 0 , 'L' );
	$this->pdf->SetXY(12,200);
	$this->pdf->Cell(44,10,"Sr. (a)",1, 0 , 'L' );
	$this->pdf->Cell(131,10,utf8_decode($compra->nombre_completo),1, 0 , 'C' );
	$this->pdf->SetXY(12,210);
	$this->pdf->Cell(44,10,"Hectareas Sembradas",1, 0 , 'L' );
	$this->pdf->Cell(43,10,utf8_decode($superficie),1, 0 , 'C' );
	$this->pdf->Cell(44,10,utf8_decode("Teléfono"),1, 0 , 'L' );
	$this->pdf->Cell(44,10,utf8_decode($compra->telefono),1, 0 , 'C' );
	$this->pdf->SetXY(12,220);
	$localidad = utf8_decode($compra->nombre_l);
	$this->pdf->Cell(44,10,utf8_decode("Población"),1, 0 , 'L' );
	$this->pdf->Cell(131,10,$localidad,1, 0 , 'C' );
	$this->pdf->SetXY(12,230);
	$this->pdf->Cell(44,10,utf8_decode("Cantidad Kg"),1, 0 , 'L' );
	$this->pdf->Cell(55,10,utf8_decode("Descripción"),1, 0 , 'L' );
	$this->pdf->Cell(32,10,utf8_decode("$ / kg"),1, 0 , 'L' );
	$this->pdf->Cell(44,10,utf8_decode("Importe Total"),1, 0 , 'L' );
	$this->pdf->SetXY(12,240);
	$this->pdf->SetFont('Arial','',18);
	$this->pdf->SetTextColor(255,0,0);
	$this->pdf->Cell(44,10,utf8_decode($compra->cantidadkg)." Kg",1, 0 , 'C' );
	$this->pdf->SetTextColor(0,0,0);
	$this->pdf->SetFont('Arial','',12);
	$this->pdf->Cell(55,10,utf8_decode("Manzanita"),1, 0 , 'L' );
	$this->pdf->SetFont('Arial','',12);
	$this->pdf->Cell(32,10,utf8_decode("$4.00"),1, 0 , 'C' );
	$this->pdf->SetFont('Arial','',18);
	$this->pdf->SetTextColor(255,0,0);
	$this->pdf->Cell(44,10,utf8_decode("$ ".$compra->importetotal),1, 0 , 'C' );
	$this->pdf->SetTextColor(0,0,0);
	$this->pdf->SetFont('Arial','',12);
	$this->pdf->SetXY(12,250);
	$this->pdf->Cell(65,10,"Recibo de conformidad mi pago",1, 0 , 'L' );
	$this->pdf->Cell(110,10,utf8_decode(""),1, 0 , 'C' );
	$this->pdf->SetXY(12,260);
	$this->pdf->Cell(65,10,"Compro",1, 0 , 'L' );
	$this->pdf->Cell(110,10,utf8_decode(""),1, 0 , 'C' );
	$this->pdf->Output($id_compra.".pdf", 'D');
?>