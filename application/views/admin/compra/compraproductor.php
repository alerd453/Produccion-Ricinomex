<table class="table table-bordered table-hover" id="personal">
	<thead>
	  <th>Folio</th>
	  <th>Nombre</th>
	  <th>Localidad</th>
	  <th>Kilos</th>
	  <th>Importe</th>
	  <th>Fecha Compra</th>
	</thead>
	<tbody>
		<?php
            $totalkg = 0;
            $totaldinero = 0;
        ?>
	  	<?php if (!empty($compras)):?>
	    	<?php foreach ($compras as $p): ?>
	    		<?php 
	    			$totalkg += $p->cantidadkg;
	    			$totaldinero += $p->importetotal;
	    		?>
	        	<tr>
	          		<td><?php echo $p->id_compra_semilla ?></td>
			        <td><?php echo $p->nombre_completo ?></td>
			        <td><?php echo $p->nombre_l ?></td>
			        <td><?php echo $p->cantidadkg ?></td>
			        <td><?php echo $p->importetotal ?></td>
			        <td><?php echo $p->fecha_compra ?></td>
	        	</tr>
	    <?php endforeach; ?>
	  <?php endif; ?>
	  <tr>
	  	<td colspan="3">Totales</td>
	  	<td><?php echo $totalkg; ?></td>
	  	<td><?php echo $totaldinero; ?></td>
	  </tr>
	</tbody>
</table>