<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Auditoria 
        <small>Compra de Semilla</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Auditoria</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="box box-solid">
        <div class="box-body">
          <?php //print_r($productores); ?>
          <table class="table table-bordered table-hover" id="personal">
            <thead>
              <th>Folio</th>
              <th>Comprador</th>
              <th>Localidad</th>
              <th>Kilos</th>
              <th>Importe</th>
              <th>Peso Auditoria</th>
              <th>Diferencia</th>
              <th>Acciones</th>
            </thead>
            <tbody>
              <?php if (!empty($compra)):?>
                <?php foreach ($compra as $c): ?>
                  <?php if($c->cancelado==0): ?>
                    <?php if($c->auditado==0):  ?> 
                        <tr>
                      <?php else: ?>
                        <tr bgcolor="#d5fed3">
                      <?php endif; ?>
                        <td><?php echo $c->id_compra_semilla ?></td>
                        <td><?php echo $c->nombre_completo ?></td>
                        <td><?php echo $c->nombre_l ?></td>
                        <td><?php echo $c->cantidadkg ?></td>
                        <td><?php echo $c->importetotal ?></td>
                        <?php if($c->kg_au == 0): ?>
                          <td class="text-center">-</td>
                          <td class="text-center">-</td>
                        <?php else: ?>
                          <td class="text-center"><?php echo $c->kg_au ?></td>
                          <?php if($c->cantidadkg-$c->kg_au>0): ?>
                            <td class="text-center"><font color="red">-<?php echo $c->cantidadkg-$c->kg_au ?></font></td>
                          <?php else: ?>
                            <td class="text-center"><?php echo $c->kg_au-$c->cantidadkg ?></td>
                          <?php endif; ?>
                        <?php endif; ?>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-info btn-auditar" data-toggle="modal" data-target="#modal-default" value="<?php echo $c->id_compra_semilla.'-new';?>">
                              <span class="fa fa-info"></span>
                            </button>
                          </div>
                        </td>
                      </tr>
                  <?php else: ?>
                    <tr bgcolor="#fed3d3">
                        <td><?php echo $c->id_compra_semilla ?></td>
                        <td><b>Cancelado</b></td>
                        <td><b><?php echo $c->razon_cancelado ?></b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php endif; ?>
              <?php if (!empty($compraold)):?>
                <?php foreach ($compraold as $c): ?>
                  <?php if($c->auditado==0):  ?> 
                      <tr>
                    <?php else: ?>
                      <tr bgcolor="#d5fed3">
                    <?php endif; ?>
                      <td><?php echo $c->id_compra_semilla ?></td>
                      <td><?php echo $c->nombre_completo ?></td>
                      <td><?php echo $c->nombre_l ?></td>
                      <td><?php echo $c->cantidadkg ?></td>
                      <td><?php echo $c->importetotal ?></td>
                      <?php if($c->kg_au == 0): ?>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                      <?php else: ?>
                        <td class="text-center"><?php echo $c->kg_au ?></td>
                        <?php if($c->cantidadkg-$c->kg_au>0): ?>
                          <td class="text-center"><font color="red">-<?php echo $c->cantidadkg-$c->kg_au ?></font></td>
                        <?php else: ?>
                          <td class="text-center"><?php echo $c->kg_au-$c->cantidadkg ?></td>
                        <?php endif; ?>
                      <?php endif; ?>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-info btn-auditar" data-toggle="modal" data-target="#modal-default" value="<?php echo $c->id_compra_semilla.'-old';?>">
                            <span class="fa fa-info"></span>
                          </button>
                        </div>
                      </td>
                    </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Informacion del Productor</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
          </div>
      </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->