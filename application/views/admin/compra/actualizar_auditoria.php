<form method="POST" action="<?php echo base_url()."auditoria/save" ?>">
	<input type="number" name="id_compra" value="<?php echo $id; ?>" hidden="true">
	<?php if($type == "new"): ?>
		<input type="number" name="tipo" value="1" hidden="true">
	<?php else: ?>
		<input type="number" name="tipo" value="2" hidden="true">
	<?php endif; ?>
	<div class="form-group">
		<label for="peso">Peso: </label>
		<input type="number" name="peso" class="form-control" step="0.5" required="true">
	</div>
	<div class="form-group">
		<label for="obs">Observaciones: </label>
		<input type="text" name="observacion" id="obs" class="form-control">
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-flat btn-success">Guardar</button>
	</div>
</form>