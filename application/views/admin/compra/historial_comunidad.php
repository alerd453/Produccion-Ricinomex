<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Compra Semilla 
        <small>Historial</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Compra Semilla</li>
        <li>Historial</li>
        <li>Historial Comunidad</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	<div class="box box-solid">
    		<div class="box-body">
    			<?php //print_r($localidades); ?>
          <form class="form-inline col-md-8 col-md-offset-2">
            <div class="form-group">
              <label for="localidades_consul">Localidad: </label>
              <select class="js-example-basic-single" name="localidades" id="localidades_consul">
                <?php if (!empty($localidades)):?>
                  <?php foreach ($localidades as $l): ?>
                    <option value="<?php echo $l->id ?>"><?php echo $l->nombre_l; ?></option>
                  <?php endforeach; ?>
                <?php endif; ?>
              </select>
            </div>
            <div class="form-group">
              <button type="button" class="btn btn-default pull-rigth boton-consulta-localidad">Consultar</button>
            </div>
          </form>
          <hr>
          <div id="cuerpo_localidades"></div>
    		</div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
  	<div class="modal-dialog">
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Informacion del Productor</h4>
	      	</div>
	      	<div class="modal-body">
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
	      	</div>
    	</div>
    <!-- /.modal-content -->
  	</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->