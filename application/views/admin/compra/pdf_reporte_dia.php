<table class="table table-bordered table-hover" id="personal">
	<thead>
		<th>Folio Compra</th>
		<th>Id Proveedor</th>
		<th>Nombre</th>
		<th>Poblacion</th>
		<th>Cantidad Kg</th>
        <th>Importe Total</th>
	</thead>
	<tbody>
        <?php 
            $totalkg = 0;
            $total = 0;
         ?>
		<?php if (!empty($dia)):?>
			<?php foreach ($dia as $d): ?>
                <?php 
                    if($d->cancelado != 1):
                    $totalkg += $d->cantidadkg;
                    $total += $d->importetotal;
                ?>
				<tr>
					<td><?php echo $d->id_compra_semilla ?></td>
					<td><?php echo $d->id_proveedor ?></td>
					<td><?php echo $d->nombre_completo ?></td>
					<td><?php echo $d->nombre_l ?></td>
                    <td><?php echo $d->cantidadkg ?></td>
					<td><?php echo $d->importetotal ?></td>
				</tr>
				<?php else: ?>
					 <tr>
						<td><?php echo $d->id_compra_semilla ?></td>
						<td><b>Cancelado</b></td>
						<td colspan="4"><?php echo $d->razon_cancelado ?></td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
        <tr>
            <td>Totales</td>
            <td></td>
            <td></td>
            <td></td>
            <td><?php echo $totalkg ?></td>
            <td><?php echo $total ?></td>
        </tr>
	</tbody>
</table>