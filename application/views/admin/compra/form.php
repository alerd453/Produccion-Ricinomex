<?php date_default_timezone_set('America/Mexico_City');  ?>
<div class="row" id="formulario">
        <div class="box box-primary">
          <div class="box-body col-md-8 col-md-offset-2">
            <form>
              <input type="text" name="id_proveedor" id="id_proveedor" hidden="true" value="<?php echo $productor->idProducores ?>">
              <div class="form-group">
                <label>Sr. (a)</label>
                <input type="text" name="nombre" placeholder="" class="form-control" required disabled="" id="nombre" value="<?php echo $productor->nombre_completo ?>">
              </div>
              <div class="form-group">
                <label>Fecha de Compra</label>
                <input type="date" name="Fecha" placeholder="" class="form-control" required id="fecha" value="<?php echo date("Y")."-".date("m")."-".date("d") ?>">
              </div>
              <div class="form-group">
                <label>Hectarias Sembradas</label>
                <input type="text" name="superficie" placeholder="" class="form-control" required disabled="" id="superficie" value="<?php echo $productor->superficie ?>">
              </div>
              <div class="form-group">
                <label>Telefono</label>
                <input type="text" name="telefono" placeholder="Telefono" class="form-control" value="<?php echo $productor->telefono?>">
              </div>
               <div class="form-group">
                <label>Poblacion</label>
                <input type="text" name="poblacion" placeholder="" class="form-control" required disabled="" id="poblacion" value="<?php echo $productor->nombre_l ?>">
              </div>
              <div class="form-group">
                <label class="radio-inline">
                  <input type="radio" name="semilla" value="9" checked>Semilla Criolla
                </label>
                <label class="radio-inline">
                  <input type="radio" name="semilla" value="12">Semilla Hibrida
                </label>
                <label class="radio-inline">
                  <input type="radio" name="semilla" value="4">Manzanita de Higuerilla
                </label>
              </div>
              <div class="form-group">
                <label>Cant (Kg.)</label>
                <input type="text" name="cantidad" placeholder="Cantidad" class="form-control cantidad" required id="kilos">
              </div>
              <div class="form-group">
                <label>$/kg.</label>
                <input type="text" name="precio" placeholder="" class="form-control" required disabled="" value="$9.00" id="precio">
              </div>
              <div class="form-group">
                <label>Importe Total</label>
                <input type="text" name="import" placeholder="" class="form-control" required id="total">
              </div>
              <input class="btn btn-info save_compra" value="Generar nota de remisión">
            </form>
          </div>
        </div>
      </div>
      <script type="text/javascript">
        //precio = 9;
        $("input[name=semilla]").click(function () {    
          //alert("La edad seleccionada es: " + $('input:radio[name=edad]:checked').val());
          //alert("La edad seleccionada es: " + $(this).val());
          if($(this).val()==9){
            precio = 9;
            $("#precio")[0].value = "$9.00";
            if($("#kilos")[0].value != ""){
              $("#total")[0].value = $("#kilos")[0].value * 9;
            }
          }else{
            if($(this).val()==4){
              precio = 4;
              $("#precio")[0].value = "$4.00";
              console.log($("#kilos")[0].value);
              if($("#kilos")[0].value != ""){
                $("#total")[0].value = $("#kilos")[0].value * 4;
              }
            }else{
              precio = 12;
              $("#precio")[0].value = "$12.00";
              console.log($("#kilos")[0].value);
              if($("#kilos")[0].value != ""){
                $("#total")[0].value = $("#kilos")[0].value * 12;
              }
            }
          }
        });
        $("#kilos").change(function(){
          precio = $("#precio").val();
          if(precio == "$9.00"){
            $("#total")[0].value = $("#kilos")[0].value * 9;
          }else{
            if(precio == "$4.00"){
              $("#total")[0].value = $("#kilos")[0].value * 4 ;
            }else{
              $("#total")[0].value = $("#kilos")[0].value * 12;
            }
          }
        });
        $('.save_compra').on("click",function(e){
          var base_url = "<?php echo base_url(); ?>";
          e.preventDefault();
          var idProductor = $("#id_proveedor").val();
          var fecha = $("#fecha").val();
          var kg = $("#kilos").val();
          var importe = $("#total").val();
          precio = $("#precio").val();
          if(precio == "$9.00"){
            $.ajax({
              url: base_url + "compra/save_compra/" + idProductor + "/" + fecha + "/" + kg + "/" + importe,
              type: "POST",
              success:function(resp){
                var id_compra = resp;
                location.href = base_url + "compra/generar_pdf/" + id_compra;
              }
            });
          }else{
            if(precio == "$4.00"){
              $.ajax({
                url: base_url + "compra/save_compram/" + idProductor + "/" + fecha + "/" + kg + "/" + importe + "/1",
                type: "POST",
                success:function(resp){
                  var id_compra = resp;
                  location.href = base_url + "compra/generar_pdfm/" + id_compra;
                }
              });
            }else{
              $.ajax({
                url: base_url + "compra/save_comprah/" + idProductor + "/" + fecha + "/" + kg + "/" + importe + "/1",
                type: "POST",
                success:function(resp){
                  var id_compra = resp;
                  location.href = base_url + "compra/generar_pdfh/" + id_compra;
                }
              });
            }
          }
        });
      </script>