<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Personal
        <small>Agregar</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Personal</li>
        <li>Agregar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	<div class="box box-solid">
    		<div class="box-body">
    			<div class="row">
                    <?php if($this->session->flashdata("error")): ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                        </div>
                    <?php endif; ?>
                    <h2 class="text-center">Agregar Nuevo Trabajador</h2>
    				<form action="<?php echo base_url();?>personal/store" method="POST" class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" id="nombre" class="form-control">    
                        </div>
                        <div class="form-group">
                            <label for="ap">Apellido Paterno</label>
                            <input type="text" name="apellidop" id="ap" class="form-control">    
                        </div>
                        <div class="form-group">
                            <label for="am">Apellido Materno</label>
                            <input type="text" name="apellidom" id="am" class="form-control">    
                        </div>
                        <div class="form-group">
                            <label for="dep">Departamento</label>
                            <select class="form-control" name="dep">
                                <?php if (!empty($departamento)):?>
                                    <?php foreach ($departamento as $der): ?>
                                        <option value="<?php echo $der->idDepartamento ?>"><?php echo $der->nombred ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>    
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success btn-flat" type="submit">Guardar</button>
                        </div>
                    </form>
    			</div>
    		</div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->