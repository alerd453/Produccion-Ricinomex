<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Personal
        <small>Reportes</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Personal</li>
        <li>Reportes</li>
      </ol>
    </section>
    <section class="content">
      <div class="box box-solid">
        <div class="box-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <ul class="nav nav-pills">
                <li class="active"><a data-toggle="pill" href="#reporte_dia_p">Reporte Dia Personal</a></li>
                <li><a data-toggle="pill" href="#reporte_dia_d">Reporte Dia Departamento</a></li>
                <li><a data-toggle="pill" href="#reporte_semana_p">Reporte Semanal Personal</a></li>
                <li><a data-toggle="pill" href="#reporte_semana_d">Reporte Semanal Departamento</a></li>
              </ul>             
              <div class="tab-content">
                <div id="reporte_dia_p" class="tab-pane fade in active">
                  <h3 class="text-center">Reporte Por Dia Personal</h3>
                  <div>
                    <div class="form-group">
                      <label>Fecha a consultar:</label>
                      <input type="date" name="fecha" class="form-control" id="consulta_dia_fecha">
                    </div>
                    <button class="btn btn-success btn-flat consulta_dia">Consultar</button>
                    <hr>
                    <canvas id="GaphicDia" width="400" height="200"></canvas>
                  </div>    
                </div>
                <br>
                <div id="reporte_semana_p" class="tab-pane fade">
                  <h3 class="text-center">Reporte Por Semana Personal</h3>
                  <div class="form-group">
                      <label>Fecha a consultar:</label>
                      <input type="date" name="fecha" class="form-control" min="2018-01-01" max="<?php echo date("Y-m-d");?>" step="7" id="consulta_semana_fecha">
                  </div>
                  <button class="btn btn-success btn-flat consulta_semana_p">Consultar</button>
                  <hr>
                  <canvas id="GaphicSem_p" width="400" height="200"></canvas>
                </div>
                <br>
                <div id="reporte_dia_d" class="tab-pane fade">
                  <h3 class="text-center">Reporte Por Dia Departamento</h3>
                  <div>
                    <div class="form-group">
                      <label>Fecha a consultar:</label>
                      <input type="date" name="fecha" class="form-control" id="consulta_dia_fecha_d">
                    </div>
                    <button class="btn btn-success btn-flat consulta_dia_d">Consultar</button>
                    <hr>
                    <canvas id="GaphicDia_d" width="400" height="200"></canvas>
                  </div>    
                </div>
                <br>
                <div id="reporte_semana_d" class="tab-pane fade">
                  <h3 class="text-center">Reporte Por Semana Departamento</h3>
                  <div class="form-group">
                      <label>Fecha a consultar:</label>
                      <input type="date" name="fecha" class="form-control" min="2018-01-01" max="<?php echo date("Y-m-d");?>" step="7" id="consulta_semana_fecha_p">
                  </div>
                  <button class="btn btn-success btn-flat consulta_semana">Consultar</button>
                  <hr>
                  <canvas id="GaphicSem" width="400" height="200"></canvas>
                </div>
              </div>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- /.content -->
    <!-- Main content -->
    <!-- <section class="content">
    	<div class="box box-solid">
    		<div class="box-body">
                <h4>Reporte diario del Personal</h4>
                <div>
                    <div class="form-group">
                        <label>Fecha a consultar:</label>
                        <input type="date" name="fecha" class="form-control" id="consulta_dia_fecha">
                    </div>
                    <button class="btn btn-success btn-flat consulta_dia" data-toggle="modal" data-target="#modal-reporte">Consultar</button>
                </div>
    		</div>
    	</div>
    </section> -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->