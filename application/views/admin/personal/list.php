<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Personal
        <small>Listado</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Personal</li>
        <li>Listado</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	<div class="box box-solid">
    		<div class="box-body">
    			<div class="row">
    				<div class="col-md-12">
    					<a href="<?php echo base_url(); ?>personal/add" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span> Agregar Personal</a>
    				</div>
    			</div>
    			<hr>
    			<div class="row">
    				<div class="col-md-12">
    					<table class="table table-bordered table-hover" id="personal">
    						<thead>
    							<th>#</th>
    							<th>Nombre</th>
    							<!--<th>Apellido Paterno</th>
    							<th>Apellido Materno</th>-->
                                <th>Departamento</th>
    							<th>Acciones</th>
    						</thead>
    						<tbody>
    							<?php if (!empty($personal)):?>
    								<?php foreach ($personal as $per): ?>
		    							<tr>
		    								<td><?php echo $per->idTrabajadores ?></td>
		    								<td><?php echo $per->nombre ?></td>
		    								<!--<td><?php echo $per->apellido_paterno ?></td>
		    								<td><?php echo $per->apellido_materno ?></td>-->
                                            <td><?php echo $per->nombred ?></td>
		    								<td>
		    									<div class="btn-group">
		    										<button type="button" class="btn btn-info btn-view" data-toggle="modal" data-target="#modal-default" value="<?php echo $per->idTrabajadores;?>">
									                <span class="fa fa-search"></span>
									                </button>
		    										<a href="<?php echo base_url();?>personal/edit/<?php echo $per->idTrabajadores;?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
		    										<a href="<?php echo base_url();?>personal/delete/<?php echo $per->idTrabajadores; ?>" class="btn btn-danger btn-remove"><span class="fa fa-remove"></span></a>
		    									</div>
		    								</td>
		    							</tr>
    								<?php endforeach; ?>
    							<?php endif; ?>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
  	<div class="modal-dialog">
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Informacion del Trabajador</h4>
	      	</div>
	      	<div class="modal-body">
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
	      	</div>
    	</div>
    <!-- /.modal-content -->
  	</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->