<!--Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Personal
        <small>Agregar Informacion</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Personal</li>
        <li>Informacion</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-solid">
        <div class="box-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <ul class="nav nav-pills">
                <?php $i=1; ?>
                <?php if (!empty($personal)):?>
                  <?php foreach ($personal as $per): ?>
                    <?php if($i==1): ?>
                      <li class="active"><a data-toggle="pill" href="#<?php echo $per->idTrabajadores?>"><?php echo $per->nombre ?></a></li>
                      <?php $i++; ?>
                    <?php else: ?>
                      <li><a data-toggle="pill" href="#<?php echo $per->idTrabajadores?>"><?php echo $per->nombre ?></a></li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                <?php endif;?>
              </ul>             
              <div class="tab-content">
                <?php $i=1; ?>
                <?php if (!empty($personal)):?>
                  <?php foreach ($personal as $per): ?>
                    <?php if($i==1): ?>
                        <div id="<?php echo $per->idTrabajadores ?>" class="tab-pane fade in active">
                      <?php $i++; ?>
                    <?php else: ?>
                      <div id="<?php echo $per->idTrabajadores ?>" class="tab-pane fade">
                    <?php endif; ?>
                    <h3 class="text-center"><?php echo $per->nombre; ?></h3>
                      <div class="form"> 
                        <?php 
                          $fechaactual=getdate();
                          $fechaactual = $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'];
                        ?>
                        <input type="text" name="fecha" value="<?php echo $fechaactual; ?>" hidden="true" id="<?php echo $per->idTrabajadores; ?>fecha">  
                        <input type="number" name="id_trabajador" value="<?php echo $per->idTrabajadores; ?>" hidden="true">
                        <div class="form-group">
                          <?php if($per->idDepartamento==1): ?>
                            <label for="cantidadC">Cantidad de Costales Descascarados</label>
                          <?php else: ?>
                            <label for="cantidadC">Cantidad de Costales Limpiados</label>
                          <?php endif; ?>
                          <input type="number" name="cantidadC" class="form-control" id="<?php echo $per->idTrabajadores; ?>cantidadc" required="true">
                        </div>
                        <div class="form-group">
                          <?php if($per->idDepartamento==1): ?>
                            <label for="cantidadk">Cantidad de Kilos Descascarados</label>
                          <?php else: ?>
                            <label for="cantidadk">Cantidad de Kilos Limpiados</label>
                          <?php endif; ?>
                          <input type="number" step="0.001" name="cantidadk" class="form-control" id="<?php echo $per->idTrabajadores; ?>cantidadk" required>
                        </div>
                        <?php $ver=1 ?>
                        <?php if(!empty($prod)): ?>
                          <?php foreach ($prod as $p): ?>
                            <?php 
                              //echo($p->idTrabajadores);
                              //echo($p->fecha);
                              $fechaactual= getdate();
                              if ($fechaactual['mon']<10 && $fechaactual['mday']<10) {
                                $fechaactual = $fechaactual['year'].'-0'.$fechaactual['mon'].'-0'.$fechaactual['mday'];
                              }elseif ($fechaactual['mon']<10 && $fechaactual['mday']>=10) {
                                $fechaactual = $fechaactual['year'].'-0'.$fechaactual['mon'].'-'.$fechaactual['mday'];
                              }elseif ($fechaactual['mon']>=10 && $fechaactual['mday']<10) {
                                $fechaactual = $fechaactual['year'].'-'.$fechaactual['mon'].'-0'.$fechaactual['mday'];
                              }else{
                                $fechaactual = $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'];
                              }
                              //echo($per->idTrabajadores);
                              //echo($fechaactual);
                            ?>
                            <?php if($p->idTrabajadores==$per->idTrabajadores && $p->fecha==$fechaactual ): ?>
                              <?php $ver=0;?>
                            <?php endif;?>
                          <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if($ver): ?>
                          <button type="button" class="btn btn-info btn-save-prod" data-toggle="tooltip" title="Guardar" data-placement="top" data-id="<?php echo $per->idTrabajadores;?>">
                            <small>Guardar</small> 
                          </button>
                        <?php else: ?>
                          <button type="button" class="btn btn-info btn-save-prod" data-toggle="tooltip" title="Ya guardo la Informacion" data-placement="top" data-id="<?php echo $per->idTrabajadores;?>" disabled="true">
                            <small>Guardar</small>
                          </button>
                        <?php endif;?>
                      </div>
                    </div>
                  <?php endforeach; ?>
                <?php endif;?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper