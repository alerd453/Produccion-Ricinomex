<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ricinomex Produccion</title>
    <link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bower_components/select2/dist/css/select2.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
    <?=$map2['js']?>                
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?php echo base_url();?>dashboard" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>R</b>MX</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>RICINO</b>MEX</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <!--<li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                //inner menu: contains the actual data
                <ul class="menu">
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>-->
                        <!-- Notifications: style can be found in dropdown.less -->
                        <!--<li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                //inner menu: contains the actual data
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>-->
                        <!-- Tasks: style can be found in dropdown.less -->
                        <!--<li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                //inner menu: contains the actual data
                <ul class="menu">
                  <li>//Task item
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  //end task item
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>-->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo base_url();?>assets/dist/img/boxed-bg.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs">
                                    <?php echo $this->session->userdata("nombre")?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php echo base_url();?>assets/dist/img/boxed-bg.jpg" class="img-circle" alt="User Image">
                                    <p>
                                        <?php echo $this->session->userdata("nombre")?>
                                        <small>-</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!--<li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                // /.row
              </li>-->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo base_url()?>auth/logout" class="btn btn-default btn-flat">Cerrar Sesion</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo base_url();?>assets/dist/img/boxed-bg.jpg" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>
                            <?php echo $this->session->userdata("nombre")?>
                        </p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Menu Principal</li>
                     <?php if($this->session->userdata("username") != "Bartolome" && $this->session->userdata("username") != "SergioA"): ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-cogs"></i><span>Produccion</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>produccion"><i class="fa fa-circle-o"></i> Inicio</a></li>
                            <li><a href="<?php echo base_url();?>reportes_produccion"><i class="fa fa-circle-o"></i> Reportes</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-shopping-cart"></i><span>Compra Semilla</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>compra"><i class="fa fa-circle-o"></i>Nueva Compra</a></li>
                            <li><a href="<?php echo base_url();?>imprimir_folio"><i class="fa fa-circle-o"></i>Reimprimir Folios</a></li>
                            <li><a href="<?php echo base_url();?>cancelar_folio"><i class="fa fa-circle-o"></i>Cancelar Folios</a></li>
                            <li><a href="<?php echo base_url();?>reporte_compra_dia"><i class="fa fa-circle-o"></i>Reporte Diario Compra</a></li>
                            <li class="treeview">
                                <a href="#"><i class="fa fa-circle-o"></i>Historial<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                                <ul class="treeview-menu">
                                    <li><a href="<?php echo base_url();?>historial/productor"><i class="fa fa-circle-o"></i>Historial por Productor</a></li>
                                    <li><a href="<?php echo base_url();?>historial/comunidad"><i class="fa fa-circle-o"></i>Historial por Comunidad </a></li>
                                </ul>
                            </li>
                            <?php if($this->session->userdata("nombre") == "SuperAdmin" || $this->session->userdata("nombre") == "Jonatan" || $this->session->userdata("nombre") == "Edgardo"): ?>
                            <li><a href="<?php echo base_url();?>auditoria"><i class="fa fa-circle-o"></i>Auditoria Compra de Semilla</a></li>
                            <?php endif; ?>
                        </ul>
                        <!--<span class="pull-right-container">
              <small class="label pull-right bg-green">Hot</small>
            </span>-->
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-truck"></i><span>Entrega Fertilizante</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <!--<li><a href="<?php echo base_url();?>entrega_2017"><i class="fa fa-circle-o"></i>Entrega 2017</a></li>-->
                            <li><a href="<?php echo base_url();?>entrega_2018"><i class="fa fa-circle-o"></i>Entrega 2018</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-users"></i><span>Personal</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>personal"><i class="fa fa-circle-o"></i> Listado</a></li>
                            <li><a href="<?php echo base_url();?>informacion"><i class="fa fa-circle-o"></i>Agregar Informacion</a></li>
                            <li><a href="<?php echo base_url();?>reportes_pers"><i class="fa fa-circle-o"></i> Reportes de Personal</a></li>
                        </ul>
                    </li>
                    <!-- <li>
          <a href="<?php echo base_url();?>personal">
            <i class="fa fa-users"></i> <span>Personal</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">Hot</small>
            </span>
          </a>
        </li> -->
      <?php endif; ?>
                    <li>
                        <a href="<?php echo base_url();?>productores">
                            <i class="fa fa-users"></i> <span>Productores</span>
                            <!--<span class="pull-right-container">
              <small class="label pull-right bg-green">Hot</small>
            </span>-->
                        </a>
                    </li><li>
          <a href="<?php echo base_url();?>parcelas">
            <i class="fa fa-globe"></i> <span>Parcelas</span>
            <!--<span class="pull-right-container">
              <small class="label pull-right bg-green">Hot</small>
            </span>-->
          </a>
        </li>
                </ul>
                <!-- search form 
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less 
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>
        <li>
          <a href="../widgets.html">
            <i class="fa fa-th"></i> <span>Widgets</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">Hot</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Charts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>UI Elements</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
            <li><a href="../UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="../UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="../UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="../UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
            <li><a href="../UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Forms</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="../forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="../forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="../tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
        <li>
          <a href="../calendar.html">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="../mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Examples</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="register.html"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
            <li><a href="500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li class="active"><a href="blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
            <li><a href="pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
        <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>-->
            </section>
            <!-- /.sidebar -->
        </aside>
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Productores
                    <small>Detalle</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
                    <li>Productores</li>
                    <li>Detalle</li>
                </ol>
            </section>
            <!-- /.header -->
            <!-- Main content -->
            <section class="content">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <?php if($this->session->flashdata("error")): ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i>
                                    <?php echo $this->session->flashdata("error"); ?>
                                </p>
                            </div>
                            <?php endif; ?>
                            <h2 class="text-center">Detalle Productor</h2>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="panel panel-default">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading">Informacion Productor</div>
                                    <div class="panel-body">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <th>#</th>
                                                <th>Apellido Paterno</th>
                                                <th>Apellido Materno</th>
                                                <th>Nombre</th>
                                                <th>CURP</th>
                                                <th>Localidad</th>
                                                <th>Municipio</th>
                                                <th>Superficie</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <?php echo $productor->idProducores ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $productor->apellido_paterno ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $productor->apellido_materno ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $productor->nombre ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $productor->curp ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $productor->nombre_l ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $productor->nombre_m ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $productor->superficie ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <a href="<?php echo base_url();?>productores/edit/<?php echo $productor->idProducores;?>" class="btn btn-warning"><span class="fa fa-pencil"></span> Editar</a>
                                        <!--<a href="<?php echo base_url();?>productores/delete/<?php echo $productor->idProducores;?>" class="btn btn-danger"><span class="fa fa-remove"></span> Eliminar</a>-->
                                        <button type="button" class="btn btn-danger btn-delete-productor" value="<?php echo $productor->idProducores; ?>"><span class="fa fa-remove"></span> Eliminar</button>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading">Informacion Parcelas</div>
                                    <div class="panel-body">
                                       <button type="button" class="btn btn-info btn-new-parcela" data-toggle="modal" data-target="#modal-default">
                                            <span class="fa fa-plus"></span> Nueva
                                        </button>
                                        <br>
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <th>#</th>
                                                <th>Paraje</th>
                                                <th>Latitud</th>
                                                <th>Longitud</th>
                                                <th>Area (ha)</th>
                                            </thead>
                                            <tbody>
                                                <?php if (!empty($parcelas)):?>
                                                <?php foreach ($parcelas as $p): ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $p->id_parcela ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $p->paraje_parcela ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $p->pos_x ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $p->pos_y ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $p->area_ha ?>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-warning btn-edit-parcela" data-toggle="modal" data-target="#modal-default" value="<?php echo $p->id_parcela;?>">
                                                            <span class="fa fa-edit"></span>
                                                        </button>
                                                        <button type="button" class="btn btn-danger btn-delete-parcela" value="<?php echo $p->id_parcela;?>">
                                                            <span class="fa fa-remove"></span>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-8 col-md-offset-2">
                                    <?=$map2['html']?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
    </div>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="TituloModal"></h4>
                </div>
                <div class="modal-body">
                  <form class="form">
                    <input type="number" name="id_productor" value="" id="id_productor" hidden="true">
                    <input type="number" name="id_parcela" value="" id="id_parcela" hidden="true">
                    <div class="form-group">
                      <label>Nombre Paraje</label>
                      <input type="text" name="paraje" class="form-control" id="paraje">
                    </div> 
                    <div class="form-group">
                      <label>Latitud</label>
                      <input type="text" name="latitud" class="form-control" id="lat">
                    </div> 
                    <div class="form-group">
                      <label>Longitud</label>
                      <input type="text" name="longitud" class="form-control" id="lon">
                    </div>
                    <div class="form-group">
                      <label>Identificador</label>
                      <input type="text" name="text_marquer" id="text_marquer" class="form-control" disabled="true"> 
                    </div>
                    <div class="form-group">
                      <label>Superficie</label>
                      <input type="number" step="0.01" name="ha" id="ha" class="form-control"> 
                    </div>
                    <div class="form-group">
                      <button type="button" class="btn btn-flat" id="btn_save_parcela">Guardar</button>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        <!-- /.modal-content -->
        </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2018 <a href="http://ricinomex.com.mx/app/">Ricinomex</a>.</strong> All rights
        reserved.
    </footer>
    <!-- jQuery 3 -->
    <!--<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>-->
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/js/productores.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <!--<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->
    <!-- FastClick -->
    <!--<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>-->
    <!-- select2 -->
    <script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!--chartjs-->
    <script src="<?php echo base_url();?>assets/bower_components/chart.js/Chart.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/productores.js"></script>
    <script type="application/javascript">
        $(document).ready(function(){
          $(".btn-new-parcela").click(function(){
            $("#TituloModal").html("Nueva Parcela");
            $("#id_productor").val(<?php echo $productor->idProducores; ?>);
            $("#text_marquer").val("<?php echo $productor->nombre_completo; ?>");
            $("#paraje").val("");
            $("#lat").val("");
            $("#lon").val("");
            $("#ha").val("");
            $("#id_parcela").val("");
          });
          $(".btn-edit-parcela").click(function(){
            var id_parcela = $(this).val();
            $("#TituloModal").html("Editar Parcela");
            $("#id_parcela").val(id_parcela);
            $("#id_productor").val(<?php echo $productor->idProducores; ?>);
            $("#text_marquer").val("<?php echo $productor->nombre_completo; ?>");
            $.ajax({
              url: "http://www.ricinomex.com.mx/produccion/productores/getparcelaid/"+ id_parcela, 
              type: "POST",
              success:function(resp){
                //console.log(resp);
                var respJSON = JSON.parse(resp);
                //console.log(respJSON[0].area_ha);
                $("#paraje").val(respJSON[0].paraje_parcela);
                $("#lat").val(respJSON[0].pos_x);
                $("#lon").val(respJSON[0].pos_y);
                $("#text_marquer").val(respJSON[0].text_marker);
                $("#ha").val(respJSON[0].area_ha);
              }
            });
          });
          $(".btn-delete-parcela").click(function(){
            var id_parcela = $(this).val();
            var opcion = confirm("¿Realmente desea eliminar esta parcela?");
            if (opcion == true) {
              $.ajax({
                url: "http://www.ricinomex.com.mx/produccion/productores/deleteparcela/"+ id_parcela, 
                type: "POST",
                success:function(resp){
                  if(resp == "Ok"){
                    location.reload();
                  }
                }
              });
            }
          });
          $(".btn-delete-productor").click(function(){
            var id_productor = $(this).val();
            var opcion = confirm("¿Realmente desea eliminar este productor?");
            if (opcion == true) {
              $.ajax({
                url: "http://www.ricinomex.com.mx/produccion/productores/delete/"+ id_productor, 
                type: "POST",
                success:function(resp){
                  if(resp == "Ok"){
                    location.href = "http://www.ricinomex.com.mx/produccion/productores";
                  }
                }
              });
            }
          });
          $("#paraje").on("change",function(){
            $("#text_marquer").val("<?php echo $productor->nombre_completo ?> - " + $("#paraje").val());
          });
          $("#btn_save_parcela").click(function(){
            var id_productor = $("#id_productor").val();
            var paraje = $("#paraje").val();
            var latitud = $("#lat").val();
            var longitud = $("#lon").val();
            var text = $("#text_marquer").val();
            var ha = $("#ha").val();
            var id_parcela = $("#id_parcela").val(); 
            if(id_parcela == ""){
              $.ajax({
                url: "http://www.ricinomex.com.mx/produccion/productores/saveparcela/"+ paraje + "/" + latitud + "/" + longitud + "/" + text + "/" + ha + "/" + id_productor, 
                type: "POST",
                success:function(resp){
                  console.log(resp);
                  if(resp == "Ok"){
                    location.reload();
                  }
                }
              });
            }else{
              $.ajax({
                url: "http://www.ricinomex.com.mx/produccion/productores/updateparcela/"+ paraje + "/" + latitud + "/" + longitud + "/" + text + "/" + ha + "/" + id_productor + "/" + id_parcela, 
                type: "POST",
                success:function(resp){
                  //console.log(resp);
                  if(resp == "Ok"){
                    location.reload();
                  }
                }
              });
            }
          });
        });
    </script>
</body>

</html>
