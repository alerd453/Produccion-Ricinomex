<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Productores
        <small>Agregar</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Productores</li>
        <li>Agregar</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    	<div class="box box-solid">
    		<div class="box-body">
    			<div class="row">
                    <?php if($this->session->flashdata("error")): ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                        </div>
                    <?php endif; ?>
                    <h2 class="text-center">Agregar Nuevo Productor</h2>
    				<form action="<?php echo base_url();?>productores/store" method="POST" class="col-md-8 col-md-offset-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="programa" checked="true" class="programa_h"> ¿El usuario pertenece al Programa Higuerrilla?
                            </label>
                        </div>
                        <hr>
                        <div style="display: block;">
                            <h3>Productor Perteneciente al Programa</h3>
                            <div class="form-group">
                                <label for="nombre">Nombre <b style="color: red;">(*)</b></label>
                                <input type="text" name="nombre" id="nombre" class="form-control" required="true">    
                            </div>
                            <div class="form-group">
                                <label for="ap">Apellido Paterno <b style="color: red;">(*)</b></label>
                                <input type="text" name="apellidop" id="ap" class="form-control" required="true">    
                            </div>
                            <div class="form-group">
                                <label for="am">Apellido Materno</label>
                                <input type="text" name="apellidom" id="am" class="form-control">    
                            </div>
                            <div class="form-group">
                                <label for="sexo">Genero <b style="color: red;">(*)</b></label>
                                <select class="form-control" name="sexo" id="sexo" required="true">
                                    <option></option>
                                    <option>F</option>
                                    <option>M</option>
                                </select>    
                            </div>
                            <div class="form-group">
                                <label for="curp">CURP</label>
                                <input type="text" class="form-control" name="curp" id="curp">
                            </div>
                            <div class="form-group">
                                <label for="telefono">Telefono</label>
                                <input type="text" class="form-control" name="telefono" id="telefono">
                            </div>
                            <div class="form-group">
                                <label for="mun">Municipio <b style="color: red;">(*)</b></label>
                                <select class="form-control" name="mun" id="mun" required="true">
                                    <option></option>
                                    <?php if (!empty($municipios)):?>
                                        <?php foreach ($municipios as $m): ?>
                                            <option value="<?php echo $m->id ?>"><?php echo $m->nombre_m ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>    
                            </div>
                            <div class="form-group">
                                <label for="loc">Localidad <b style="color: red;">(*)</b></label>
                                <select class="form-control" name="loc" id="loc" required="true">
                                    <option></option>
                                </select>    
                            </div>
                            <div class="form-group">
                                <label for="superficie">Superficie <b style="color: red;">(*)</b></label>
                                <input type="number" name="superficie" step="0.01" required="true" class="form-control" required="true">
                            </div>
                            <div class="form-group">
                                <label for="aprobado"> Aprobado</label>
                                <input type="number" name="aprobado" step="0.01" class="form-control" id="aprobado">
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success btn-flat" type="submit">Guardar</button>
                        </div>
                    </form>
    			</div>
    		</div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
