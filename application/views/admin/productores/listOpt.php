<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Productor
        <small>Listado</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Incio</a></li>
        <li>Productor</li>
        <li>Listado</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo base_url(); ?>productores/add" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span> Agregar Productor</a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 col-md-offset-8">
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" name="busqueda" placeholder="Buscar algo" />
                            <i class="glyphicon glyphicon-search form-control-feedback"></i>
                        </div>
                        
                    </div>
                    <div class="col-md-12 table-responsive">
                        <p>
                            <strong>Mostrar por : </strong>
                            <select name="cantidad" id="cantidad">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="250">250</option>
                                <option value="500">500</option>
                            </select>
                        </p>
                        <table class="table table-bordered table-hover" id="tbproductores">
                            <thead>
                                <th>#</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Nombre</th>
                                <th>CURP</th>
                                <th>Localidad</th>
                                <th>Municipio</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div class="text-center paginacion">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Informacion del Productor</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-rigth" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->