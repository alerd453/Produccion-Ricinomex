<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url();?>assets/dist/img/boxed-bg.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $this->session->userdata("nombre")?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">Menu Principal</li>
      <?php if($this->session->userdata("username") != "Bartolome" && $this->session->userdata("username") != "SergioA"): ?>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-cogs"></i><span>Produccion</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>produccion"><i class="fa fa-circle-o"></i> Inicio</a></li>
          <li><a href="<?php echo base_url();?>reportes_produccion"><i class="fa fa-circle-o"></i> Reportes</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-shopping-cart"></i><span>Compra Semilla</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>compra"><i class="fa fa-circle-o"></i>Nueva Compra</a></li>
          <li><a href="<?php echo base_url();?>imprimir_folio"><i class="fa fa-circle-o"></i>Reimprimir Folios</a></li>
          <li><a href="<?php echo base_url();?>cancelar_folio"><i class="fa fa-circle-o"></i>Cancelar Folios</a></li>
          <li><a href="<?php echo base_url();?>reporte_compra_dia"><i class="fa fa-circle-o"></i>Reporte Diario Compra</a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>Historial<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url();?>historial/productor"><i class="fa fa-circle-o"></i>Historial por Productor</a></li>
              <li><a href="<?php echo base_url();?>historial/comunidad"><i class="fa fa-circle-o"></i>Historial por Comunidad </a></li>
            </ul>
          </li>
          <?php if($this->session->userdata("nombre") == "SuperAdmin" || $this->session->userdata("nombre") == "Jonatan" || $this->session->userdata("nombre") == "Edgardo"): ?>
            <li><a href="<?php echo base_url();?>auditoria"><i class="fa fa-circle-o"></i>Auditoria Compra de Semilla</a></li>
          <?php endif; ?> 
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-credit-card"></i><span>Ventas</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>ventas"><i class="fa fa-circle-o"></i>Nueva Venta</a></li>
          <li><a href="<?php echo base_url();?>ventas/reimprimir"><i class="fa fa-circle-o"></i>Reimprimir Ventas</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-truck"></i><span>Entrega Fertilizante</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>entrega_2018"><i class="fa fa-circle-o"></i>Entrega 2018</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i><span>Personal</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>personal"><i class="fa fa-circle-o"></i> Listado</a></li>
          <li><a href="<?php echo base_url();?>informacion"><i class="fa fa-circle-o"></i>Agregar Informacion</a></li>
          <li><a href="<?php echo base_url();?>reportes_pers"><i class="fa fa-circle-o"></i> Reportes de Personal</a></li>
        </ul>
      </li>
      <?php endif; ?>
      <li>
        <a href="<?php echo base_url();?>productores">
          <i class="fa fa-users"></i> <span>Productores</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>parcelas">
          <i class="fa fa-globe"></i> <span>Parcelas</span>
        </a>
      </li>
    </ul>
  </section>
</aside>