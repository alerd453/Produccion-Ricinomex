  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 <a href="http://ricinomex.com.mx/app/">Ricnomex</a>.</strong> All rights
    reserved.
  </footer>

  <!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<!--<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->
<!-- FastClick -->
<!--<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>-->
<!-- select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!--chartjs-->
<script src="<?php echo base_url();?>assets/bower_components/chart.js/Chart.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<script>
  $(document).ready(function () {
    var base_url = "<?php echo base_url(); ?>";
    function aleatorio(inferior,superior){
       numPosibilidades = superior - inferior
       aleat = Math.random() * numPosibilidades
       aleat = Math.floor(aleat)
       return parseInt(inferior) + aleat
    } 
    function dame_color_aleatorio(){
      hexadecimal = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F")
      color_aleatorio = "#";
      for (i=0;i<6;i++){
        posarray = aleatorio(0,hexadecimal.length)
        color_aleatorio += hexadecimal[posarray]
      }
      return color_aleatorio
    }
    $('.btn-chose-productor').on("click",function(){
      var id=$(this).val();
      $.ajax({
        url: base_url + "compra/view/" + id,
        type: "POST",
        success:function(resp){
          $("#modal-default .modal-body").html(resp);
        }
      });
    });
    $(".consulta_semana_p").on("click",function(e){
      e.preventDefault();
      var canvas = document.getElementById("GaphicSem_p");
      var ctx = canvas.getContext("2d");  
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      var sem = $("#consulta_semana_fecha").val();
      $.ajax({
        url: base_url + "/reportes_pers/consulta_semana_past/" + sem,
        type : "POST",
        success: function(resp){
          var respJSON = JSON.parse(resp);
          respDJSON = respJSON['semana'];
          console.log(respDJSON);
          var arrayfechas = [];
          var arraypersonal = [];
          var arraycolores = [];
          var arraydataset = [];
          var arraykilos = [];
          for(i=0;i<respDJSON.length;i++){
            if(arrayfechas.length == 0){
              arrayfechas.push(respDJSON[i]["fecha"]);
            }
            if(arrayfechas.length > 0){
              if(arrayfechas.indexOf(respDJSON[i]["fecha"])==-1){
                arrayfechas.push(respDJSON[i]["fecha"]);
              }
            }
          }
          console.log(arrayfechas);
          for(i=0;i<respDJSON.length;i++){
            if(arraypersonal.length == 0){
              //console.log(respDJSON[i]["nombre"]);
              arraypersonal.push(respDJSON[i]["nombre"]+" ("+respDJSON[i]["nombred"]+")");
              //arraycolores.push(dame_color_aleatorio());
              /*var personal = new Object();
              personal.label = respDJSON[i]["nombre"];
              personal.backgroundColor = dame_color_aleatorio();
              arraydataset.push(personal);*/
            }
            if(arraypersonal.indexOf(respDJSON[i]["nombre"]+" ("+respDJSON[i]["nombred"]+")")==-1){
              arraypersonal.push(respDJSON[i]["nombre"]+" ("+respDJSON[i]["nombred"]+")");
              //arraycolores.push(dame_color_aleatorio());
            }

          }
          console.log(arraypersonal);
          arraypersonal.forEach(function(element){
            arraycolores.push(dame_color_aleatorio());
          });
          var i = 0;
          arraypersonal.forEach(function(personal){
            var j = 0;
            var arraytemp = [];
            respDJSON.forEach(function(element){
              if(personal == element["nombre"]+" ("+element["nombred"]+")"){
                arraytemp[j] = element["kilos"];
                j++;
              }
            });
            arraykilos[i] = arraytemp.slice();
            i++;
          });
          console.log(arraykilos);
          var i = 0;
          var arreglofinal = [];
          arraypersonal.forEach(function(personal){
            var object = new Object();
            object.label = personal;
            object.backgroundColor = arraycolores[i];
            object.data = arraykilos[i];
            arreglofinal[i] = jQuery.extend(true, {}, object);
            i++;
            //console.log(object);
          });
          console.log(arreglofinal);
          /*for (i=0; i<arrayfechas.length; i++) {
            sumkgbyfechaP[i] = 0;
            sumkgbyfechaL[i] = 0
            for(j=0; j<respPJSON.length; j++){
              if(arrayfechas[i]==respPJSON[j]["fecha"]){
                sumkgbyfechaP[i] += parseInt(respPJSON[j]["kilos"]);
              }
            }
          }*/
          var ctx = document.getElementById("GaphicSem_p").getContext("2d");
          var mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: arrayfechas,
              datasets: arreglofinal
            },
            options: {
              legend: {
                display: true,
                position: 'top',
                labels: {
                  fontColor: "#000080",
                }
              },
              title: {
                display: true,
                text: 'Trabajo Realizado por Semana Personal (Kilos)'
              },
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true
                  }
                }]
              }
            }
          });
        }
      });
    });
    $(".consulta_semana").on("click",function(e){
      e.preventDefault();
      var date = $("#consulta_semana_fecha_p").val();
      var arrayfechas = new Array();
      var sumkgbyfechaP = new Array();
      var sumkgbyfechaL = new Array();
      $.ajax({
        url: base_url + "/reportes_pers/consulta_semana_p/" + date,
        type : "POST",
        success: function(resp){
          var respJSON = JSON.parse(resp);
          respPJSON = respJSON['produccion'];
          respLJSON = respJSON['limpieza'];
          console.log(respLJSON);
          for(i=0;i<respPJSON.length;i++){
            if(arrayfechas.length == 0){
              arrayfechas.push(respPJSON[i]["fecha"]);
            }
            if(arrayfechas.length > 0){
              if(arrayfechas.indexOf(respPJSON[i]["fecha"])==-1){
                arrayfechas.push(respPJSON[i]["fecha"]);
              }
            }
          }
          for(i=0;i<respLJSON.length;i++){
            if(arrayfechas.length == 0){
              arrayfechas.push(respLJSON[i]["fecha"]);
            }
            if(arrayfechas.length > 0){
              if(arrayfechas.indexOf(respLJSON[i]["fecha"])==-1){
                arrayfechas.push(respLJSON[i]["fecha"]);
              }
            }
          }
          for (i=0; i<arrayfechas.length; i++) {
            sumkgbyfechaP[i] = 0;
            sumkgbyfechaL[i] = 0
            for(j=0; j<respPJSON.length; j++){
              if(arrayfechas[i]==respPJSON[j]["fecha"]){
                sumkgbyfechaP[i] += parseInt(respPJSON[j]["kilos"]);
              }
            }
            for(j=0; j<respLJSON.length; j++){
              if(arrayfechas[i]==respLJSON[j]["fecha"]){
                sumkgbyfechaL[i] += parseInt(respLJSON[j]["kilos"]);
              }
            }
          }
          var ctx = document.getElementById("GaphicSem").getContext("2d");
          var mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: arrayfechas,
              datasets: [{
                label: 'Descascarado',
                backgroundColor: "#000080",
                data: sumkgbyfechaP
              }, {
                label: 'Limpieza',
                backgroundColor: "#add8e6",
                data: sumkgbyfechaL
              }]
            },
            options: {
              legend: {
                display: true,
                position: 'top',
                labels: {
                  fontColor: "#000080",
                }
              },
              title: {
                display: true,
                text: 'Trabajo Realizado por Semana Departamento (Kilos)'
              },
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true
                  }
                }]
              }
            }
          });
        }
      });
    });
    /* Reporte Dia Departamento */
    $(".consulta_dia_d").on("click",function(e){
      e.preventDefault();
      var date = $("#consulta_dia_fecha_d").val();
      var arrayfechas = new Array();
      var sumkgbyfechaP = new Array();
      var sumkgbyfechaL = new Array();
      $.ajax({
        url: base_url + "/reportes_pers/consulta_dia_d/" + date,
        type : "POST",
        success: function(resp){
          var respJSON = JSON.parse(resp);
          respPJSON = respJSON['produccion'];
          respLJSON = respJSON['limpieza'];
          /*console.log(respPJSON);
          console.log(respLJSON);*/
          for(i=0;i<respPJSON.length;i++){
            if(arrayfechas.length == 0){
              arrayfechas.push(respPJSON[i]["fecha"]);
            }
            if(arrayfechas.length > 0){
              if(arrayfechas.indexOf(respPJSON[i]["fecha"])==-1){
                arrayfechas.push(respPJSON[i]["fecha"]);
              }
            }
          }
          for(i=0;i<respLJSON.length;i++){
            if(arrayfechas.length == 0){
              arrayfechas.push(respLJSON[i]["fecha"]);
            }
            if(arrayfechas.length > 0){
              if(arrayfechas.indexOf(respLJSON[i]["fecha"])==-1){
                arrayfechas.push(respLJSON[i]["fecha"]);
              }
            }
          }
          //console.log(arrayfechas[0]);
          for (i=0; i<arrayfechas.length; i++) {
            sumkgbyfechaP[i] = 0;
            sumkgbyfechaL[i] = 0
            for(j=0; j<respPJSON.length; j++){
              if(arrayfechas[i]==respPJSON[j]["fecha"]){
                sumkgbyfechaP[i] += parseInt(respPJSON[j]["kilos"]);
              }
            }
            for(j=0; j<respLJSON.length; j++){
              if(arrayfechas[i]==respLJSON[j]["fecha"]){
                sumkgbyfechaL[i] += parseInt(respLJSON[j]["kilos"]);
              }
            }
          }
          var ctx = document.getElementById("GaphicDia_d").getContext("2d");
          var mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: arrayfechas,
              datasets: [{
                label: 'Descascarado',
                backgroundColor: "#000080",
                data: sumkgbyfechaP
              }, {
                label: 'Limpieza',
                backgroundColor: "#add8e6",
                data: sumkgbyfechaL
              }]
            },
            options: {
              legend: {
                display: true,
                position: 'top',
                labels: {
                  fontColor: "#000080",
                }
              },
              title: {
                display: true,
                text: 'Trabajo Realizado por Dia Departamento (Kilos)'
              },
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true
                  }
                }]
              }
            }
          });
        }
      });
    });
    $(".consulta_dia").on("click",function(e){
      e.preventDefault();
      var date = $("#consulta_dia_fecha").val();
      $.ajax({
        url: base_url + "/reportes_pers/consulta_dia/" + date,
        type : "POST",
        success: function(resp){
          var respJSON = JSON.parse(resp);
          respDJSON = respJSON['dia'];
          var arraydata = new Array();
          var arraycolors = new Array();
          var arraylabel = new Array();
          respDJSON.forEach(function(element) {
            //console.log(element["kilos"]);
            arraydata.push(element["kilos"]);
            arraycolors.push(dame_color_aleatorio());
            arraylabel.push(element["nombre"]+" ("+element["nombred"]+")");
          });
          var datos = {
            type: "pie",
            data : {
              datasets :[{
                data : arraydata,
                backgroundColor: arraycolors,
              }],
              labels : arraylabel
            },
            options : {
              title: {
                display: true,
                text: 'Trabajo Realizado por Dia Personal (Kilos)'
              },
              responsive : true,
            }
          };

          var canvas = document.getElementById('GaphicDia').getContext('2d');
          window.pie = new Chart(canvas, datos);
        }
      });
    });

    $(".btn-save-prod").on("click",function(e){
      e.preventDefault();
      var id = $(this).data().id;
      var fecha = $("#"+id+"fecha").val();
      var cantidadc = $("#"+id+"cantidadc").val();
      var cantidadk = $("#"+id+"cantidadk").val(); 
      console.log(base_url + "personal/productividad/"+id+"/"+fecha+"/"+cantidadc+"/"+cantidadk);
      $.ajax({
        url: base_url + "personal/productividad/"+id+"/"+fecha+"/"+cantidadc+"/"+cantidadk,
        type: "POST",
        success: function(resp){
          location.reload();
        }
      });
    });
    /*BOTONES QUE CONTROLAN EL FLUJO DEL PROCESO*/
    $(".btn-view-s0").on("click",function(){
      /*PASAR DE ESTADO 0(INICIAL) A 1(DESCASCARANDO)*/
      var id=$(this).val();
      array = id.split("|");
      id_proceso = array[0];
      id_productor = array[1];
      $("#modal-0 .modal-body #id_proceso").val(id_proceso);
      $("#modal-0 .modal-body #id_productor").val(id_productor);
      $.ajax({
        url: base_url + "produccion/trabajador_maquina/",
        type: "POST",
        success:function(resp){
          $("#modal-0 .modal-body #contenido0").html(resp);
          $("#trabajador").select2({
            placeholder: 'Seleccione un Trabajador'
          });
          $("#maquina").select2({
            placeholder: 'Seleccione una Maquina'
          });
        }
      });
    });
    $(".btn-view-s1-pause").on("click",function(e){
      /*PAUSAR PROCESO DE DESCASCARADO*/
      e.preventDefault();
      var id = $(this).data().id;
      var button = $(this);
      console.log(button);
      $.ajax({
        url: base_url + "produccion/pausa_descascarado/"+id,
        type: "POST",
        success:function(resp){
          location.reload();
        }
      });
    });
    $(".btn-view-s1-play").on("click",function(e){
      /*CONTINUAR PROCESO DE DESCASCARADO*/
      e.preventDefault();
      var id = $(this).data().id;
      $.ajax({
        url: base_url + "produccion/play_descascarado/"+id,
        type: "POST",
        success:function(resp){
          location.reload();
        }
      });
    });
    $(".btn-view-s1-stop").on("click",function(e){
      /*TERMINAR PROCESO DE DESCASCARADO*/
      e.preventDefault();
      var id = $(this).data().id;
      array = id.split("|");
      id_proceso = array[0];
      id_productor = array[1];
      $("#modal-1 .modal-body #id_proceso").val(id_proceso);
      $("#modal-1 .modal-body #id_productor").val(id_productor);
    });
    $(".btn-view-s2").on("click",function(){
      /*PASAR DE ESTADO 2(LIMPIAR) A 3(LIMPIANDO)*/
      var id=$(this).val();
      array = id.split("|");
      id_proceso = array[0];
      id_productor = array[1];
      $("#modal-2 .modal-body #id_proceso").val(id_proceso);
      $("#modal-2 .modal-body #id_productor").val(id_productor);
      $.ajax({
        url: base_url + "produccion/trabajador_limp/",
        type: "POST",
        success:function(resp){
          $("#modal-2 .modal-body #contenido0").html(resp);
          $("#trabajador").select2({
            placeholder: 'Seleccione un Trabajador'
          });
        }
      });
    });
    $(".btn-view-s3-pause").on("click",function(e){
      /*PAUSAR PROCESO DE LIMPIEZA*/
      e.preventDefault();
      var id = $(this).data().id;
      var button = $(this);
      console.log(button);
      $.ajax({
        url: base_url + "produccion/pausa_limpieza/"+id,
        type: "POST",
        success:function(resp){
          location.reload();
        }
      });
    });
    $(".btn-view-s3-play").on("click",function(e){
      /*CONTINUAR PROCESO DE DESCASCARADO*/
      e.preventDefault();
      var id = $(this).data().id;
      $.ajax({
        url: base_url + "produccion/play_limpieza/"+id,
        type: "POST",
        success:function(resp){
          location.reload();
        }
      });
    });
    $(".btn-view-s3-stop").on("click",function(e){
      /*TERMINAR PROCESO DE LIMPIEZA*/
      e.preventDefault();
      var id = $(this).data().id;
      array = id.split("|");
      id_proceso = array[0];
      id_productor = array[1];
      $("#modal-3 .modal-body #id_proceso").val(id_proceso);
      $("#modal-3 .modal-body #id_productor").val(id_productor);
    });
    $(".btn-view-s4").on("click",function(){
      /*PASAR DE ESTADO 4(DISPONIBLE A PAGAR) A 5(PAGADO)*/
      var id=$(this).val();
      //alert(id);
      array = id.split("|");
      id_proceso = array[0];
      id_productor = array[1];
      $("#modal-4 .modal-body #id_proceso").val(id_proceso);
      $("#modal-4 .modal-body #id_productor").val(id_productor);
      $.ajax({
        url: base_url + "produccion/info_entrega/"+id_proceso+"/"+id_productor,
        type: "POST",
        success:function(resp){
          console.log(resp);
          $("#modal-4 .modal-body #contenido1").html(resp);
        }
      });
    });
    $(".btn-view-s5").on("click",function(){
      /*VER INFORMACION DE PAGO*/
      var id=$(this).val();
      $.ajax({
        url: base_url + "produccion/info_compra/" + id,
        type: "POST",
        success: function(resp){
          $("#modal-5 .modal-body").html(resp);
        }
      });
    });
    $(".btn-view-s6").on("click",function(){
      /*PASAR DE ESTADO 6(SECAR) A 8(SECANDO) SIN PASAR POR ESTADO 7*/  
      var id = $(this).data().id;
      array = id.split("|");
      console.log(array);
      id_proceso = array[0];
      id_productor = array[1];
      $.ajax({
        url: base_url + "produccion/secado/"+id_proceso+"/"+id_productor,
        type: "POST",
        success:function(resp){
          location.reload();
        }
      });
    });
    $(".btn-view-s7").on("click",function(){
      /*VER INFORMACION DE CANCELACION*/
      var id=$(this).val();
    });
    $(".btn-view-s8-stop").on("click",function(e){
      /*TERMINAR PROCESO DE SECADO*/
      e.preventDefault();
      var id_proceso = $(this).data().id;
      $.ajax({
        url: base_url + "produccion/secado_fin/"+id_proceso,
        type: "POST",
        success:function(resp){
          location.reload();
        }
      });
    });
    /*FIN BOTONES QUE CONTROLAN EL FLUJO DEL PROCESO*/
    $("#mun").select2({
      placeholder: 'Seleccione un Municipio'
    });
    $("#mun").change(function(){
      var munselect = $("#mun").val();
      $.ajax({
        url : base_url + "productores/loc/" + munselect,
        type : "POST",
        success : function(res){
          $('#loc').children('option').remove();
          $("#loc").append(res);
          $("#loc").select2({
            placeholder: 'Seleccione una Localidad'
          });
        },
        error : function(res){
          console.log(res);
        }
      });
    });
    $("#loc").select2({
      placeholder: 'Seleccione una Localidad'
    });
    $("#sexo").select2({
      placeholder: 'Seleccione el Genero'
    });
    $('.sidebar-menu').tree();
    $('.btn-form-prod').on("click",function(){
      var id=$(this).val();
      $.ajax({
        url: base_url + "produccion/chose/" + id,
        type: "POST",
        success:function(resp){
          $("#modal-default .modal-body").html(resp);
        }
      });
    });
    $('.btn-view-productor').on("click",function(){
      var id=$(this).val();
      $.ajax({
        url: base_url + "productores/view/" + id,
        type: "POST",
        success:function(resp){
          $("#modal-default .modal-body").html(resp);
        }
      });
    });
    $('.btn-view').on("click",function(){
      var id=$(this).val();
      $.ajax({
        url: base_url + "personal/view/" + id,
        type: "POST",
        success:function(resp){
          $("#modal-default .modal-body").html(resp);
        }
      });
    });
    $('.btn-remove').on("click",function(e){
      e.preventDefault();
      var ruta =  $(this).attr("href");
      $.ajax({
        url: ruta,
        type: "POST",
        success:function(resp){
          window.location.href = base_url + resp;
        }
      });
    });
    $('#personal').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
  })
</script>
</body>
</html>