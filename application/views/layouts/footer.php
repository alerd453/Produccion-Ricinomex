<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.3
    </div>
    <strong>Copyright &copy; 2018 <a href="http://ricinomex.com.mx/app/">Ricinomex</a>.</strong> All rights
    reserved.
</footer>
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/js/productores.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<!--<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->
<!-- FastClick -->
<!--<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>-->
<!-- select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!--chartjs-->
<script src="<?php echo base_url();?>assets/bower_components/chart.js/Chart.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        var base_url = "<?php echo base_url(); ?>";
        function intlRound(numero, decimales = 2, usarComa = false) {
            var opciones = {
                maximumFractionDigits: decimales, 
                useGrouping: false
            };
            usarComa = usarComa ? "es" : "en";
            return new Intl.NumberFormat(usarComa, opciones).format(numero);
        }
        function addZero(i) {
            if (i < 10) {
                i = '0' + i;
            }
            return i;
        }
        function hoyFecha(){
            var hoy = new Date();
            var dd = hoy.getDate();
            var mm = hoy.getMonth()+1;
            var yyyy = hoy.getFullYear();
            
            dd = addZero(dd);
            mm = addZero(mm);

            return yyyy+'-'+mm+'-'+dd;
        }
        function aleatorio(inferior, superior) {
            numPosibilidades = superior - inferior
            aleat = Math.random() * numPosibilidades
            aleat = Math.floor(aleat)
            return parseInt(inferior) + aleat
        }

        function dame_color_aleatorio() {
            hexadecimal = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F")
            color_aleatorio = "#";
            for (i = 0; i < 6; i++) {
                posarray = aleatorio(0, hexadecimal.length)
                color_aleatorio += hexadecimal[posarray]
            }
            return color_aleatorio
        }
        $('.js-example-basic-single').select2();
        $(".programa_h").on('change', function() {
            if ($(this).is(':checked')) {
                $("#curp").removeAttr('disabled');
                $("#aprobado").removeAttr('disabled');
            } else {
                $("#curp").attr('disabled', 'disabled');
                $("#aprobado").attr('disabled', 'disabled');
            }
        });
        $('.boton-consulta-localidad').on("click", function(e) {
            e.preventDefault();
            var localidad = $("#localidades_consul").val();
            $.ajax({
                url: base_url + "historial/ver_localidad/" + localidad,
                type: "POST",
                success: function(resp) {
                    console.log(resp);
                    $("#cuerpo_localidades").html(resp);
                }
            });
            $('#personal').DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por pagina",
                    "zeroRecords": "No se encontraron resultados en su busqueda",
                    "searchPlaceholder": "Buscar registros",
                    "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                    "infoEmpty": "No existen registros",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                }
            });
        });
        $('.btn-chose-productor').on("click", function() {
            var id = $(this).val();
            $.ajax({
                url: base_url + "compra/view/" + id,
                type: "POST",
                success: function(resp) {
                    $("#modal-default .modal-body").html(resp);
                }
            });
        });
        $('.btn-chose-productor-historial').on("click", function() {
            var id = $(this).val();
            $.ajax({
                url: base_url + "historial/ver_productor/" + id,
                type: "POST",
                success: function(resp) {
                    $("#modal-default .modal-body").html(resp);
                }
            });
        });
        $("#productoselec").on("change", function(){
            var producto = $("#productoselec").val();
            if(producto == "Aceite"){
                $("#precioventa").val("38.00");
            }else{
                if (producto == "Pasta") {
                    $("#precioventa").val("6.00");
                }else{
                    if (producto == "Seleccione algun producto") {
                        $("#precioventa").val("");
                    }else{
                        $("#precioventa").val("");
                    }
                }
            }
            if($("#precioventa").val() != "" && $("#cantidadventa").val() != ""){
                var precio = $("#precioventa").val();
                var cantidad = $("#cantidadventa").val();
                var importetotal =  precio * cantidad; 
                $("#importeventa").val(importetotal);
            }else{
                $("#importeventa").val("");   
            }
        });
        $("#cantidadventa").on("change", function(){
            if($("#precioventa").val() != "" && $("#cantidadventa").val() != ""){
                var precio = $("#precioventa").val();
                var cantidad = $("#cantidadventa").val();
                var importetotal =  precio * cantidad; 
                $("#importeventa").val(importetotal);
            }else{
                $("#importeventa").val("");   
            }
        });
        /*$("#precioventa").on("change", function(){
            if($("#precioventa").val() != "" && $("#cantidadventa").val() != ""){
                var precio = $("#precioventa").val();
                var cantidad = $("#cantidadventa").val();
                var importetotal =  precio * cantidad; 
                $("#importeventa").val(importetotal);
            }else{
                $("#importeventa").val("");
            }
        });
        $("#cantidadventa").on("change", function(){
            if($("#precioventa").val() != "" && $("#cantidadventa").val() != ""){
                var precio = $("#precioventa").val();
                var cantidad = $("#cantidadventa").val();
                var importetotal =  precio * cantidad; 
                $("#importeventa").val(importetotal);
            }else{
                $("#importeventa").val("");   
            }
        });*/
        /*$("#importeventa").focus(function(){
            alert("focus"); 
            var precio = $("#precioventa").val();
            var cantidad = $("#cantidadventa").val();
            console.log(precio + " - " + cantidad);
        });*/
        $('.btn-auditar').on("click", function(e) {
            var obj = $(this).val();
            var obk = obj.split('-');
            console.log(obk);
            var id = obk[0];
            var type = obk[1];
            $.ajax({
                url: base_url + "auditoria/auditando/" + id + "/" + type,
                type: "POST",
                success: function(resp) {
                    $("#modal-default .modal-body").html(resp);
                }
            });
        });
        $(".consulta_semana_p").on("click", function(e) {
            e.preventDefault();
            var canvas = document.getElementById("GaphicSem_p");
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            var sem = $("#consulta_semana_fecha").val();
            $.ajax({
                url: base_url + "/reportes_pers/consulta_semana_past/" + sem,
                type: "POST",
                success: function(resp) {
                    var respJSON = JSON.parse(resp);
                    respDJSON = respJSON['semana'];
                    console.log(respDJSON);
                    var arrayfechas = [];
                    var arraypersonal = [];
                    var arraycolores = [];
                    var arraydataset = [];
                    var arraykilos = [];
                    for (i = 0; i < respDJSON.length; i++) {
                        if (arrayfechas.length == 0) {
                            arrayfechas.push(respDJSON[i]["fecha"]);
                        }
                        if (arrayfechas.length > 0) {
                            if (arrayfechas.indexOf(respDJSON[i]["fecha"]) == -1) {
                                arrayfechas.push(respDJSON[i]["fecha"]);
                            }
                        }
                    }
                    console.log(arrayfechas);
                    for (i = 0; i < respDJSON.length; i++) {
                        if (arraypersonal.length == 0) {
                            //console.log(respDJSON[i]["nombre"]);
                            arraypersonal.push(respDJSON[i]["nombre"] + " (" + respDJSON[i]["nombred"] + ")");
                            //arraycolores.push(dame_color_aleatorio());
                            /*var personal = new Object();
                            personal.label = respDJSON[i]["nombre"];
                            personal.backgroundColor = dame_color_aleatorio();
                            arraydataset.push(personal);*/
                        }
                        if (arraypersonal.indexOf(respDJSON[i]["nombre"] + " (" + respDJSON[i]["nombred"] + ")") == -1) {
                            arraypersonal.push(respDJSON[i]["nombre"] + " (" + respDJSON[i]["nombred"] + ")");
                            //arraycolores.push(dame_color_aleatorio());
                        }
                    }
                    console.log(arraypersonal);
                    arraypersonal.forEach(function(element) {
                        arraycolores.push(dame_color_aleatorio());
                    });
                    var i = 0;
                    arraypersonal.forEach(function(personal) {
                        var j = 0;
                        var arraytemp = [];
                        respDJSON.forEach(function(element) {
                            if (personal == element["nombre"] + " (" + element["nombred"] + ")") {
                                arraytemp[j] = element["kilos"];
                                j++;
                            }
                        });
                        arraykilos[i] = arraytemp.slice();
                        i++;
                    });
                    console.log(arraykilos);
                    var i = 0;
                    var arreglofinal = [];
                    arraypersonal.forEach(function(personal) {
                        var object = new Object();
                        object.label = personal;
                        object.backgroundColor = arraycolores[i];
                        object.data = arraykilos[i];
                        arreglofinal[i] = jQuery.extend(true, {}, object);
                        i++;
                        //console.log(object);
                    });
                    console.log(arreglofinal);
                    /*for (i=0; i<arrayfechas.length; i++) {
                      sumkgbyfechaP[i] = 0;
                      sumkgbyfechaL[i] = 0
                      for(j=0; j<respPJSON.length; j++){
                        if(arrayfechas[i]==respPJSON[j]["fecha"]){
                          sumkgbyfechaP[i] += parseInt(respPJSON[j]["kilos"]);
                        }
                      }
                    }*/
                    var ctx = document.getElementById("GaphicSem_p").getContext("2d");
                    var mybarChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: arrayfechas,
                            datasets: arreglofinal
                        },
                        options: {
                            legend: {
                                display: true,
                                position: 'top',
                                labels: {
                                    fontColor: "#000080",
                                }
                            },
                            title: {
                                display: true,
                                text: 'Trabajo Realizado por Semana Personal (Kilos)'
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                }
            });
        });
        $(".consulta_semana").on("click", function(e) {
            e.preventDefault();
            var date = $("#consulta_semana_fecha_p").val();
            var arrayfechas = new Array();
            var sumkgbyfechaP = new Array();
            var sumkgbyfechaL = new Array();
            $.ajax({
                url: base_url + "/reportes_pers/consulta_semana_p/" + date,
                type: "POST",
                success: function(resp) {
                    var respJSON = JSON.parse(resp);
                    respPJSON = respJSON['produccion'];
                    respLJSON = respJSON['limpieza'];
                    console.log(respLJSON);
                    for (i = 0; i < respPJSON.length; i++) {
                        if (arrayfechas.length == 0) {
                            arrayfechas.push(respPJSON[i]["fecha"]);
                        }
                        if (arrayfechas.length > 0) {
                            if (arrayfechas.indexOf(respPJSON[i]["fecha"]) == -1) {
                                arrayfechas.push(respPJSON[i]["fecha"]);
                            }
                        }
                    }
                    for (i = 0; i < respLJSON.length; i++) {
                        if (arrayfechas.length == 0) {
                            arrayfechas.push(respLJSON[i]["fecha"]);
                        }
                        if (arrayfechas.length > 0) {
                            if (arrayfechas.indexOf(respLJSON[i]["fecha"]) == -1) {
                                arrayfechas.push(respLJSON[i]["fecha"]);
                            }
                        }
                    }
                    for (i = 0; i < arrayfechas.length; i++) {
                        sumkgbyfechaP[i] = 0;
                        sumkgbyfechaL[i] = 0
                        for (j = 0; j < respPJSON.length; j++) {
                            if (arrayfechas[i] == respPJSON[j]["fecha"]) {
                                sumkgbyfechaP[i] += parseInt(respPJSON[j]["kilos"]);
                            }
                        }
                        for (j = 0; j < respLJSON.length; j++) {
                            if (arrayfechas[i] == respLJSON[j]["fecha"]) {
                                sumkgbyfechaL[i] += parseInt(respLJSON[j]["kilos"]);
                            }
                        }
                    }
                    var ctx = document.getElementById("GaphicSem").getContext("2d");
                    var mybarChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: arrayfechas,
                            datasets: [{
                                label: 'Descascarado',
                                backgroundColor: "#000080",
                                data: sumkgbyfechaP
                            }, {
                                label: 'Limpieza',
                                backgroundColor: "#add8e6",
                                data: sumkgbyfechaL
                            }]
                        },
                        options: {
                            legend: {
                                display: true,
                                position: 'top',
                                labels: {
                                    fontColor: "#000080",
                                }
                            },
                            title: {
                                display: true,
                                text: 'Trabajo Realizado por Semana Departamento (Kilos)'
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                }
            });
        });
        /* Reporte Dia Departamento */
        $(".consulta_dia_d").on("click", function(e) {
            e.preventDefault();
            var date = $("#consulta_dia_fecha_d").val();
            var arrayfechas = new Array();
            var sumkgbyfechaP = new Array();
            var sumkgbyfechaL = new Array();
            $.ajax({
                url: base_url + "/reportes_pers/consulta_dia_d/" + date,
                type: "POST",
                success: function(resp) {
                    var respJSON = JSON.parse(resp);
                    respPJSON = respJSON['produccion'];
                    respLJSON = respJSON['limpieza'];
                    /*console.log(respPJSON);
                    console.log(respLJSON);*/
                    for (i = 0; i < respPJSON.length; i++) {
                        if (arrayfechas.length == 0) {
                            arrayfechas.push(respPJSON[i]["fecha"]);
                        }
                        if (arrayfechas.length > 0) {
                            if (arrayfechas.indexOf(respPJSON[i]["fecha"]) == -1) {
                                arrayfechas.push(respPJSON[i]["fecha"]);
                            }
                        }
                    }
                    for (i = 0; i < respLJSON.length; i++) {
                        if (arrayfechas.length == 0) {
                            arrayfechas.push(respLJSON[i]["fecha"]);
                        }
                        if (arrayfechas.length > 0) {
                            if (arrayfechas.indexOf(respLJSON[i]["fecha"]) == -1) {
                                arrayfechas.push(respLJSON[i]["fecha"]);
                            }
                        }
                    }
                    //console.log(arrayfechas[0]);
                    for (i = 0; i < arrayfechas.length; i++) {
                        sumkgbyfechaP[i] = 0;
                        sumkgbyfechaL[i] = 0
                        for (j = 0; j < respPJSON.length; j++) {
                            if (arrayfechas[i] == respPJSON[j]["fecha"]) {
                                sumkgbyfechaP[i] += parseInt(respPJSON[j]["kilos"]);
                            }
                        }
                        for (j = 0; j < respLJSON.length; j++) {
                            if (arrayfechas[i] == respLJSON[j]["fecha"]) {
                                sumkgbyfechaL[i] += parseInt(respLJSON[j]["kilos"]);
                            }
                        }
                    }
                    var ctx = document.getElementById("GaphicDia_d").getContext("2d");
                    var mybarChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: arrayfechas,
                            datasets: [{
                                label: 'Descascarado',
                                backgroundColor: "#000080",
                                data: sumkgbyfechaP
                            }, {
                                label: 'Limpieza',
                                backgroundColor: "#add8e6",
                                data: sumkgbyfechaL
                            }]
                        },
                        options: {
                            legend: {
                                display: true,
                                position: 'top',
                                labels: {
                                    fontColor: "#000080",
                                }
                            },
                            title: {
                                display: true,
                                text: 'Trabajo Realizado por Dia Departamento (Kilos)'
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                }
            });
        });
        $(".consulta_dia").on("click", function(e) {
            e.preventDefault();
            var date = $("#consulta_dia_fecha").val();
            $.ajax({
                url: base_url + "/reportes_pers/consulta_dia/" + date,
                type: "POST",
                success: function(resp) {
                    var respJSON = JSON.parse(resp);
                    respDJSON = respJSON['dia'];
                    var arraydata = new Array();
                    var arraycolors = new Array();
                    var arraylabel = new Array();
                    respDJSON.forEach(function(element) {
                        //console.log(element["kilos"]);
                        arraydata.push(element["kilos"]);
                        arraycolors.push(dame_color_aleatorio());
                        arraylabel.push(element["nombre"] + " (" + element["nombred"] + ")");
                    });
                    var datos = {
                        type: "pie",
                        data: {
                            datasets: [{
                                data: arraydata,
                                backgroundColor: arraycolors,
                            }],
                            labels: arraylabel
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Trabajo Realizado por Dia Personal (Kilos)'
                            },
                            responsive: true,
                        }
                    };
                    var canvas = document.getElementById('GaphicDia').getContext('2d');
                    window.pie = new Chart(canvas, datos);
                }
            });
        });
        $(".btn-save-prod").on("click", function(e) {
            e.preventDefault();
            var id = $(this).data().id;
            var fecha = $("#" + id + "fecha").val();
            var cantidadc = $("#" + id + "cantidadc").val();
            var cantidadk = $("#" + id + "cantidadk").val();
            console.log(base_url + "personal/productividad/" + id + "/" + fecha + "/" + cantidadc + "/" + cantidadk);
            $.ajax({
                url: base_url + "personal/productividad/" + id + "/" + fecha + "/" + cantidadc + "/" + cantidadk,
                type: "POST",
                success: function(resp) {
                    location.reload();
                }
            });
        });
        /*BOTONES QUE CONTROLAN EL FLUJO DEL PROCESO*/
        $(".btn-view-s0").on("click", function() {
            /*PASAR DE ESTADO 0(INICIAL) A 1(DESCASCARANDO)*/
            var id = $(this).val();
            array = id.split("|");
            id_proceso = array[0];
            id_productor = array[1];
            $("#modal-0 .modal-body #id_proceso").val(id_proceso);
            $("#modal-0 .modal-body #id_productor").val(id_productor);
            $.ajax({
                url: base_url + "produccion/trabajador_maquina/",
                type: "POST",
                success: function(resp) {
                    $("#modal-0 .modal-body #contenido0").html(resp);
                    $("#trabajador").select2({
                        placeholder: 'Seleccione un Trabajador'
                    });
                    $("#maquina").select2({
                        placeholder: 'Seleccione una Maquina'
                    });
                }
            });
        });
        $(".btn-view-s1-pause").on("click", function(e) {
            /*PAUSAR PROCESO DE DESCASCARADO*/
            e.preventDefault();
            var id = $(this).data().id;
            var button = $(this);
            console.log(button);
            $.ajax({
                url: base_url + "produccion/pausa_descascarado/" + id,
                type: "POST",
                success: function(resp) {
                    location.reload();
                }
            });
        });
        $(".btn-view-s1-play").on("click", function(e) {
            /*CONTINUAR PROCESO DE DESCASCARADO*/
            e.preventDefault();
            var id = $(this).data().id;
            $.ajax({
                url: base_url + "produccion/play_descascarado/" + id,
                type: "POST",
                success: function(resp) {
                    location.reload();
                }
            });
        });
        $(".btn-view-s1-stop").on("click", function(e) {
            /*TERMINAR PROCESO DE DESCASCARADO*/
            e.preventDefault();
            var id = $(this).data().id;
            array = id.split("|");
            id_proceso = array[0];
            id_productor = array[1];
            $("#modal-1 .modal-body #id_proceso").val(id_proceso);
            $("#modal-1 .modal-body #id_productor").val(id_productor);
        });
        $(".btn-view-s2").on("click", function() {
            /*PASAR DE ESTADO 2(LIMPIAR) A 3(LIMPIANDO)*/
            var id = $(this).val();
            array = id.split("|");
            id_proceso = array[0];
            id_productor = array[1];
            $("#modal-2 .modal-body #id_proceso").val(id_proceso);
            $("#modal-2 .modal-body #id_productor").val(id_productor);
            $.ajax({
                url: base_url + "produccion/trabajador_limp/",
                type: "POST",
                success: function(resp) {
                    $("#modal-2 .modal-body #contenido0").html(resp);
                    $("#trabajador").select2({
                        placeholder: 'Seleccione un Trabajador'
                    });
                }
            });
        });
        $(".btn-view-s3-pause").on("click", function(e) {
            /*PAUSAR PROCESO DE LIMPIEZA*/
            e.preventDefault();
            var id = $(this).data().id;
            var button = $(this);
            console.log(button);
            $.ajax({
                url: base_url + "produccion/pausa_limpieza/" + id,
                type: "POST",
                success: function(resp) {
                    location.reload();
                }
            });
        });
        $(".btn-view-s3-play").on("click", function(e) {
            /*CONTINUAR PROCESO DE DESCASCARADO*/
            e.preventDefault();
            var id = $(this).data().id;
            $.ajax({
                url: base_url + "produccion/play_limpieza/" + id,
                type: "POST",
                success: function(resp) {
                    location.reload();
                }
            });
        });
        $(".btn-view-s3-stop").on("click", function(e) {
            /*TERMINAR PROCESO DE LIMPIEZA*/
            e.preventDefault();
            var id = $(this).data().id;
            array = id.split("|");
            id_proceso = array[0];
            id_productor = array[1];
            $("#modal-3 .modal-body #id_proceso").val(id_proceso);
            $("#modal-3 .modal-body #id_productor").val(id_productor);
        });
        $(".btn-view-s4").on("click", function() {
            /*PASAR DE ESTADO 4(DISPONIBLE A PAGAR) A 5(PAGADO)*/
            var id = $(this).val();
            //alert(id);
            array = id.split("|");
            id_proceso = array[0];
            id_productor = array[1];
            $("#modal-4 .modal-body #id_proceso").val(id_proceso);
            $("#modal-4 .modal-body #id_productor").val(id_productor);
            $.ajax({
                url: base_url + "produccion/info_entrega/" + id_proceso + "/" + id_productor,
                type: "POST",
                success: function(resp) {
                    console.log(resp);
                    $("#modal-4 .modal-body #contenido1").html(resp);
                }
            });
        });
        $(".btn-view-s5").on("click", function() {
            /*VER INFORMACION DE PAGO*/
            var id = $(this).val();
            $.ajax({
                url: base_url + "produccion/info_compra/" + id,
                type: "POST",
                success: function(resp) {
                    $("#modal-5 .modal-body").html(resp);
                }
            });
        });
        $(".btn-view-s6").on("click", function() {
            /*PASAR DE ESTADO 6(SECAR) A 8(SECANDO) SIN PASAR POR ESTADO 7*/
            var id = $(this).data().id;
            array = id.split("|");
            console.log(array);
            id_proceso = array[0];
            id_productor = array[1];
            $.ajax({
                url: base_url + "produccion/secado/" + id_proceso + "/" + id_productor,
                type: "POST",
                success: function(resp) {
                    location.reload();
                }
            });
        });
        $(".btn-view-s7").on("click", function() {
            /*VER INFORMACION DE CANCELACION*/
            var id = $(this).val();
        });
        $(".btn-view-s8-stop").on("click", function(e) {
            /*TERMINAR PROCESO DE SECADO*/
            e.preventDefault();
            var id_proceso = $(this).data().id;
            $.ajax({
                url: base_url + "produccion/secado_fin/" + id_proceso,
                type: "POST",
                success: function(resp) {
                    location.reload();
                }
            });
        });
        $(".btn-view-s9").on("click", function(e){
            //alert("Si entro aca we");
            e.preventDefault();
            var id = $(this).data().id;
            array = id.split("|");
            id_proceso = array[0];
            kg_i = array[1];
            tel = array[2];
            id_productor = array[3];
            alert(kg_i);
            //console.log(id_proceso + " | " + kg_i + " | "+ tel + " | " + id_productor);
            var kg_pagar = kg_i * 0.42;
            kg_pagar = intlRound(kg_pagar,2);
            alert(kg_pagar);
            var total_pagar = kg_pagar * 9;
            var fecha = hoyFecha();
            //console.log(fecha);
            /*$("#modal-6 .modal-body #id_proceso").val(id_proceso);
            $("#modal-6 .modal-body #id_productor").val(id_productor);*/
            $("#id_productor").val(id_productor);
            $("#fecha").val(fecha);
            $("#propuesta").val(kg_pagar);
            /*Inicio AJAX para guardar compra y generar folio*/
            /*$.ajax({
              url: base_url + "compra/save_compram/" + id_productor + "/" + fecha + "/" + kg_pagar + "/" + total_pagar + "/1",
              type: "POST",
              success:function(resp){
                var id_compra = resp;
                $.ajax({
                    url: base_url + "produccion/setIdCompra/" + id_proceso + "/" + id_compra,
                    type: "POST",
                    success:function(resp){
                        location.href = base_url + "compra/generar_pdf/" + id_compra;
                    }
                });
              }
            });
            /* Fin AJAX */
        });
        $("#pagarcuarentaydos").on("click", function(e){
            var id_productor = $("#id_productor").val();
            var fecha = $("#fecha").val();
            var totalkg = $("#real").val();
            var totalpagar = totalkg * 9;
            $.ajax({
              url: base_url + "compra/save_compram/" + id_productor + "/" + fecha + "/" + totalkg + "/" + totalpagar + "/1",
              type: "POST",
              success:function(resp){
                var id_compra = resp;
                $.ajax({
                    url: base_url + "produccion/setIdCompra/" + id_proceso + "/" + id_compra,
                    type: "POST",
                    success:function(resp){
                        location.href = base_url + "compra/generar_pdf/" + id_compra;
                    }
                });
              }
            });
        });
        /*FIN BOTONES QUE CONTROLAN EL FLUJO DEL PROCESO*/
        $("#mun").select2({
            placeholder: 'Seleccione un Municipio'
        });
        $("#mun").change(function() {
            var munselect = $("#mun").val();
            $.ajax({
                url: base_url + "productores/loc/" + munselect,
                type: "POST",
                success: function(res) {
                    $('#loc').children('option').remove();
                    $("#loc").append(res);
                    $("#loc").select2({
                        placeholder: 'Seleccione una Localidad'
                    });
                },
                error: function(res) {
                    console.log(res);
                }
            });
        });
        $("#loc").select2({
            placeholder: 'Seleccione una Localidad'
        });
        $("#sexo").select2({
            placeholder: 'Seleccione el Genero'
        });
        $('.sidebar-menu').tree();
        $('.btn-form-prod').on("click", function() {
            var id = $(this).val();
            $.ajax({
                url: base_url + "produccion/chose/" + id,
                type: "POST",
                success: function(resp) {
                    $("#modal-default .modal-body").html(resp);
                }
            });
        });
        $('.btn-view-productor').on("click", function() {
            var id = $(this).val();
            console.log("sdf");
            $.ajax({
                url: base_url + "productores/view2/" + id,
                type: "POST",
                success: function(resp) {
                    $("#modal-default .modal-body").html(resp);
                }
            });
        });
        $('.btn-view-entregas').on("click", function() {
            var id = $(this).val();
            $("#id_productor_entrega").val(id);
            $.ajax({
                url: base_url + "entrega_2017/get/" + id,
                type: "POST",
                success: function(resp) {
                    $("#modal-entrega #contenido_entrega").html(resp);
                }
            });
        });
        $('.btn-view-entre').on("click", function() {
            var id = $(this).val();
            $.ajax({
                url: base_url + "entrega_2018/get/" + id,
                type: "POST",
                success: function(resp) {
                    $("#modal-entrega .modal-body").html(resp);
                }
            });
        });
        $('.entrega_polvo').on("click", function() {
            var id = $("#id_productor_entrega").val();
            $.ajax({
                url: base_url + "entrega_2017/folio/" + id,
                type: "POST",
                success: function(resp) {
                    console.log(resp);
                }
            });
        });
        $('.btn-view').on("click", function() {
            var id = $(this).val();
            $.ajax({
                url: base_url + "personal/view/" + id,
                type: "POST",
                success: function(resp) {
                    $("#modal-default .modal-body").html(resp);
                }
            });
        });
        $('.btn-remove').on("click", function(e) {
            e.preventDefault();
            var ruta = $(this).attr("href");
            $.ajax({
                url: ruta,
                type: "POST",
                success: function(resp) {
                    window.location.href = base_url + resp;
                }
            });
        });
        $('.print_folio').on("click", function(e) {
            var base_url = "<?php echo base_url(); ?>";
            e.preventDefault();
            var folio = $("#folio").val();
            $.ajax({
                url: base_url + "imprimir_folio/search/" + folio,
                type: "POST",
                success: function(resp) {
                    var obj = resp.split(">");
                    console.log(obj);
                    var hibrida = obj[6].split("\n");
                    var manzanita = obj[7].split("\n");
                    hibrida = hibrida[0];
                    manzanita = manzanita[0];
                    console.log("aa" + hibrida + "aa");
                    if (manzanita == " 0" && hibrida == " 0") {
                        location.href = base_url + "compra/generar_pdf/" + folio;
                    } else {
                        if (manzanita == " 0" && hibrida == " 1") {
                            location.href = base_url + "compra/generar_pdfh/" + folio;
                        } else {
                            if (manzanita == " 1" && hibrida == " 0") {
                                location.href = base_url + "compra/generar_pdfm/" + folio;
                            }
                        }
                    }
                }
            });
        });
        $('.print_folio_v').on("click", function(e) {
            var base_url = "<?php echo base_url(); ?>";
            e.preventDefault();
            var folio = $("#folio_v").val();
            //alert("Ya ando aca we");
            $.ajax({
                url: base_url + "ventas/search/" + folio,
                type: "POST",
                success: function(resp) {
                    var obj = resp.split(">");
                    console.log(obj);
                    var id_venta = obj[1].split("\n");
                    id_venta = id_venta[0];
                    location.href = base_url + "ventas/generar_pdf/" + folio;
                }
            });
        });
        $('.print_all_folios').on("click", function(e) {
            var base_url = "<?php echo base_url(); ?>";
            e.preventDefault();
            //console.log(arrayfolios[0]);
            //console.log(arrayfolios.length);
            location.href = base_url + "compra/generar_pdf2/";
            /*$.ajax({
              url: base_url + "imprimir_folio/search/" + folio,
              type: "POST",
              success:function(resp){
                console.log(resp);
                if(resp){
                  location.href = base_url + "compra/generar_pdf/" + folio;
                }else{
                  alert("No existe el Folio ingresado, favor de verificar.");
                }
              }
            });*/
        });
        $('.print_ingresos').on("click", function(e) {
            var base_url = "<?php echo base_url(); ?>";
            e.preventDefault();
            //console.log(arrayfolios[0]);
            //console.log(arrayfolios.length);
            location.href = base_url + "compra/generar_pdf3/";
        });
        $('#consulta_dia_compra').on("click", function(e) {
            e.preventDefault();
            var fecha = $("#fecha_compra").val();
            $.ajax({
                url: base_url + "reporte_compra_dia/generar_pdf/" + fecha,
                type: "POST",
                success: function(resp) {
                    console.log(resp);
                    $("#modal-10 .modal-body").html(resp);
                }
            });
        });
        $('.cancel_folio').on("click", function(e) {
            var base_url = "<?php echo base_url(); ?>";
            e.preventDefault();
            var folio = $("#folioc").val();
            var razon = $("#razon").val();
            $.ajax({
                url: base_url + "cancelar_folio/search/" + folio,
                type: "POST",
                success: function(resp) {
                    console.log(resp);
                    if (resp) {
                        //location.href = base_url + "compra/generar_pdf/" + folio;
                        $.ajax({
                            url: base_url + "cancelar_folio/update_cancel/" + folio + "/" + razon,
                            type: "POST",
                            success: function(resp) {
                                //location.reload();
                                if (resp) {
                                    alert("Folio cancelado");
                                    $("#folioc").val("");
                                    $("#razon").val("");
                                }
                            }
                        });
                    } else {
                        alert("No existe el Folio ingresado, favor de verificar.");
                    }
                }
            });
        });
        $('#personal').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "No se encontraron resultados en su busqueda",
                "searchPlaceholder": "Buscar registros",
                "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });
    });

</script>
</body>

</html>
