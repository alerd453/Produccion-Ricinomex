<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productores extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Productores_model");
		$this->load->model("Municipios_model");
		$this->load->model("Localidades_model");
        $this->load->model("Parcelas_model");
	}
	public function index()
	{
		$data = array(
			'productores' => $this->Productores_model->getProductores()
		);
		$this->output->cache(15);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		//$this->load->view('admin/productores/listOpt');
		$this->load->view('admin/productores/list',$data);
		$this->load->view('layouts/footer');
	}
	public function add(){
		$data = array(
			'municipios' => $this->Municipios_model->getMunicipios()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/productores/add',$data);
		$this->load->view('layouts/footer');
	}
	public function loc($id){
		$data = array('localidades' => $this->Localidades_model->getLocalidades($id) );
		$this->load->view("admin/productores/localidades",$data);
	}
	public function store(){
		$programa = $this->input->post("programa");
		$nombre = $this->input->post("nombre");
		$apellidop = $this->input->post("apellidop");
		$apellidom = $this->input->post("apellidom");
		$sexo = $this->input->post("sexo");
		$curp = $this->input->post("curp");
		$telefono = $this->input->post("telefono");
		$loc = $this->input->post("loc");
		$superficie = $this->input->post("superficie");
		$aprobado = $this->input->post("aprobado");
		//echo $nombre." ".$apellidop." ".$apellidom." ".$departamento;
		if($programa == "on"){
			$data = array(
				'nombre_completo' => $apellidop." ".$apellidom." ".$nombre,
				'apellido_paterno' => $apellidop,
				'apellido_materno' => $apellidom,
				'nombre' => $nombre,
				'sexo' => $sexo,
				'curp' => $curp,
				'localidad' => $loc,
				'telefono' => $telefono,
				'superficie' => $superficie,
				'aprobado' => $aprobado,
				'programa' => 1
			);
		}else{
			$data = array(
				'nombre_completo' => $apellidop." ".$apellidom." ".$nombre,
				'apellido_paterno' => $apellidop,
				'apellido_materno' => $apellidom,
				'nombre' => $nombre,
				'sexo' => $sexo,
				'curp' => $curp,
				'localidad' => $loc,
				'telefono' => $telefono,
				'superficie' => $superficie,
				'aprobado' => $aprobado,
				'programa' => 0
			);
		}
		if ($this->Productores_model->save($data)) {
			$this->output->delete_cache();
			redirect(base_url()."productores");
		}else{
			$this->session->set_flashdata("error","No se pudo guardar la información");
			redirect(base_url()."productores/add");
		}
	}
	public function edit($id){
		$idLocalidad = $this->Productores_model->getProductor($id)->localidad;
		$municipio = $this->Productores_model->getLocalidad($idLocalidad)->municipio_id;
		$data = array(
			'productor' => $this->Productores_model->getProductor($id),
			'localidades' => $this->Localidades_model->getLocalidades($municipio),
			'municipios' => $this->Municipios_model->getMunicipios()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/productores/edit',$data);
		$this->load->view('layouts/footer');
	}
	public function update(){
		$idProductor = $this->input->post("idProductor");
		$programa = $this->input->post("programa");
		$nombre = $this->input->post("nombre");
		$apellidop = $this->input->post("apellidop");
		$apellidom = $this->input->post("apellidom");
		$sexo = $this->input->post("sexo");
		$curp = $this->input->post("curp");
		$telefono = $this->input->post("telefono");
		$loc = $this->input->post("loc");
		$superficie = $this->input->post("superficie");
		$aprobado = $this->input->post("aprobado");
		//echo $nombre." ".$apellidop." ".$apellidom." ".$sexo;
		if($programa == "on"){
			$data = array(
				'nombre_completo' => $apellidop." ".$apellidom." ".$nombre,
				'apellido_paterno' => $apellidop,
				'apellido_materno' => $apellidom,
				'nombre' => $nombre,
				'sexo' => $sexo,
				'curp' => $curp,
				'localidad' => $loc,
				'telefono' => $telefono,
				'superficie' => $superficie,
				'aprobado' => $aprobado,
				'programa' => 1
			);
		}else{
			$data = array(
				'nombre_completo' => $apellidop." ".$apellidom." ".$nombre,
				'apellido_paterno' => $apellidop,
				'apellido_materno' => $apellidom,
				'nombre' => $nombre,
				'sexo' => $sexo,
				'curp' => $curp,
				'localidad' => $loc,
				'telefono' => $telefono,
				'superficie' => $superficie,
				'aprobado' => $aprobado,
				'programa' => 0
			);
		}
		if ($this->Productores_model->update($idProductor,$data)) {
			redirect(base_url()."productores/view/".$idProductor);
		}else{
			$this->session->set_flashdata("error","No se pudo actualizar la información");
			redirect(base_url()."productores/edit/".$idProductor);
		}
	}
	public function view2 ($id){
		$data = array(
			'productor' => $this->Productores_model->getProductor($id)
		);
        //$this->load->view('layouts/header');
		//$this->load->view('layouts/aside');
		$this->load->view("admin/productores/view2",$data);
        //$this->load->view('layouts/footer');
	}
	public function view($id){
        $this->load->library("leaflet");
        $config = array(
            'center'         => '16.640728,-96.73376', // Center of the map
            'zoom'           => 9, // Map zoom
        );
        $this->leaflet->initialize($config);
        $markers = $this->Parcelas_model->getParcelasByIdProductor($id);
        foreach($markers as $marker_i)
        {
            $marker = array(
                'latlng' 		=> $marker_i->pos_x.','.$marker_i->pos_y, // Marker Location
                'popupContent' 	=> $marker_i->text_marker, // Popup Content
            );
            $this->leaflet->add_marker($marker);
         }
        //print_r($marker);
		$data = array(
			'productor' => $this->Productores_model->getProductor($id),
            'parcelas' => $this->Parcelas_model->getParcelasByIdProductor($id),
            'map2' => $this->leaflet->create_map()
		);
        //$this->load->view('layouts/header');
		//$this->load->view('layouts/aside');
		$this->load->view("admin/productores/view",$data);
        //$this->load->view('layouts/footer');
	}
	public function delete($id){
		$this->Productores_model->delete($id);
		echo "Ok";
	}
	public function mostrar()
	{	
		//valor a Buscar
		$buscar = $this->input->post("buscar");
		$numeropagina = $this->input->post("nropagina");
		$cantidad = $this->input->post("cantidad");
		
		$inicio = ($numeropagina -1)*$cantidad;
		$data = array(
			"productores" => $this->Productores_model->buscar($buscar,$inicio,$cantidad),
			"totalregistros" => count($this->Productores_model->buscar($buscar)),
			"cantidad" =>$cantidad
			
		);
		echo json_encode($data);
	}
	public function saveparcela($parcela,$latitud,$longitud,$text,$ha,$id_productor){
		$data = array(
			'id_productor' => $id_productor,
			'paraje_parcela' => str_replace("%20"," ",$parcela),
			'pos_x' => $latitud,
			'pos_y' => $longitud,
			'text_marker' => str_replace("%20"," ",$text),
			'area_ha' => $ha
		); 
		if ($this->Parcelas_model->saveParcela($data)) {
			echo "Ok";
		}else{
			echo "fail";
		}
	}
	public function getparcelaid($id){
		$parcela = $this->Parcelas_model->getParcelasByIdParcelas($id);
		echo json_encode($parcela);
	}
	public function updateparcela($parcela,$latitud,$longitud,$text,$ha,$id_productor,$id_parcela){
		$data = array(
			'id_productor' => $id_productor,
			'paraje_parcela' => str_replace("%20"," ",$parcela),
			'pos_x' => $latitud,
			'pos_y' => $longitud,
			'text_marker' => str_replace("%20"," ",$text),
			'area_ha' => $ha
		); 
		if ($this->Parcelas_model->updateParcela($id_parcela,$data)) {
			echo "Ok";
		}else{
			echo "fail";
		}
	}
	public function deleteparcela($id_parcela){
		$this->Parcelas_model->deleteParcela($id_parcela);
		echo "Ok";
	}
}