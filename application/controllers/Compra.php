<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Compra extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Productores_model");
		$this->load->model("Compra_model");
		$this->load->library("pdf");
	}
	public function index()
	{
		$data = array(
			'productores' => $this->Productores_model->getProductores()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/compra/nueva_compra',$data);
		$this->load->view('layouts/footer');
	}
	public function view($id){
		$data = array(
			'productor' => $this->Productores_model->getProductor($id)
		);
		$this->load->view("admin/compra/form",$data);
	}
	public function save_compra($idProductor,$fecha,$kg,$importe){
		$data = array(
			'id_proveedor' => $idProductor,
			'fecha_compra' => $fecha,
			'cantidadkg' => $kg,
			'importetotal' => $importe,
			'cancelado' => 0
		);
		$id_compra = $this->Compra_model->save($data);
		echo $id_compra;
	}
	public function save_comprah($idProductor,$fecha,$kg,$importe,$hibrida){
		$data = array(
			'id_proveedor' => $idProductor,
			'fecha_compra' => $fecha,
			'cantidadkg' => $kg,
			'importetotal' => $importe,
			'cancelado' => 0,
			'hibrida' => $hibrida
		);
		$id_compra = $this->Compra_model->save($data);
		echo $id_compra;
	}
	public function save_compram($idProductor,$fecha,$kg,$importe,$manzanita){
		$data = array(
			'id_proveedor' => $idProductor,
			'fecha_compra' => $fecha,
			'cantidadkg' => $kg,
			'importetotal' => $importe,
			'cancelado' => 0,
			'manzanita' => $manzanita
		);
		$id_compra = $this->Compra_model->save($data);
		echo $id_compra;
	}
	public function generar_pdf($id_compra){
		$compra = $this->Compra_model->getById($id_compra);
		$data = array(
			'id_compra' => $id_compra,
			'compra' => $compra
		);
		//print_r($compra);
		$this->load->view("admin/compra/pdf",$data);
	}
	public function generar_pdfh($id_compra){
		$compra = $this->Compra_model->getById($id_compra);
		$data = array(
			'id_compra' => $id_compra,
			'compra' => $compra
		);
		//print_r($compra);
		$this->load->view("admin/compra/pdfh",$data);
	}
	public function generar_pdfm($id_compra){
		$compra = $this->Compra_model->getById($id_compra);
		$data = array(
			'id_compra' => $id_compra,
			'compra' => $compra
		);
		//print_r($compra);
		$this->load->view("admin/compra/pdfm",$data);
	}
	public function generar_pdf2(){
		$compras = $this->Compra_model->getAllCompras();
		$data = array(
			'com' => $compras
		);
		//print_r($data);
		$this->load->view("admin/compra/pdf2",$data);
	}
	public function generar_pdf3(){
		$compras = $this->Compra_model->getAllIngresos();
		$data = array(
			'com' => $compras
		);
		$this->load->view("admin/compra/pdf3",$data);
	}
}