<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auditoria extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Productores_model");
		$this->load->model("Compra_model");
        $this->load->library('excel');
	}
	public function index()
	{
		$data = array(
			'compra' => $this->Compra_model->getCompras(),
			'compraold' => $this->Compra_model->getComprasOld()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/compra/auditar',$data);
		$this->load->view('layouts/footer');
	}
	public function auditando($id,$type){
		$data = array(
			'id' => $id,
			'type' => $type
		);
		$this->load->view("admin/compra/actualizar_auditoria",$data);
	}
	public function save(){
        date_default_timezone_set('America/Mexico_City');
		$tipo = $this->input->post("tipo");
		$id = $this->input->post("id_compra");
		$peso = $this->input->post("peso");
		$observacion = $this->input->post("observacion");
        $idUsuario = $this->session->userdata("id");
        $fechaactual=	getdate();
		$fechaactual = $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'];
		$auditado = 1;
		$data = array(
			'auditado' => $auditado,
			'kg_au' => $peso,
			'observacion_auditoria' => $observacion,
            'audito' => $idUsuario,
            'fecha_auditoria' => $fechaactual
		);
		if($tipo == "1"){
			if ($this->Compra_model->setAuditoria($id,$data)) {
				redirect(base_url()."auditoria");
			}
		}else{
			if ($this->Compra_model->setAuditoriaOld($id,$data)) {
				redirect(base_url()."auditoria");
			}
		}
	}
    public function getResultAuditoria(){
        
        $this->excel->getProperties()->setCreator("RICINOMEX") // Nombre del autor
            ->setLastModifiedBy("RICINOMEX") //Ultimo usuario que lo modificó
            ->setTitle("Reporte Auditoria") // Titulo
            ->setSubject("Reporte Auditoria Inicial") //Asunto
            ->setDescription("Reporte Final de Auditoria Principal") //Descripción
            ->setKeywords("reporte auditoria ricinomex") //Etiquetas
            ->setCategory("Reporte"); //Categorias
        $tituloReporte = "Folios Auditados";
        $titulosColumnas = array('Folio de Compra', 'Comprador', 'Localidad', 'Auditado', 'Kg Pagados', 'Kg Auditado', 'Diferencia', 'Observacion', 'Audito', 'Fecha de Auditoria');
        // Se combinan las celdas A1 hasta D1, para colocar ahí el titulo del reporte
        $this->excel->setActiveSheetIndex(0)
            ->mergeCells('A1:J1');

        // Se agregan los titulos del reporte
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue('A1', $tituloReporte) // Titulo del reporte
            ->setCellValue('A3',  $titulosColumnas[0])  //Titulo de las columnas
            ->setCellValue('B3',  $titulosColumnas[1])
            ->setCellValue('C3',  $titulosColumnas[2])
            ->setCellValue('D3',  $titulosColumnas[3])
            ->setCellValue('E3',  $titulosColumnas[4])
            ->setCellValue('F3',  $titulosColumnas[5])
            ->setCellValue('G3',  $titulosColumnas[6])
            ->setCellValue('H3',  $titulosColumnas[7])
            ->setCellValue('I3',  $titulosColumnas[8])
            ->setCellValue('J3',  $titulosColumnas[9]);
        $i = 4; //Numero de fila donde se va a comenzar a rellenar
        
        $compras = $this->Compra_model->getCompras();
        $comprasOld = $this->Compra_model->getComprasOld();
        
        //print_r($compras);
        //print_r($comprasOld);
        
        foreach($compras as $c){
            $this->excel->setActiveSheetIndex(0)
				        ->setCellValue('A'.$i,$c->id_compra_semilla)
				        ->setCellValue('B'.$i,$c->nombre_completo)
				        ->setCellValue('C'.$i,$c->nombre_l)
                        ->setCellValue('D'.$i,$c->auditado)
				        ->setCellValue('E'.$i,$c->cantidadkg)
				        ->setCellValue('F'.$i,$c->kg_au)
				        ->setCellValue('G'.$i,$c->kg_au - $c->cantidadkg)
                        ->setCellValue('H'.$i,$c->observacion_auditoria)
                        ->setCellValue('I'.$i,$c->audito)
                        ->setCellValue('J'.$i,$c->fecha_auditoria);
            $i++;
        }
        foreach($comprasOld as $c){
            $this->excel->setActiveSheetIndex(0)
				        ->setCellValue('A'.$i,$c->id_compra_semilla)
				        ->setCellValue('B'.$i,$c->nombre_completo)
				        ->setCellValue('C'.$i,$c->nombre_l)
                        ->setCellValue('D'.$i,$c->auditado)
				        ->setCellValue('E'.$i,$c->cantidadkg)
				        ->setCellValue('F'.$i,$c->kg_au)
				        ->setCellValue('G'.$i,$c->kg_au - $c->cantidadkg)
                        ->setCellValue('H'.$i,$c->observacion_auditoria)
                        ->setCellValue('I'.$i,$c->audito)
                        ->setCellValue('J'.$i,$c->fecha_auditoria);
            $i++;
        }
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ReporteAuditoria.xls"');
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // Forzamos a la descarga
        $objWriter->save('php://output');
    }
}