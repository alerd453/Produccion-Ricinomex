<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ventas extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model("Venta_model");
        $this->load->library("pdf");
    }
    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/ventas/nueva_venta');
        $this->load->view('layouts/footer');
    }
    public function save_venta(){
        $nombre_comprador = $this->input->post("nombre");
        $producto_venta = $this->input->post("producto");
        $precio = $this->input->post("precio");
        $cantidad = $this->input->post("cantidad");
        $importe = $this->input->post("importe");
        $fecha_venta = $this->input->post("Fecha");
        $data = array(
            'nombre_comprador' => $nombre_comprador,
            'producto_venta' => $producto_venta,
            'precio' => $precio,
            'cantidad' => $cantidad,
            'importe' => $importe,
            'fecha_venta' => $fecha_venta
        );
        $id_venta = $this->Venta_model->save($data);
        $data2 = array(
            'id_venta' => $id_venta,
            'venta' => $data
        );
        //print_r($data2);
        $this->load->view("admin/ventas/pdf",$data2);
    }
    public function reimprimir(){
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/ventas/imprimir_venta');
        $this->load->view('layouts/footer');
    }
    public function search($id_folio){
        $existe = $this->Venta_model->getById($id_folio);
        $arrayexiste = json_decode(json_encode($existe), True);
        print_r($arrayexiste);
    }
    public function generar_pdf($id_venta){
        $venta = $this->Venta_model->getById($id_venta);
        $data = array(
            'id_venta' => $id_venta,
            'venta' => $venta
        );
        //print_r($data);
        $this->load->view("admin/ventas/pdfv2",$data);
    }
}