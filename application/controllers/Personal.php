<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Personal extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Personal_model");
		$this->load->model("Departamento_model");
		$this->load->model("Productividad_model");
	}
	public function index()
	{
		$data = array(
			'personal' => $this->Personal_model->getPersonal()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/personal/list',$data);
		$this->load->view('layouts/footer');
	}
	public function add(){
		$data = array(
			'departamento' => $this->Departamento_model->getDepartamentos(), 
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/personal/add',$data);
		$this->load->view('layouts/footer');
	}
	public function store(){
		$nombre = $this->input->post("nombre");
		$apellidop = $this->input->post("apellidop");
		$apellidom = $this->input->post("apellidom");
		$departamento = $this->input->post("dep");
		//echo $nombre." ".$apellidop." ".$apellidom." ".$departamento;
		$data = array(
			'nombre' => $nombre,
			'apellido_paterno' => $apellidop,
			'apellido_materno' => $apellidom,
			'RP_Departamento_idDepartamento' => $departamento,
			'estado' => "1"
		);
		if ($this->Personal_model->save($data)) {
			redirect(base_url()."personal");
		}else{
			$this->session->set_flashdata("error","No se pudo guardar la información");
			redirect(base_url()."personal/add");
		}
	}
	public function edit($id){
		$data = array(
			'departamento' => $this->Departamento_model->getDepartamentos(), 
			'personal' => $this->Personal_model->getPerson($id)
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/personal/edit',$data);
		$this->load->view('layouts/footer');
	}
	public function update(){
		$idTrabajadores = $this->input->post("idTrabajadores");
		$nombre = $this->input->post("nombre");
		$apellidop = $this->input->post("apellidop");
		$apellidom = $this->input->post("apellidom");
		$departamento = $this->input->post("dep");
		$data = array(
			'nombre' => $nombre,
			'apellido_paterno' => $apellidop,
			'apellido_materno' => $apellidom,
			'RP_Departamento_idDepartamento' => $departamento
		);
		if ($this->Personal_model->update($idTrabajadores,$data)) {
			redirect(base_url()."personal");
		}else{
			$this->session->set_flashdata("error","No se pudo actualizar la información");
			redirect(base_url()."personal/edit/".$idTrabajadores);
		}
	}
	public function view($id){
		$data = array(
			'personal' => $this->Personal_model->getPerson($id)
		);
		$this->load->view("admin/personal/view",$data);
	}
	public function delete($id){
		$data = array('estado' => "0", );
		$this->Personal_model->update($id,$data);
		echo "personal";
	}
	public function productividad($id,$fecha,$cantidadC,$cantidadK){
		$data = array('fecha' => $fecha, 'costales' => $cantidadC, 'kilos' => $cantidadK, 'RP_idTrabajadores' => $id);
		if($this->Productividad_model->save($data)){
			redirect(base_url()."informacion");
		}
	}
}
