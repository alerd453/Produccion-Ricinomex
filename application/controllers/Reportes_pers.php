<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reportes_pers extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Productividad_model");
	}
	public function index()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/personal/reporte_personal');
		$this->load->view('layouts/footer');
	}
	public function consulta_dia($fecha)
	{
		$data = array(
			'dia' => $this->Productividad_model->getProductividadByFecha($fecha)
		);
		$dataj = json_encode($data);
		echo $dataj;
		//$this->load->view("admin/personal/reporte_dia",$data);
	}
	public function consulta_semana_p($fecha)
	{
		$fechafin = strtotime ( '+6 day' , strtotime ( $fecha ) ) ;
		$fechafin = date ( 'Y-m-j' , $fechafin );
		$data = array('produccion' => $this->Productividad_model->getProductividadSemanaP($fecha,$fechafin), 'limpieza' => $this->Productividad_model->getProductividadSemanaL($fecha,$fechafin));
		$dataj = json_encode($data);
		echo $dataj;
		/*$this->load->view("admin/personal/reporte_semana",$data);*/
	}
	public function consulta_dia_d($fecha){
		$data = array(
			'produccion' => $this->Productividad_model->getProductividadDiaP($fecha),
			'limpieza' => $this->Productividad_model->getProductividadDiaL($fecha)
		);
		$dataj = json_encode($data);
		echo $dataj;
	}
	public function consulta_semana_past($fecha){
		$fechafin = strtotime ( '+6 day' , strtotime ( $fecha ) ) ;
		$fechafin = date ( 'Y-m-j' , $fechafin );
		$data = array('semana' => $this->Productividad_model->getProductividadSemanaPastel($fecha,$fechafin));
		$dataj = json_encode($data);
		echo $dataj;
	}
}