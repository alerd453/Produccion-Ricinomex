<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard_invitado extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata("login")){
			redirect(base_url());
		}
		$this->load->model("Proceso_model");
		$this->load->model("Productores_model");
		$this->load->model("Descascarado_model");
		$this->load->model("Personal_model");
		$this->load->model("Maquinas_model");
		$this->load->model("Limpieza_model");
		$this->load->model("Secado_model");
		$this->load->model("Compra_model");
		$this->load->library("pdf");
	}
	public function index()
	{
		$data = array(
			'proceso' => $this->Proceso_model->getProcesos(),
			'descascarado' => $this->Descascarado_model->getDescascarados(),
			'limpieza' => $this->Limpieza_model->getLimpiezas()
		);
		$this->load->view('layouts/header_i');
		$this->load->view('layouts/aside_i');
		$this->load->view('admin/produccion/list',$data);
		$this->load->view('layouts/footer_i');
	}
}
