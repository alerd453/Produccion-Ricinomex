<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Informacion extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Personal_model");
		$this->load->model("Departamento_model");
		$this->load->model("Productividad_model");
	}
	public function index()
	{
		$data = array(
			'personal' => $this->Personal_model->getPersonal(),
			'prod' => $this->Personal_model->getPersonalProd()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/personal/informacion',$data);
		$this->load->view('layouts/footer');
	}
}