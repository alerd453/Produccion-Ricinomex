<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reportes_produccion extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Proceso_model");
	}
	public function index()
	{
		$data = array(
			'humedo' => $this->Proceso_model->getHumedo(),
			'sindescascarar' => $this->Proceso_model->getSinDescascarar(),
			'sinlimpiar' => $this->Proceso_model->getSinLimpiar(),
			'porpagar' => $this->Proceso_model->getPorPagar(),
			'pagadoc' => $this->Proceso_model->getPagadoC(),
			'pagadoh' => $this->Proceso_model->getPagadoH(),
			'hueso' => $this->Proceso_model->getHueso()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/produccion/reportes',$data);
		$this->load->view('layouts/footer');
	}
	public function consulta(){
		$fechaini = $this->input->post("fechaini");
		$fechafin = $this->input->post("fechafin");
		$data = array(
			'humedo' => $this->Proceso_model->getHumedoByFecha($fechaini, $fechafin),
			'sindescascarar' => $this->Proceso_model->getSinDescascararByFecha($fechaini, $fechafin),
			'sinlimpiar' => $this->Proceso_model->getSinLimpiarByFecha($fechaini, $fechafin),
			'porpagar' => $this->Proceso_model->getPorPagarByFecha($fechaini, $fechafin),
			'pagadoc' => $this->Proceso_model->getPagadoCByFecha($fechaini, $fechafin),
			'pagadoh' => $this->Proceso_model->getPagadoHByFecha($fechaini, $fechafin),
			'hueso' => $this->Proceso_model->getHuesoByFecha($fechaini, $fechafin)
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/produccion/reportes',$data);
		$this->load->view('layouts/footer');
	}
}