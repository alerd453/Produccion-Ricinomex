<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Parcelas extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Productores_model");
		$this->load->model("Municipios_model");
		$this->load->model("Localidades_model");
        $this->load->model("Parcelas_model");
	}
	public function index()
	{
		$this->load->library("leaflet");
        $config = array(
            'center'         => '16.640728,-96.73376', // Center of the map
            'zoom'           => 9, // Map zoom
        );
        $this->leaflet->initialize($config);
        $markers = $this->Parcelas_model->getParcelas();
        foreach($markers as $marker_i)
        {
            $marker = array(
                'latlng' 		=> $marker_i->pos_x.','.$marker_i->pos_y, // Marker Location
                'popupContent' 	=> $marker_i->text_marker, // Popup Content
            );
            $this->leaflet->add_marker($marker);
         }
        //print_r($marker);
		$data = array(
            'map2' => $this->leaflet->create_map()
		);
        //$this->load->view('layouts/header');
		//$this->load->view('layouts/aside');
		$this->load->view("admin/parcelas/view",$data);
        //$this->load->view('layouts/footer');
	}
}