<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Produccion extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Proceso_model");
		$this->load->model("Productores_model");
		$this->load->model("Descascarado_model");
		$this->load->model("Personal_model");
		$this->load->model("Maquinas_model");
		$this->load->model("Limpieza_model");
		$this->load->model("Secado_model");
		$this->load->model("Compra_model");
		$this->load->library("pdf");
	}
	public function index(){
		$data = array(
			'proceso' => $this->Proceso_model->getProcesos(),
			'descascarado' => $this->Descascarado_model->getDescascarados(),
			'limpieza' => $this->Limpieza_model->getLimpiezas()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/produccion/list',$data);
		$this->load->view('layouts/footer');
	}
	public function add(){
		$data = array(
			'productores' => $this->Productores_model->getProductores()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/produccion/add',$data);
		$this->load->view('layouts/footer');
	}
	public function chose($id){
		$data = array(
			'productor' => $this->Productores_model->getProductor($id)
		);
		$this->load->view("admin/produccion/chose",$data);
	}
	public function trabajador_maquina(){
		$data = array(
			'trabajadores' => $this->Personal_model->getPersonalDesc(),
			'maquinas' => $this->Maquinas_model->getMaquinas()
		);
		$this->load->view("admin/produccion/trabajadores",$data);
	}
	public function save_form_ini_desc(){
		$id_productor = $this->input->post("id_productor");
		$id_proceso = $this->input->post("id_proceso");
		$fecha_inicial = $this->input->post("fecha_inicial");
		$trabajador = $this->input->post("trabajador");
		$maquina = $this->input->post("maquina");
		//echo $id_productor."/".$id_proceso."/".$fecha_inicial."/".$trabajador."/".$maquina;
		$data = array(
			'fecha_inicial' => $fecha_inicial,
			'RP_Maquinas_idMaquinas' => $maquina,
			'RP_Trabajadores_idTrabajadores' => $trabajador,
			'RP_Proceso_idRP_Proceso' => $id_proceso,
			'estado' => "Running"
		);
		$data2 = array(
			'RP_Status_idRP_Status' => 1
		);
		if ($this->Descascarado_model->save($data) && $this->Proceso_model->setStatus($id_proceso,$data2)) {
			redirect(base_url()."produccion");
		}
	}
	public function pausa_descascarado($id){
		date_default_timezone_set('America/Mexico_City'); 
		$descascarado = $this->Descascarado_model->getProceso($id);
		$id_descascarado = $descascarado->idRP_Descascarado;
		if (!$descascarado->temp_fecha) {
			$fecha_inicial = new DateTime($descascarado->fecha_inicial);
			$fecha_actual = new DateTime();
			$diferencia = $fecha_inicial->diff($fecha_actual);
			$diferencia = ($diferencia->format("%D")*86400)+($diferencia->format("%H")*3600)+($diferencia->format("%I")*60)+($diferencia->format("%S"));
			$data = array('tiempo_des' => $diferencia, 'estado' => "Pause" );
			$this->Descascarado_model->setTiempo($id_descascarado,$data);
		}else{
			$fecha_inicial = new DateTime($descascarado->temp_fecha);
			$fecha_actual = new DateTime();
			$diferencia = $fecha_inicial->diff($fecha_actual);
			$diferencia = ($diferencia->format("%D")*86400)+($diferencia->format("%H")*3600)+($diferencia->format("%I")*60)+($diferencia->format("%S"));
			$data = array('tiempo_des' => ($descascarado->tiempo_des + $diferencia), 'estado' => "Pause" );
			$this->Descascarado_model->setTiempo($id_descascarado,$data);
		}
		redirect(base_url()."produccion");
	}
	public function play_descascarado($id){
		date_default_timezone_set('America/Mexico_City');
		$descascarado = $this->Descascarado_model->getProceso($id);
		$id_descascarado = $descascarado->idRP_Descascarado;
		$fechaactual=	getdate();
		$fechaactual = $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds'];
		$data = array('temp_fecha' => $fechaactual, 'estado' => "Running" );
		$this->Descascarado_model->setTiempo($id_descascarado,$data);
		redirect(base_url()."produccion");
	}
	public function save_form_fin_desc(){
		date_default_timezone_set('America/Mexico_City');
		$id_productor = $this->input->post("id_productor");
		$id_proceso = $this->input->post("id_proceso");
		$fecha_final = $this->input->post("fecha_final");
		$cvacios = $this->input->post("cvacios");
		$csemilla = $this->input->post("csemilla");
		$kgsemilla = $this->input->post("kgsemilla");
		$descascarado = $this->Descascarado_model->getProceso($id_proceso);
		$id_descascarado = $descascarado->idRP_Descascarado;
		if($descascarado->estado == "Pause"){
			$data = array(
				'fecha_final' => $fecha_final, 
				'costales_vacios' => $cvacios,
				'costales' => $csemilla,
				'kg' => $kgsemilla,
				'estado' => "Finish" 
			);
		}elseif ($descascarado->estado == "Running") {
			if(!$descascarado->temp_fecha){
				$fecha_inicial = new DateTime($descascarado->fecha_inicial);
			}else{
				$fecha_inicial = new DateTime($descascarado->temp_fecha);
			}
			$fecha_actual = new DateTime();
			$diferencia = $fecha_inicial->diff($fecha_actual);
			$diferencia = ($diferencia->format("%D")*86400)+($diferencia->format("%H")*3600)+($diferencia->format("%I")*60)+($diferencia->format("%S"));
			$data = array(
				'fecha_final' => $fecha_final, 
				'costales_vacios' => $cvacios,
				'costales' => $csemilla,
				'kg' => $kgsemilla,
				'estado' => "Finish",
				'tiempo_des' => ($descascarado->tiempo_des + $diferencia)
			);
		}
		$data2 = array(
			'RP_Status_idRP_Status' => 2
		);
		$this->Descascarado_model->setTiempo($id_descascarado,$data);
		$this->Proceso_model->setStatus($id_proceso,$data2);
		redirect(base_url()."produccion");
	}
	public function trabajador_limp(){
		$data = array(
			'trabajadores' => $this->Personal_model->getPersonalLimp()
		);
		$this->load->view("admin/produccion/trabajadoreslimp",$data);
	}
	public function save_form_ini_limp(){
		$id_proceso = $this->input->post("id_proceso");
		$id_productor = $this->input->post("id_productor");
		$fecha_inicial = $this->input->post("fecha_inicial");
		$trabajador = $this->input->post("trabajador");
		$data = array(
			'fecha_inicial' => $fecha_inicial,
			'RP_Trabajadores_idTrabajadores' => $trabajador,
			'RP_Proceso_idRP_Proceso' => $id_proceso,
			'estado' => "Running"
		);
		$data2 = array(
			'RP_Status_idRP_Status' => 3
		);
		if ($this->Limpieza_model->save($data) && $this->Proceso_model->setStatus($id_proceso,$data2)) {
			redirect(base_url()."produccion");
		}
	}
	public function pausa_limpieza($id){
		date_default_timezone_set('America/Mexico_City'); 
		$limpieza = $this->Limpieza_model->getProceso($id);
		$id_limpieza = $limpieza->idRP_Limpieza;
		if (!$limpieza->temp_fecha) {
			$fecha_inicial = new DateTime($limpieza->fecha_inicial);
			$fecha_actual = new DateTime();
			$diferencia = $fecha_inicial->diff($fecha_actual);
			$diferencia = ($diferencia->format("%D")*86400)+($diferencia->format("%H")*3600)+($diferencia->format("%I")*60)+($diferencia->format("%S"));
			$data = array('tiempo_lim' => $diferencia, 'estado' => "Pause" );
			$this->Limpieza_model->setTiempo($id_limpieza,$data);
		}else{
			$fecha_inicial = new DateTime($limpieza->temp_fecha);
			$fecha_actual = new DateTime();
			$diferencia = $fecha_inicial->diff($fecha_actual);
			$diferencia = ($diferencia->format("%D")*86400)+($diferencia->format("%H")*3600)+($diferencia->format("%I")*60)+($diferencia->format("%S"));
			$data = array('tiempo_lim' => ($limpieza->tiempo_des + $diferencia), 'estado' => "Pause" );
			$this->Limpieza_model->setTiempo($id_limpieza,$data);
		}
		redirect(base_url()."produccion");
	}
	public function play_limpieza($id){
		date_default_timezone_set('America/Mexico_City');
		$limpieza = $this->Limpieza_model->getProceso($id);
		$id_limpieza = $limpieza->idRP_Limpieza;
		$fechaactual=	getdate();
		$fechaactual = $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds'];
		$data = array('temp_fecha' => $fechaactual, 'estado' => "Running" );
		$this->Limpieza_model->setTiempo($id_limpieza,$data);
		redirect(base_url()."produccion");
	}
	public function save_form_fin_limp(){
		date_default_timezone_set('America/Mexico_City');
		$id_productor = $this->input->post("id_productor");
		$id_proceso = $this->input->post("id_proceso");
		$fecha_final = $this->input->post("fecha_final");
		$cvacios = $this->input->post("cvacios");
		$csemilla = $this->input->post("csemilla");
		$kgsemilla = $this->input->post("kgsemilla");
		$kghueso = $this->input->post("kghueso");
		$limpieza = $this->Limpieza_model->getProceso($id_proceso);
		$id_descascarado = $limpieza->idRP_Limpieza;
		if($limpieza->estado == "Pause"){
			$data = array(
				'fecha_final' => $fecha_final, 
				'costales_vacios' => $cvacios,
				'costales' => $csemilla,
				'kg' => $kgsemilla,
				'hueso' => $kghueso,
				'estado' => "Finish"
			);
		}elseif ($limpieza->estado == "Running") {
			if(!$limpieza->temp_fecha){
				$fecha_inicial = new DateTime($limpieza->fecha_inicial);
			}else{
				$fecha_inicial = new DateTime($limpieza->temp_fecha);
			}
			$fecha_actual = new DateTime();
			$diferencia = $fecha_inicial->diff($fecha_actual);
			$diferencia = ($diferencia->format("%D")*86400)+($diferencia->format("%H")*3600)+($diferencia->format("%I")*60)+($diferencia->format("%S"));
			$data = array(
				'fecha_final' => $fecha_final, 
				'costales_vacios' => $cvacios,
				'costales' => $csemilla,
				'kg' => $kgsemilla,
				'hueso' => $kghueso,
				'estado' => "Finish",
				'tiempo_lim' => ($limpieza->tiempo_lim + $diferencia)
			);
		}
		$data2 = array(
			'RP_Status_idRP_Status' => 4
		);
		$this->Limpieza_model->setTiempo($id_descascarado,$data);
		$this->Proceso_model->setStatus($id_proceso,$data2);
		redirect(base_url()."produccion");
	}
	public function info_entrega($id_proceso,$id_productor){
		$data = array(
			'info' => $this->Proceso_model->getInfo($id_proceso,$id_productor)
		);
		$this->load->view("admin/produccion/info_entrega",$data);
	}
	/*FUNCIONES PAR EL SECADO DE SEMILLA*/
	public function secado($id_proceso,$id_productor){
		date_default_timezone_set('America/Mexico_City');
		$fechaactual=	getdate();
		$fechaactual = $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds'];
		$data = array('fecha_inicial' => $fechaactual, 'estado' => "Running", 'RP_Proceso_idRP_Proceso' => $id_proceso);
		$data2 = array(
			'RP_Status_idRP_Status' => 8
		);
		$this->Secado_model->save_ini($data);
		$this->Proceso_model->setStatus($id_proceso,$data2);
		redirect(base_url()."produccion");
	}
	public function secado_fin($id_proceso){
		date_default_timezone_set('America/Mexico_City');
		$secado = $this->Secado_model->getProceso($id_proceso);
		$id_secado = $secado->idRP_Secado;
		$fechaactual=	getdate();
		$fechaactual = $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds'];
		$data = array('fecha_final' => $fechaactual, 'estado' => "Finish");
		$data2 = array(
			'RP_Status_idRP_Status' => 0
		);
		$this->Secado_model->save_fin($id_secado,$data);
		$this->Proceso_model->setStatus($id_proceso,$data2);
		redirect(base_url()."produccion");
	}
	public function store(){
		$fecha_actual = $this->input->post("fecha_actual");
		$id_productor = $this->input->post("id_productor");
		$telefono = $this->input->post("telefono");
		$costales = $this->input->post("costales");
		$kilos = $this->input->post("kilos");
		$humedo = $this->input->post("humedo");
		$tipo = $this->input->post("semilla");
		//echo $humedo;
		$statustel = false;
		if ($telefono) {
			$data = array(
				'telefono' => $telefono
			);
			if($this->Productores_model->setTelefono($id_productor,$data)){
				$statustel = true;
			}
		}
		if($humedo){
			$data = array(
				'fecha_inicial' => $fecha_actual,
				'RP_Productores_idProducores' => $id_productor,
				'costales_i' => $costales,
				'kg_i' => $kilos,
				'tipo' => $tipo,
				'RP_Status_idRP_Status' => 6
			);
		}else{
			$data = array(
				'fecha_inicial' => $fecha_actual,
				'RP_Productores_idProducores' => $id_productor,
				'costales_i' => $costales,
				'kg_i' => $kilos,
				'tipo' => $tipo
			);
		}
		if ($this->Proceso_model->save($data)) {
			redirect(base_url()."produccion");
		}else{
			$this->session->set_flashdata("error","No se pudo guardar la información");
			redirect(base_url()."produccion/add");
		}
	}
	public function generateFolio(){
		$id_proceso = $this->input->post("id_proceso");
		$id_productor = $this->input->post("id_productor");
		date_default_timezone_set('America/Mexico_City');
        $fechaactual=   getdate();  
        $fecha = $fechaactual['year'].'-'.$fechaactual['mon'].'-'.$fechaactual['mday'].' '.$fechaactual['hours'].':'.$fechaactual['minutes'].':'.$fechaactual['seconds'];
        $proceso = $this->Proceso_model->getProcesoById2($id_proceso);
        if($proceso->tipo == 9){
        	$data = array(
				'id_proveedor' => $id_productor,
				'fecha_compra' => $fecha,
				'cantidadkg' => $proceso->kg,
				'importetotal' => $proceso->kg*9,
				'cancelado' => 0
			);
			$id_compra = $this->Compra_model->save($data);
			$compra = $this->Compra_model->getById($id_compra);
			$data = array(
				'id_compra' => $id_compra,
				'compra' => $compra
			);
			$this->load->view("admin/compra/pdf",$data);
        }elseif($proceso->tipo == 12){
        	$data = array(
				'id_proveedor' => $id_productor,
				'fecha_compra' => $fecha,
				'cantidadkg' => $proceso->kg,
				'importetotal' => $proceso->kg*12,
				'cancelado' => 0
			);
			$id_compra = $this->Compra_model->save($data);
			$compra = $this->Compra_model->getById($id_compra);
			$data = array(
				'id_compra' => $id_compra,
				'compra' => $compra
			);
			$this->load->view("admin/compra/pdfh",$data);
        }
		$data2 = array(
			'fecha_f' => $fecha,
			'RP_Status_idRP_Status' => 5,
			'RPid_compra_semilla' => $id_compra
		);
		$this->Proceso_model->setStatus($id_proceso,$data2);
	}
	public function setIdCompra($id_proceso,$id_compra){
		$data = array(
			'RPid_compra_semilla' => $id_compra
		);
		$this->Proceso_model->setStatus($id_proceso,$data);
	}
	public function info_compra($id){
		$proceso = $this->Proceso_model->getProcesoByIdP($id);
		$data = array(
			'compra' => $this->Compra_model->getById($proceso->RPid_compra_semilla)
		);
		//print_r($data);
		$this->load->view("admin/produccion/info_compra",$data);
	}
}
