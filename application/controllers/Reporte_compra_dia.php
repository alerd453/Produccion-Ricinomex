<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reporte_compra_dia extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Compra_model");
		$this->load->library("pdf");
	}
	public function index()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/compra/reporte_dia');
		$this->load->view('layouts/footer');
	}
	public function generar_pdf($fecha)
	{
		$data = array(
			'dia' => $this->Compra_model->getComprasByFecha($fecha)
		);	
		$this->load->view("admin/compra/pdf_reporte_dia",$data);
	}
}