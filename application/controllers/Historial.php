<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Historial extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Productores_model");
		$this->load->model("Compra_model");
		$this->load->model("Localidades_model");
		$this->load->library("pdf");
	}
	public function index()
	{
		/*$data = array(
			'productores' => $this->Productores_model->getProductores()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/compra/nueva_compra',$data);
		$this->load->view('layouts/footer');*/
	}
	public function productor(){
		$data = array(
			'productores' => $this->Productores_model->getProductores()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view("admin/compra/historial_productor",$data);
		$this->load->view('layouts/footer');
	}
	public function comunidad(){
		$data = array(
			'localidades' => $this->Localidades_model->getAllLocalidades()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view("admin/compra/historial_comunidad",$data);
		$this->load->view('layouts/footer');
	}
	public function ver_productor($id){
		$data = array(
			'compras' => $this->Compra_model->getCompraByProductor($id)
		);
		$this->load->view("admin/compra/compraproductor",$data);
	}
	public function ver_localidad($id){
		$data = array(
			'compras' => $this->Compra_model->getCompraByLocalidad($id)
		);
		$this->load->view("admin/compra/compralocalidad",$data);
	}
}