<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Entrega_2018 extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Productores_model");
		$this->load->model("Entrega_model_2018");
	}
	public function index()
	{
		$data = array(
			'productores' => $this->Productores_model->getProductoresF(),
			'entregados' => $this->Entrega_model_2018->get()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/entrega/listF',$data);
		$this->load->view('layouts/footer');
	}
	public function get($id){
		$data = array(
			'productor' => $this->Productores_model->getProductor($id)
		);
		$this->load->view("admin/entrega/formulario",$data);
	}
	public function save($id_productores,$hibrida,$urea,$dap,$insecticida,$mejorador,$fertilizante,$fecha){
		$data = array(
			'id_productor' => $id_productores,
			'semilla_hibrida' => $hibrida,
			'urea_kg' => $urea,
			'dap' => $dap,
			'insecticida' => $insecticida,
			'mejorador' => $mejorador,
			'fertilizante' => $fertilizante,
			'fecha_entrega' => $fecha
		);
		$id_entrega = $this->Entrega_model_2018->save($data);
		echo $id_entrega;
	}
}
