<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Entrega_2017 extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Productores_model");
		$this->load->model("Entrega_2017_model");
	}
	public function index()
	{
		$data = array(
			'productores' => $this->Productores_model->getProductoresPrograma()
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/entrega/list',$data);
		$this->load->view('layouts/footer');
	}
	public function get($id){
		$data = array(
			'Entrega_2017' => $this->Entrega_2017_model->getById($id)
		);
		$this->load->view("admin/entrega/entregaid",$data);
	}
	public function folio($id){
		echo $id; 
	}
}
