<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productores_model extends CI_Model {
	public function getProductores(){
		$this->db->select('pd.telefono,pd.idProducores,pd.nombre_completo,pd.apellido_paterno,pd.apellido_materno,pd.nombre,pd.curp,pd.localidad,pd.superficie,l.id,l.nombre_l,l.municipio_id,m.id,m.nombre_m');
        $this->db->from('rp_productores pd');
        $this->db->join('localidades l', 'pd.localidad=l.id');
        $this->db->join('municipios m', 'l.municipio_id=m.id');
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getProductoresPrograma(){
		$this->db->select('pd.telefono,pd.idProducores,pd.nombre_completo,pd.apellido_paterno,pd.apellido_materno,pd.nombre,pd.curp,pd.localidad,pd.aprobado,l.id,l.nombre_l,l.municipio_id,m.id,m.nombre_m, e.id_entrega, e.id_productor, e.fecha_entrega_polvo, e.fecha_entrega_liquido');
        $this->db->from('rp_productores pd');
        $this->db->join('localidades l', 'pd.localidad=l.id');
        $this->db->join('municipios m', 'l.municipio_id=m.id');
        $this->db->join('rp_entrega_2017 e', 'e.id_productor=pd.idProducores');
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getProductoresF(){
		$this->db->select('pd.telefono,pd.idProducores,pd.nombre_completo,pd.apellido_paterno,pd.apellido_materno,pd.nombre,pd.curp,pd.localidad,pd.aprobado,pd.programa,l.id,l.nombre_l,l.municipio_id,m.id,m.nombre_m');
        $this->db->from('rp_productores pd');
        $this->db->join('localidades l', 'pd.localidad=l.id');
        $this->db->join('municipios m', 'l.municipio_id=m.id');
        $this->db->where('pd.programa',1);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getProductor($id){
		$this->db->select('pd.telefono,pd.idProducores,pd.nombre_completo,pd.apellido_paterno,pd.apellido_materno,pd.nombre,pd.curp,pd.localidad,pd.sexo,pd.superficie,pd.aprobado,pd.telefono,pd.programa,l.id,l.nombre_l,l.municipio_id,m.id,m.nombre_m');
        $this->db->from('rp_productores pd');
        $this->db->join('localidades l', 'pd.localidad=l.id');
        $this->db->join('municipios m', 'l.municipio_id=m.id');
        $this->db->where('pd.idProducores',$id);
		$resultados = $this->db->get();
		return $resultados->row();
	}
	public function getlocalidad($id){
		$this->db->where("id",$id);
		$resultados = $this->db->get("localidades");
		return $resultados->row();
	}
	public function setTelefono($id,$data){
		$this->db->where("idProducores",$id);
		return $this->db->update("rp_productores",$data);
	}
	public function save($data){
		return $this->db->insert("rp_productores",$data);
	}
	public function update($id,$data){
		$this->db->where("idProducores",$id);
		return $this->db->update("rp_productores",$data);
	}
	public function delete($id){
		$this->db->where("idProducores",$id); 
		$this->db->delete("rp_productores");
	}
	public function buscar($buscar,$inicio = FALSE, $cantidadregistro = FALSE)
	{
		$this->db->select('pd.telefono,pd.idProducores,pd.nombre_completo,pd.apellido_paterno,pd.apellido_materno,pd.nombre,pd.curp,pd.localidad,l.id,l.nombre_l,l.municipio_id,m.id,m.nombre_m');
        $this->db->from('rp_productores pd');
        $this->db->join('localidades l', 'pd.localidad=l.id');
        $this->db->join('municipios m', 'l.municipio_id=m.id');
		$this->db->like("pd.nombre",$buscar,'both');
		$this->db->or_like('pd.apellido_paterno',$buscar, 'both');
		$this->db->or_like('pd.apellido_materno',$buscar);
		$this->db->or_like('l.nombre_l',$buscar);
		$this->db->or_like('m.nombre_m',$buscar);
		if ($inicio !== FALSE && $cantidadregistro !== FALSE) {
			$this->db->limit($cantidadregistro,$inicio);
		}
		$consulta = $this->db->get();
		return $consulta->result();
	}
}