<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Localidades_model extends CI_Model {
	public function getLocalidades($id){
		$this->db->where("municipio_id",$id);
		$resultados = $this->db->get("localidades");
		return $resultados->result();
	}
	public function getAllLocalidades(){
		$resultados = $this->db->get("localidades");
		return $resultados->result();
	}
}
