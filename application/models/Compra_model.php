<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Compra_model extends CI_Model {
	public function save($data){
		$this->db->insert("rp_compra_semilla",$data);
		return $this->db->insert_id();
	}
	public function getCompras(){
		$this->db->select('cs.id_compra_semilla,cs.id_proveedor,cs.cantidadkg,cs.importetotal,cs.fecha_compra,cs.cancelado, cs.razon_cancelado,cs.auditado,cs.kg_au,cs.observacion_auditoria,cs.audito,cs.fecha_auditoria,p.idProducores,p.nombre_completo,p.localidad,p.superficie,p.telefono,l.id,l.nombre_l');
        $this->db->from('rp_compra_semilla cs');
        $this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
        $this->db->join('localidades l', 'p.localidad = l.id' );
        $this->db->where('cs.cancelado',0);
        $query = $this->db->get();
        return $query->result();
	}

	public function getComprasOld(){
		$this->db->select('cs.id_compra_semilla,cs.id_proveedor,cs.cantidadkg,cs.importetotal,cs.fecha_compra,cs.auditado,cs.kg_au,cs.observacion_auditoria,cs.audito,cs.fecha_auditoria,p.idProducores,p.nombre_completo,p.localidad,p.superficie,p.telefono,l.id,l.nombre_l');
        $this->db->from('rp_compra_semilla_old cs');
        $this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
        $this->db->join('localidades l', 'p.localidad = l.id' );
        $query = $this->db->get();
        return $query->result();
	}

	public function getAllCompras(){
		$this->db->select('cs.id_compra_semilla,cs.id_proveedor,cs.cantidadkg,cs.importetotal,cs.fecha_compra,cs.cancelado, cs.razon_cancelado,cs.auditado,cs.kg_au,p.idProducores,p.nombre_completo,p.localidad,p.superficie,p.telefono,l.id,l.nombre_l');
        $this->db->from('rp_compra_semilla cs');
        $this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
        $this->db->join('localidades l', 'p.localidad = l.id' );
        $this->db->where('cs.id_compra_semilla >=',1521);
        $this->db->order_by("cs.id_proveedor", "asc");
        $query = $this->db->get();
        return $query->result();
	}
	public function getAllIngresos(){
		$this->db->select('cs.id_compra_semilla,cs.id_proveedor,cs.cantidadkg,cs.importetotal,cs.fecha_compra,cs.cancelado, cs.razon_cancelado,cs.auditado,cs.kg_au,p.idProducores,p.nombre_completo,p.localidad,p.superficie,p.telefono,l.id,l.nombre_l');
        $this->db->from('rp_compra_semilla cs');
        $this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
        $this->db->join('localidades l', 'p.localidad = l.id' );
        $this->db->where('cs.id_compra_semilla >=',3301);
        $query = $this->db->get();
        return $query->result();
	}
	public function getById($id){
		$this->db->select('cs.id_compra_semilla,cs.id_proveedor,cs.cantidadkg,cs.importetotal,cs.fecha_compra,cs.hibrida,cs.manzanita,p.idProducores,p.nombre_completo,p.localidad,p.superficie,p.telefono,l.id,l.nombre_l');
        $this->db->from('rp_compra_semilla cs');
        $this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
        $this->db->join('localidades l', 'p.localidad = l.id' );
        $this->db->where('cs.id_compra_semilla',$id);
        $query = $this->db->get();
        return $query->row();
	}
	public function update($id,$data){
		$this->db->where("id_compra_semilla",$id);
		return $this->db->update("rp_compra_semilla",$data);
	}
	public function getComprasByFecha($fecha){
		$this->db->select('cs.id_compra_semilla, cs.id_proveedor, cs.cantidadkg, cs.importetotal, cs.fecha_compra, cs.cancelado, cs.razon_cancelado, p.idProducores, p.nombre_completo, p.localidad,l.id,l.nombre_l');
		$this->db->from('rp_compra_semilla cs');
		$this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
		$this->db->join('localidades l', 'p.localidad = l.id');
		$this->db->where('cs.fecha_compra',$fecha);
		$query = $this->db->get();
		return $query->result();
	}
	public function getCompraByProductor($id){
		$this->db->select('cs.id_compra_semilla, cs.id_proveedor, cs.cantidadkg, cs.importetotal, cs.fecha_compra, cs.cancelado, cs.razon_cancelado, p.idProducores, p.nombre_completo, p.localidad,l.id,l.nombre_l');
		$this->db->from('rp_compra_semilla cs');
		$this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
		$this->db->join('localidades l', 'p.localidad = l.id');
		$this->db->where('cs.id_proveedor',$id);
		$query = $this->db->get();
		return $query->result();
	}
	public function getCompraByLocalidad($id){
		$this->db->select('cs.id_compra_semilla, cs.id_proveedor, cs.cantidadkg, cs.importetotal, cs.fecha_compra, cs.cancelado, cs.razon_cancelado, p.idProducores, p.nombre_completo, p.localidad,l.id,l.nombre_l');
		$this->db->from('rp_compra_semilla cs');
		$this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
		$this->db->join('localidades l', 'p.localidad = l.id');
		$this->db->where('l.id',$id);
		$query = $this->db->get();
		return $query->result();
	}
	public function setAuditoria($id,$data){
		$this->db->where("id_compra_semilla",$id);
		return $this->db->update("rp_compra_semilla",$data);
	}
	public function setAuditoriaOld($id,$data){
		$this->db->where("id_compra_semilla",$id);
		return $this->db->update("rp_compra_semilla_old",$data);
	}
}
