<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Parcelas_model extends CI_Model {
	public function getParcelas(){
        $resultados = $this->db->get('rp_parcelas');
		return $resultados->result();
    }
    public function getParcelasByIdProductor($id){
        $this->db->where("id_productor",$id);
		$resultados = $this->db->get("rp_parcelas");
        return $resultados->result();
    }
    public function getParcelasByIdParcelas($id){
        $this->db->where("id_parcela",$id);
		$resultados = $this->db->get("rp_parcelas");
        return $resultados->result();
    }
    public function saveParcela($data){
        return $this->db->insert("rp_parcelas",$data);
    }
    public function updateParcela($id_parcela, $data){
        $this->db->where("id_parcela",$id_parcela);
        return $this->db->update("rp_parcelas",$data);
    }
    public function deleteParcela($id){
        $this->db->where("id_parcela",$id); 
        $this->db->delete("rp_parcelas");
    }
}
