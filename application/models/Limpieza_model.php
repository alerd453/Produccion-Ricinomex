<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Limpieza_model extends CI_Model {
	public function save($data){
		return $this->db->insert("rp_limpieza",$data);
	}
	public function getProceso($id){
		$this->db->where("RP_Proceso_idRP_Proceso",$id);
		$resultados = $this->db->get("rp_limpieza");
		return $resultados->row();
	}
	public function setTiempo($id,$data){
		$this->db->where("idRP_Limpieza",$id);
		return $this->db->update("rp_limpieza",$data);
	}
	public function getLimpiezas(){
		$this->db;
		$resultados = $this->db->get("rp_limpieza");
		return $resultados->result();
	}
}
