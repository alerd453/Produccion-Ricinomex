<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Descascarado_model extends CI_Model {
	public function save($data){
		return $this->db->insert("rp_descascarado",$data);
	}
	public function getProceso($id){
		$this->db->where("RP_Proceso_idRP_Proceso",$id);
		$resultados = $this->db->get("rp_descascarado");
		return $resultados->row();
	}
	public function setTiempo($id,$data){
		$this->db->where("idRP_Descascarado",$id);
		return $this->db->update("rp_descascarado",$data);
	}
	public function getDescascarados(){
		$this->db;
		$resultados = $this->db->get("rp_descascarado");
		return $resultados->result();
	}
}
