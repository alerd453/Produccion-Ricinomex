<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Entrega_model_2018 extends CI_Model {
	public function save($data){
		$this->db->insert("rp_entrega_2018",$data);
		return $this->db->insert_id();
	}
	public function get(){
		$this->db->select('id_productor');
        $this->db->from('rp_entrega_2018');
		$query = $this->db->get();
        return $query->result();
	}
}
