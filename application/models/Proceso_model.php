<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Proceso_model extends CI_Model {
	public function getProcesos(){
		$this->db->select('pc.idProceso,pc.fecha_inicial,pc.costales_i,pc.kg_i,pc.RP_Productores_idProducores,pc.RP_Status_idRP_Status,pc.RPid_compra_semilla,pd.telefono,pd.idProducores,pd.nombre_completo,pd.apellido_paterno,pd.apellido_materno,pd.nombre,pd.curp,pd.localidad,l.id,l.nombre_l,l.municipio_id,m.id,m.nombre_m,s.idRP_Status,s.nombre_s,s.color');
        $this->db->from('rp_proceso pc');
        $this->db->join('rp_productores pd', 'pc.RP_Productores_idProducores=pd.idProducores');
        $this->db->join('localidades l', 'pd.localidad=l.id');
        $this->db->join('municipios m', 'l.municipio_id=m.id');
        $this->db->join('rp_status s','s.idRP_Status=pc.RP_Status_idRP_Status');	
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function save($data){
		return $this->db->insert("rp_proceso",$data);
	}
	public function setStatus($id,$data){
		$this->db->where("idProceso",$id);
		return $this->db->update("rp_proceso",$data);
	}
	public function getInfo($id_Proceso,$id_Productor){
		$this->db->select('pc.idProceso, pc.fecha_inicial, pc.costales_i,pc.kg_i,pc.tipo,pc.RP_Productores_idProducores, d.idRP_Descascarado, d.fecha_inicial fecha_inicial_d,d.fecha_final fecha_final_d,d.costales costales_d, d.costales_vacios costales_vacios_d, d.kg kg_d, d.tiempo_des,d.RP_Trabajadores_idTrabajadores,l.idRP_Limpieza, l.fecha_inicial fecha_inicial_l, l.fecha_final fecha_final_l, l.costales, l.costales_vacios, l.kg, l.tiempo_lim, l.RP_Trabajadores_idTrabajadores');
		$this->db->from('rp_proceso pc');
		$this->db->join('rp_descascarado d', 'd.RP_Proceso_idRP_Proceso=pc.idProceso');
		$this->db->join('rp_limpieza l', 'l.RP_Proceso_idRP_Proceso=pc.idProceso');
		$this->db->where('pc.idProceso',$id_Proceso);
		$resultados = $this->db->get();
		return $resultados->row();
	}
	public function getProcesoById($id){
		$this->db->where("RP_Proceso_idRP_Proceso",$id);
		$resultados = $this->db->get("rp_limpieza");
		return $resultados->row();
	}
	public function getProcesoById2($id_Proceso){
		$this->db->select('pc.idProceso, pc.fecha_inicial, pc.costales_i,pc.kg_i,pc.tipo,pc.RP_Productores_idProducores, d.idRP_Descascarado, d.fecha_inicial fecha_inicial_d,d.fecha_final fecha_final_d,d.costales costales_d, d.costales_vacios costales_vacios_d, d.kg kg_d, d.tiempo_des,d.RP_Trabajadores_idTrabajadores,l.idRP_Limpieza, l.fecha_inicial fecha_inicial_l, l.fecha_final fecha_final_l, l.costales, l.costales_vacios, l.kg, l.tiempo_lim, l.RP_Trabajadores_idTrabajadores');
		$this->db->from('rp_proceso pc');
		$this->db->join('rp_descascarado d', 'd.RP_Proceso_idRP_Proceso=pc.idProceso');
		$this->db->join('rp_limpieza l', 'l.RP_Proceso_idRP_Proceso=pc.idProceso');
		$this->db->where('pc.idProceso',$id_Proceso);
		$resultados = $this->db->get();
		return $resultados->row();
	}
	public function getProcesoByIdP($id){
		$this->db->where("idProceso",$id);
		$resultado = $this->db->get("rp_proceso");
		return $resultado->row();
	}
	public function getHumedo(){
		$this->db->select('p.idProceso, p.costales_i, p.kg_i, p.RP_Status_idRP_Status');
		$this->db->from('rp_proceso p');
		$this->db->where('p.RP_Status_idRP_Status',6);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getHumedoByFecha($fechaini, $fechafin){
		$this->db->select('p.idProceso, p.costales_i, p.kg_i, p.RP_Status_idRP_Status, p.fecha_inicial');
		$this->db->from('rp_proceso p');
		$this->db->where('p.RP_Status_idRP_Status',6);
		$this->db->where('p.fecha_inicial <=',$fechafin);
		$this->db->where('p.fecha_inicial >=',$fechaini);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getSinDescascarar(){
		$this->db->select('p.idProceso, p.costales_i, p.kg_i, p.RP_Status_idRP_Status');
		$this->db->from('rp_proceso p');
		$this->db->where('p.RP_Status_idRP_Status',0);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getSinDescascararByFecha($fechaini, $fechafin){
		$this->db->select('p.idProceso, p.costales_i, p.kg_i, p.RP_Status_idRP_Status, p.fecha_inicial');
		$this->db->from('rp_proceso p');
		$this->db->where('p.RP_Status_idRP_Status',0);
		$this->db->where('p.fecha_inicial <=',$fechafin);
		$this->db->where('p.fecha_inicial >=',$fechaini);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getSinLimpiar(){
		$this->db->select('p.idProceso, p.costales_i, p.kg_i, p.RP_Status_idRP_Status, d.RP_Proceso_idRP_Proceso, d.costales, d.kg');
		$this->db->from('rp_proceso p');
		$this->db->join('rp_descascarado d', 'd.RP_Proceso_idRP_Proceso=p.idProceso');
		$this->db->where('p.RP_Status_idRP_Status',2);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getSinLimpiarByFecha($fechaini, $fechafin){
		$this->db->select('p.idProceso, p.costales_i, p.kg_i, p.RP_Status_idRP_Status, p.fecha_inicial ,d.RP_Proceso_idRP_Proceso, d.costales, d.kg');
		$this->db->from('rp_proceso p');
		$this->db->join('rp_descascarado d', 'd.RP_Proceso_idRP_Proceso=p.idProceso');
		$this->db->where('p.RP_Status_idRP_Status',2);
		$this->db->where('p.fecha_inicial <=',$fechafin);
		$this->db->where('p.fecha_inicial >=',$fechaini);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getPorPagar(){
		$this->db->select('p.idProceso, p.costales_i, p.kg_i, p.RP_Status_idRP_Status, d.RP_Proceso_idRP_Proceso, d.costales, d.kg');
		$this->db->from('rp_proceso p');
		$this->db->join('rp_limpieza d', 'd.RP_Proceso_idRP_Proceso=p.idProceso');
		$this->db->where('p.RP_Status_idRP_Status',4);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getPorPagarByFecha($fechaini, $fechafin){
		$this->db->select('p.idProceso, p.costales_i, p.kg_i, p.RP_Status_idRP_Status, p.fecha_inicial, d.RP_Proceso_idRP_Proceso, d.costales, d.kg');
		$this->db->from('rp_proceso p');
		$this->db->join('rp_limpieza d', 'd.RP_Proceso_idRP_Proceso=p.idProceso');
		$this->db->where('p.RP_Status_idRP_Status',4);
		$this->db->where('p.fecha_inicial <=',$fechafin);
		$this->db->where('p.fecha_inicial >=',$fechaini);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getPagadoC(){
		$this->db->select('c.cantidadkg, c.importetotal, c.cancelado, c.hibrida');
		$this->db->from('rp_compra_semilla c');
		$this->db->where('c.cancelado',0);
		$this->db->where('c.hibrida',0);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getPagadoCByFecha($fechaini, $fechafin){
		$this->db->select('c.cantidadkg, c.importetotal, c.cancelado, c.fecha_compra, c.hibrida');
		$this->db->from('rp_compra_semilla c');
		$this->db->where('c.cancelado',0);
		$this->db->where('c.fecha_compra <=',$fechafin);
		$this->db->where('c.fecha_compra >=',$fechaini);
		$this->db->where('c.hibrida',0);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getPagadoH(){
		$this->db->select('c.cantidadkg, c.importetotal, c.cancelado, c.hibrida');
		$this->db->from('rp_compra_semilla c');
		$this->db->where('c.cancelado',0);
		$this->db->where('c.hibrida',1);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getPagadoHByFecha($fechaini, $fechafin){
		$this->db->select('c.cantidadkg, c.importetotal, c.cancelado, c.fecha_compra, c.hibrida');
		$this->db->from('rp_compra_semilla c');
		$this->db->where('c.cancelado',0);
		$this->db->where('c.fecha_compra <=',$fechafin);
		$this->db->where('c.fecha_compra >=',$fechaini);
		$this->db->where('c.hibrida',1);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getHueso(){
		$this->db->select('l.hueso, l.estado');
		$this->db->from('rp_limpieza l');
		$this->db->where('l.estado',"Finish");
		$this->db->where('l.hueso !=',0);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getHuesoByFecha($fechaini, $fechafin){
		$this->db->select('l.hueso, l.estado');
		$this->db->from('rp_limpieza l');
		$this->db->where('l.estado',"Finish");
		$this->db->where('l.fecha_final <=',$fechafin);
		$this->db->where('l.fecha_final >=',$fechaini);
		$this->db->where('l.hueso !=',0);
		$resultados = $this->db->get();
		return $resultados->result();
	}
}