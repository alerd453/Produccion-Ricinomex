<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secado_model extends CI_Model {
	public function save_ini($data){
		return $this->db->insert("rp_secado",$data);
	}
	public function getProceso($id){
		$this->db->where("RP_Proceso_idRP_Proceso",$id);
		$resultados = $this->db->get("rp_secado");
		return $resultados->row();
	}
	public function save_fin($id_secado,$data){
		$this->db->where("idRP_Secado",$id_secado);
		return $this->db->update("rp_secado",$data);
	}
}
