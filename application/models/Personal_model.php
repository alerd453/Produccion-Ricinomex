<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Personal_model extends CI_Model {
	public function getPersonal(){
		$this->db->select('t.idTrabajadores,t.nombre,t.apellido_paterno,apellido_materno,d.idDepartamento,d.nombred');
        $this->db->from('rp_trabajadores t');
        $this->db->join('rp_departamento d', 't.RP_Departamento_idDepartamento = d.idDepartamento');
		$this->db->where("estado","1");
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getPersonalProd(){
		$this->db->select("t.idTrabajadores,t.nombre,t.RP_Departamento_idDepartamento,p.id_prodictividad_emp,p.fecha,p.costales,p.kilos,p.RP_idTrabajadores");
		$this->db->from('rp_trabajadores t');
		$this->db->join('rp_productividad_emp p', 't.idTrabajadores=p.RP_idTrabajadores');
		$this->db->where("t.estado","1");
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getPersonalDesc(){
		$this->db->where("RP_Departamento_idDepartamento",1);
		$resultados = $this->db->get("rp_trabajadores");
		return $resultados->result();
	}
	public function getPersonalLimp(){
		$this->db->where("RP_Departamento_idDepartamento",2);
		$resultados = $this->db->get("rp_trabajadores");
		return $resultados->result();
	}
	public function save($data){
		return $this->db->insert("rp_trabajadores",$data);
	}
	public function getPerson($id){
		$this->db->select('t.idTrabajadores,t.nombre,t.apellido_paterno,apellido_materno,d.idDepartamento,d.nombred');
        $this->db->from('rp_trabajadores t');
        $this->db->join('rp_departamento d', 't.RP_Departamento_idDepartamento = d.idDepartamento');
        $this->db->where('t.idTrabajadores',$id);
        $query = $this->db->get();
        return $query->row();
		/*$this->db->where("idTrabajadores",$id);
		$resultado = $this->db->get("rp_trabajadores");
		return $resultado->row();*/
	}
	public function update($id,$data){
		$this->db->where("idTrabajadores",$id);
		return $this->db->update("rp_trabajadores",$data);
	}
}
