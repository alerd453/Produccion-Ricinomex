<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productividad_model extends CI_Model {
	public function save($data){
		return $this->db->insert("rp_productividad_emp",$data);
	}
	public function getProductividadByFecha($fecha){
		$this->db->select('p.id_prodictividad_emp, p.fecha, p.costales, p.kilos, p.RP_idTrabajadores, t.idTrabajadores, t.nombre, t.apellido_paterno, t.RP_Departamento_idDepartamento, d.idDepartamento, d.nombred');
		$this->db->from('rp_productividad_emp p');
		$this->db->join('rp_trabajadores t', 't.idTrabajadores=p.RP_idTrabajadores');
		$this->db->join('rp_departamento d', 'd.idDepartamento=t.RP_Departamento_idDepartamento');
		$this->db->where('p.fecha',$fecha);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getProductividadSemanaP($fechaini,$fechafin){
		$this->db->select('p.id_prodictividad_emp, p.fecha, p.costales, p.kilos, p.RP_idTrabajadores, t.idTrabajadores, t.nombre, t.apellido_paterno, t.RP_Departamento_idDepartamento, d.idDepartamento, d.nombred');
		$this->db->from('rp_productividad_emp p');
		$this->db->join('rp_trabajadores t', 't.idTrabajadores=p.RP_idTrabajadores');
		$this->db->join('rp_departamento d', 'd.idDepartamento=t.RP_Departamento_idDepartamento');
		$this->db->where('d.idDepartamento','1');
		$this->db->where('p.fecha >=',$fechaini);
		$this->db->where('p.fecha <=',$fechafin);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getProductividadSemanaL($fechaini,$fechafin){
		$this->db->select('p.id_prodictividad_emp, p.fecha, p.costales, p.kilos, p.RP_idTrabajadores, t.idTrabajadores, t.nombre, t.apellido_paterno, t.RP_Departamento_idDepartamento, d.idDepartamento, d.nombred');
		$this->db->from('rp_productividad_emp p');
		$this->db->join('rp_trabajadores t', 't.idTrabajadores=p.RP_idTrabajadores');
		$this->db->join('rp_departamento d', 'd.idDepartamento=t.RP_Departamento_idDepartamento');
		$this->db->where('d.idDepartamento','2');
		$this->db->where('p.fecha >=',$fechaini);
		$this->db->where('p.fecha <=',$fechafin);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getProductividadDiaP($fecha){
		$this->db->select('p.id_prodictividad_emp, p.fecha, p.costales, p.kilos, p.RP_idTrabajadores, t.idTrabajadores, t.nombre, t.apellido_paterno, t.RP_Departamento_idDepartamento, d.idDepartamento, d.nombred');
		$this->db->from('rp_productividad_emp p');
		$this->db->join('rp_trabajadores t', 't.idTrabajadores=p.RP_idTrabajadores');
		$this->db->join('rp_departamento d', 'd.idDepartamento=t.RP_Departamento_idDepartamento');
		$this->db->where('d.idDepartamento','1');
		$this->db->where('p.fecha',$fecha);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getProductividadDiaL($fecha){
		$this->db->select('p.id_prodictividad_emp, p.fecha, p.costales, p.kilos, p.RP_idTrabajadores, t.idTrabajadores, t.nombre, t.apellido_paterno, t.RP_Departamento_idDepartamento, d.idDepartamento, d.nombred');
		$this->db->from('rp_productividad_emp p');
		$this->db->join('rp_trabajadores t', 't.idTrabajadores=p.RP_idTrabajadores');
		$this->db->join('rp_departamento d', 'd.idDepartamento=t.RP_Departamento_idDepartamento');
		$this->db->where('d.idDepartamento','2');
		$this->db->where('p.fecha',$fecha);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getProductividadSemanaPastel($fechaini,$fechafin){
		$this->db->select('p.id_prodictividad_emp, p.fecha, p.costales, p.kilos, p.RP_idTrabajadores, t.idTrabajadores, t.nombre, t.apellido_paterno, t.RP_Departamento_idDepartamento, d.idDepartamento, d.nombred');
		$this->db->from('rp_productividad_emp p');
		$this->db->join('rp_trabajadores t', 't.idTrabajadores=p.RP_idTrabajadores');
		$this->db->join('rp_departamento d', 'd.idDepartamento=t.RP_Departamento_idDepartamento');
		$this->db->where('p.fecha >=',$fechaini);
		$this->db->where('p.fecha <=',$fechafin);
		$this->db->order_by('p.fecha', 'ASC');
		$resultados = $this->db->get();
		return $resultados->result();
	}
}