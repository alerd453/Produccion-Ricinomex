<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Venta_model extends CI_Model {
	public function save($data){
		$this->db->insert("rp_ventas",$data);
		return $this->db->insert_id();
	}
	public function getById($id){
		$this->db->select('v.id_rp_ventas, v.nombre_comprador, v.producto_venta, v.precio, v.cantidad, v.importe, v.fecha_venta');
        $this->db->from('rp_ventas v');
        $this->db->where('v.id_rp_ventas',$id);
        $query = $this->db->get();
        return $query->row();
	}
}
