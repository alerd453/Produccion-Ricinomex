<?php 
	include('php/conexion.php');
	header("Content-Type: text/html;charset=utf-8");
	$query = mysqli_query($mysqli,"SELECT * FROM proveedores_2017 WHERE municipio like 'Coatecas Altas' || municipio like 'HEROICA CIUDAD DE EJUTLA DE CR' || municipio like '%LA COMPA%' || municipio like 'LA PE' || municipio like '%N DE PORFIRIO D%' || municipio like 'MONJAS' || municipio like 'OCOTLAN DE MORELOS' || municipio like 'SAN ANDRES ZABACHE' || municipio like 'SAN JOSE DEL PROGRESO' || municipio like 'SAN LUCAS QUIAVINI' || municipio like 'SAN MARTIN DE LOS CANSECOS' || localidad like 'SAN NICOLAS' || municipio like 'SAN PABLO HUIXTEPEC' || localidad like 'SAN JOSE CIENEGUILLA'|| municipio like 'SAN SIMON ALMOLONGAS'|| municipio like 'SANTO TOMAS JALIEZA'|| municipio like 'TRINIDAD ZAACHILA'|| municipio like 'YAXE'|| municipio like 'SAN VICENTE COATLAN'|| municipio like 'SANTA ANA'|| municipio like 'SANTA CRUZ XITLA'|| municipio like 'SANTA GERTRUDIS' || municipio like 'YOGANA' ORDER BY localidad ASC") or die ('error : '.mysqli_error($mysqli));

	date_default_timezone_set('UTC');
	date_default_timezone_set("America/Mexico_City");
	/** Se agrega la libreria PHPExcel */
 	require_once 'assets/plugins/PHPExcel/PHPExcel.php';
 
	// Se crea el objeto PHPExcel
	$objPHPExcel = new PHPExcel();
	// Se asignan las propiedades del libro
	$objPHPExcel->getProperties()->setCreator("RICINOMEX") // Nombre del autor
	    ->setLastModifiedBy("RICINOMEX") //Ultimo usuario que lo modificó
	    ->setTitle("Reporte Entrega de Fertilizante de Localidades") // Titulo
	    ->setSubject("Reporte Entrega de Fertilizante de Localidades") //Asunto
	    ->setDescription("Reporte Fertilizante") //Descripción
	    ->setKeywords("reporte fertilizante localidades ricinomex") //Etiquetas
	    ->setCategory("Reporte Entrega Fertilizante"); //Categorias
	$tituloReporte = "Listado de Productores";
	$titulosColumnas = array('#', 'Nombre Completo', 'Municipio', 'Localidad', 'Fertilizante Entregado', 'Liquido Entregado');
	// Se combinan las celdas A1 hasta D1, para colocar ahí el titulo del reporte
	$objPHPExcel->setActiveSheetIndex(0)
	    ->mergeCells('A1:F1');
	 
	// Se agregan los titulos del reporte
	$objPHPExcel->setActiveSheetIndex(0)
	    ->setCellValue('A1', $tituloReporte) // Titulo del reporte
	    ->setCellValue('A3',  $titulosColumnas[0])  //Titulo de las columnas
	    ->setCellValue('B3',  $titulosColumnas[1])
	    ->setCellValue('C3',  $titulosColumnas[2])
	    ->setCellValue('D3',  $titulosColumnas[3])
	    ->setCellValue('E3',  $titulosColumnas[4])
	    ->setCellValue('F3',  $titulosColumnas[5]);
	$i = 4; //Numero de fila donde se va a comenzar a rellenar
	while($fila = mysqli_fetch_assoc($query)){
		if($fila['programa']){
			if($fila['entregado']){
				if ($fila['entregado_insec']) {
					$objPHPExcel->setActiveSheetIndex(0)
				        ->setCellValue('A'.$i, $fila['id_proveedor'])
				        ->setCellValue('B'.$i, utf8_decode($fila['nombre_completo']))
				        ->setCellValue('C'.$i, utf8_decode($fila['municipio']))
				        ->setCellValue('D'.$i, utf8_decode($fila['localidad']))
				        ->setCellValue('E'.$i, "Entregado")
				        ->setCellValue('F'.$i, "Entregado");
				    $i++;
				}else{
					$objPHPExcel->setActiveSheetIndex(0)
				        ->setCellValue('A'.$i, $fila['id_proveedor'])
				        ->setCellValue('B'.$i, utf8_decode($fila['nombre_completo']))
				        ->setCellValue('C'.$i, utf8_decode($fila['municipio']))
				        ->setCellValue('D'.$i, utf8_decode($fila['localidad']))
				        ->setCellValue('E'.$i, "Entregado")
				        ->setCellValue('F'.$i, "No Entregado");
			    	$i++;
				}
			}else{
				if ($fila['entregado_insec']) {
					$objPHPExcel->setActiveSheetIndex(0)
				        ->setCellValue('A'.$i, $fila['id_proveedor'])
				        ->setCellValue('B'.$i, utf8_decode($fila['nombre_completo']))
				        ->setCellValue('C'.$i, utf8_decode($fila['municipio']))
				        ->setCellValue('D'.$i, utf8_decode($fila['localidad']))
				        ->setCellValue('E'.$i, "No Entregado")
				        ->setCellValue('F'.$i, "Entregado");
				    $i++;
				}else{
					$objPHPExcel->setActiveSheetIndex(0)
				        ->setCellValue('A'.$i, $fila['id_proveedor'])
				        ->setCellValue('B'.$i, utf8_decode($fila['nombre_completo']))
				        ->setCellValue('C'.$i, utf8_decode($fila['municipio']))
				        ->setCellValue('D'.$i, utf8_decode($fila['localidad']))
				        ->setCellValue('E'.$i, "No Entregado")
				        ->setCellValue('F'.$i, "No Entregado");
				    $i++;
				}
			}
		}
	}
	$estiloTituloReporte = array(
		'font' => array(
		    'name'      => 'Verdana',
		    'bold'      => true,
		    'italic'    => false,
		    'strike'    => false,
		    'size' =>16,
		    'color'     => array(
		        'rgb' => 'FFFFFF'
		    )
		),
		'fill' => array(
		  'type'  => PHPExcel_Style_Fill::FILL_SOLID,
		  'color' => array(
		        'argb' => 'FF220835')
		),
		'borders' => array(
		    'allborders' => array(
		        'style' => PHPExcel_Style_Border::BORDER_NONE
		    )
		),
		'alignment' => array(
		    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		    'rotation' => 0,
		    'wrap' => TRUE
		)
	);
 
	$estiloTituloColumnas = array(
	    'font' => array(
	        'name'  => 'Arial',
	        'bold'  => true,
	        'color' => array(
	            'rgb' => 'FFFFFF'
	        )
	    ),
	    'fill' => array(
	        'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
	  'rotation'   => 90,
	        'startcolor' => array(
	            'rgb' => 'c47cf2'
	        ),
	        'endcolor' => array(
	            'argb' => 'FF431a5d'
	        )
	    ),
	    'borders' => array(
	        'top' => array(
	            'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	            'color' => array(
	                'rgb' => '143860'
	            )
	        ),
	        'bottom' => array(
	            'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	            'color' => array(
	                'rgb' => '143860'
	            )
	        )
	    ),
	    'alignment' =>  array(
	        'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        'wrap'      => TRUE
	    )
	);
	 
	$estiloInformacion = new PHPExcel_Style();
	$estiloInformacion->applyFromArray( array(
	    'font' => array(
	        'name'  => 'Arial',
	        'color' => array(
	            'rgb' => '000000'
	        )
	    ),
	    'fill' => array(
	  'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	  'color' => array(
	            'argb' => 'FFd9b7f4')
	  ),
	    'borders' => array(
	        'left' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN ,
	      'color' => array(
	              'rgb' => '3a2a47'
	            )
	        )
	    )
	));
	$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($estiloTituloReporte);
	$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($estiloTituloColumnas);
	$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:F".($i-1));
	// Se asigna el nombre a la hoja
	$objPHPExcel->getActiveSheet()->setTitle('Reporte');
	 for($i = 'A'; $i <= 'F'; $i++){
	    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
	}
	// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
	$objPHPExcel->setActiveSheetIndex(0);
	 
	// Inmovilizar paneles
	//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
	$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);
	// Se manda el archivo al navegador web, con el nombre que se indica, en formato 2007
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Reporte.xlsx"');
	header('Cache-Control: max-age=0');
	 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
?>