<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compra_model extends CI_Model {
	public function save($data){
		$this->db->insert("rp_compra_semilla",$data);
		return $this->db->insert_id();
	}
	public function getById($id){
		$this->db->select('cs.id_compra_semilla,cs.id_proveedor,cs.cantidadkg,cs.importetotal,cs.fecha_compra,p.idProducores,p.nombre_completo,p.localidad,p.consenso,p.superficie,p.telefono,l.id,l.nombre_l');
        $this->db->from('rp_compra_semilla cs');
        $this->db->join('rp_productores p', 'cs.id_proveedor = p.idProducores');
        $this->db->join('localidades l', 'p.localidad = l.id' );
        $this->db->where('cs.id_compra_semilla',$id);
        $query = $this->db->get();
        return $query->row();
	}
	public function update($id,$data){
		$this->db->where("id_compra_semilla",$id);
		return $this->db->update("rp_compra_semilla",$data);
	}
}
